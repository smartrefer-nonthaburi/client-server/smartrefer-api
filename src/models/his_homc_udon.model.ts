import Knex = require('knex');
var md5 = require('md5');
const hospcode = process.env.HIS_CODE;

export class HisHomcUbonModel {
  /* OK */
  async getLogin(db: Knex, username: any, password: any) {
    let data = await db.raw(`
    SELECT RTRIM(LTRIM(profile.UserCode)) as username ,CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname, '10671' as hcode
    from profile
    WHERE RTRIM(LTRIM(profile.UserCode)) = '${username}'
    and profile.PWDUdon = '${password}'
    `);
    return data[0];
    // let pass = md5(password).toUpperCase();
    // let data = await db(`profile`)
    // .select(db.raw(`RTRIM(LTRIM(profile.UserCode)) as username`),db.raw(`CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname`),db.raw(`'10669' as hcode`))
    // .whereRaw(`RTRIM(LTRIM(profile.UserCode)) = ?`,[username])
    // .andWhereRaw(`profile.PassCode = ?`,[pass])
    // return data;
  }
  /* OK */
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '${hospcode}'`);
    return data;
  }
  // ข้อมูลพื้นฐานผู้ป่วย >> OK
  async getProfile(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)) as hn,rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,convert(date,convert(char,p.birthDay -5430000)) as brthdate
    ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
    else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
    RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
    RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age 
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))   from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,ps.father as father_name 
    ,p.mother as mother_name
    ,'' as couple_name
    ,ps.relatives as contact_name
    ,ps.relationship as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data[0];
  }

  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    select convert(date,convert(char,o.registDate -5430000)) as date_serv,
    CONVERT (time,(left (o.timePt,2)+':'+right (o.timePt,2))) as time_serv,
    p.VACCODE as vaccine_code,v.VACNAME as vaccine_name 
    from PPOP_EPI p
    left join OPD_H o on(o.hn = p.HN and p.REGNO = o.regNo)
    left join PPOP_VACCINE v on(v.VACCODE = p.VACCODE)
    where p.HN = dbo.padc('${hn}',' ',7)
    `);
    return data;
  }

  async getChronic(db: Knex, hn: any, referno: any) {
    // let data = await db.raw(`
    // select distinct top 1 rtrim(p.ICDCode) as icd_code,rtrim(ic.DES) as icd_name
    // ,p.VisitDate as 'start_date' 
    // from PATDIAG p with(nolock) 
    // left join OPD_H o with(nolock) on(o.hn = p.Hn and o.regNo = p.regNo) 
    // left join ICD101 ic with(nolock) on(ic.CODE = p.ICDCode) where RTRIM(LTRIM(o.hn))='1309890' and p.DiagType in('I') and p.dxtype = '1' 
    // and ( p.ICDCode between 'I60' and 'I698' or p.ICDCode between 'J45' and 'J46' or p.ICDCode between 'I10' and 'I159' 
    // or p.ICDCode between 'A15' and 'A199' or p.ICDCode between 'E10' and 'E149' or p.ICDCode between 'F30' and 'F399' 
    // or p.ICDCode between 'J43' and 'J449' or p.ICDCode between 'J429' and 'J429' or p.ICDCode between 'I20' and 'I259' 
    // or p.ICDCode between 'I05' and 'I099' or p.ICDCode between 'I26' and 'I289' or p.ICDCode between 'I30' and 'I528' 
    // or p.ICDCode between 'G80' and 'G839' or p.ICDCode between 'D50' and 'D649' or p.ICDCode between 'N17' and 'N19'
    // ) order by p.VisitDate
    // `);
    // return data[0];
  }
  // ประวัติแพ้ยา >> OK
  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    select v.gen_name as drug_name,m.alergyNote as symptom 
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),2)) as begin_date
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),2)) as daterecord
    from medalery m left join Med_inv v on m.medCode=v.abbr where m.hn=dbo.padc('${hn}',' ',7) order by m.updDate desc
    `);
    return data;
  }
  /* OK */
  async getServices(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where o.hn = dbo.padc('${hn}',' ',7) and o.regNo =  right('${seq}' ,2)
    union all 
    SELECT i.hn+i.regist_flag as seq
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,w.ward_name as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    left join Refer r on i.hn = r.Hn
    where i.hn = dbo.padc('${hn}',' ',7) and i.regist_flag = right('${seq}' ,2)
    `);
    return data[0];
  }
  // การวินิจฉัย  >> OK
/*
    Old query
    SELECT ltrim(rtrim(p.Hn)) + '-' + p.regNo AS seq
    ,convert(date,convert(char,p.VisitDate -5430000)) AS date_serv
    ,SUBSTRING(o.timePt,1,2)+':'+SUBSTRING(o.timePt,3,4) as time_serv
    ,p.ICDCode as icd_code,ICD101.CODE + '>' + ICD101.DES AS icd_name
    , p.DiagType AS diag_type,isnull(p.DiagNote,'') as DiagNote 
    ,p.dxtype AS diagtype_id
    FROM PATDIAG p
    left join OPD_H o on(o.hn = p.Hn and o.regNo = p.regNo) 
    LEFT JOIN ICD101 ON p.ICDCode = ICD101.CODE 
    WHERE (p.pt_status in('O','I','D')) and p.DiagType in('I','E')
    AND o.hn =dbo.padc('${hn}',' ',7) 
    AND p.regNo = right('${vn}' ,2)
    order by p.VisitDate desc

*/
  async getDiagnosis(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq, convert(date, convert(char, r.ReferDate - 5430000)) as date_serv, '' as time_serv
    ,dx.ICDCode as 'icd_code', '1' as diag_type, rtrim(dx.DiagNote) as DiagNote, '1' as diagtype_id
    ,RTRIM(CASE WHEN LEN(ISNULL(dx.DiagNote,''))>0 THEN ISNULL(dx.DiagNote,'') ELSE  ISNULL(icd.DES,'') END) AS 'icd_name'
    from Refer r with (nolock) left join
     PATDIAG dx with (nolock) ON r.Hn = dx.Hn and r.RegNo = dx.regNo left join
    ICD101 icd with (nolock) ON dx.ICDCode = icd.CODE
    where r.ReferStatus <> 'I' and r.Hn = dbo.padc('${hn}',' ',7)  and dx.DiagType='I'
    `);
    return data;
  }
  /* ข้อมูล Refer >> OK */
  async getRefer(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , isnull(i.ladmit_n, '') as an
      , ltrim(rtrim(r.Hn)) as pid
      , ltrim(rtrim(r.Hn)) as hn
      , ltrim(rtrim(r.ReferRunno)) as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , isnull(d.qTimeOut,d.qTimeIn) as refertime
      , concat(rtrim(dr.doctorID), ',',cid.cid) as doctor
      , concat(dr.docName,' ',dr.docLName) as doctor_name
    from Refer r
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    left join Deptq_d as d on r.Hn = d.hn and r.RegNo = d.regNo 
    left join DOCC as dr on d.docCode=dr.docCode
    left join DOCC_CID as cid on dr.docName = cid.fname and dr.docLName = cid.lname
    where r.ReferStatus <> 'I'  and r.cancelFlag is null and d.qStatus <> '3'
    and r.Hn = dbo.padc('${hn}',' ',7) and r.RegNo = right('${vn}', 2) and r.ReferRunno = '${referno}'
    `);
    return data;
  }

  /* OK */
  async getProcedure(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + o.REGNO as seq,
      (o.OR_DATE) as date_serv, o.OR_TIME as time_serv, o.ORCODE as procedure_code, o.ORDESC as icd_name,
      (o.START_DATE) as start_date, (o.END_DATE) as end_date
    from ORREQ_H o
    left join OPD_H p on(o.HN = p.hn and o.REGNO = p.regNo)
    where o.HN = dbo.padc('${hn}',' ',7) and o.REGNO = right('${vn}', 2) 
    order by date_serv desc `);
    return data;
  }
  /* OK */
  async getDrugs(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`
select x.*
    from (select DISTINCT
    ltrim(rtrim(pm.hn)) + pm.registNo as seq
      , convert(date, convert(char, h.registDate - 5430000)) as date_serv
      , SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) as time_serv
      , m.name as drug_name
      , RTRIM(pm.accAmt) as qty
      , RTRIM(pm.unit) as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join OPD_H h(NOLOCK) on(h.hn = pm.hn and  h.regNo = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)
    and h.ipdStatus = '0'
    AND   pm.registNo = right('01', 2)
    union all
    select DISTINCT
    i.hn + i.regist_flag as seq
      , convert(date, convert(char, i.admit_date - 5430000)) as date_serv
      , left(i.admit_time, 2) + ':' + right(i.admit_time, 2) as time_serv
      , m.name as drug_name
      , pm.accAmt as qty
      , m.unit as unit
      , RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1
      , '' as usage_line2
      , '' as usage_line3
    from Patmed pm(NOLOCK)
    left join Ipd_h i(NOLOCK) on(i.hn = pm.hn and  i.regist_flag = pm.registNo)
    left join Med_inv m(NOLOCK)  on(pm.invCode = m.code and m.site = '1')
    where pm.hn = dbo.padc('${hn}',' ',7)  and i.ladmit_n != '' AND pm.invType='M'
    AND i.regist_flag = right('01', 2) ) as x 
    order by x.date_serv desc,x.time_serv desc
    `);
    return data;
  }
  /* OK */
  async getLabs(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(` 
    select
    convert(date, convert(char, a.res_date - 5430000)) as date_serv
      , left(a.res_time, 2) + ':' + right(a.res_time, 2) as time_serv
      , isnull(a.lab_code, '') as labgroup
      , a.result_name as lab_name
      , isnull(replace(replace(a.real_res,'""',''),'!',''),'รอผล') as lab_result
      , isnull(s.result_unit,'') as unit
      , a.low_normal + '-' + a.high_normal as standard_result
    from Labres_d a(nolock)
    left join OPD_H o(nolock) on(o.hn = a.hn and o.regNo = a.reg_flag)
    left join Labreq_h b(nolock) on(a.req_no = b.req_no)
    left join Labre_s s(nolock) on(a.lab_code = s.lab_code and a.res_item = s.res_run_no and s.labType = a.lab_type)
    LEFT JOIN Lab lb ON(a.lab_code = lb.labCode and lb.labType = a.lab_type)
    left join Ward w on(w.ward_id = b.reqFrom)
    left join PatSS ss on(ss.hn = a.hn)
    where a.hn = dbo.padc('${hn}',' ',7) AND a.real_res<>''
    AND a.reg_flag = right('${vn}', 2)
    AND a.res_date <> ''
    order by a.res_date asc ,left(a.res_time, 2) + ':' + right(a.res_time, 2) desc
    `);
    //console.log(data);

    return data;
  }

  async getAppointment(db: Knex, hn: any, dateServe: any, referno: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + p.regNo as seq, a.pre_dept_code as clinic,
      convert(date, convert(char, p.registDate - 5430000)) as date_serv,
      SUBSTRING(p.timePt, 1, 2) + ':' + SUBSTRING(p.timePt, 3, 4) as time_serv,
      convert(date, convert(char, a.appoint_date - 5430000)) as appoint_date,
      a.appoint_time_from as appoint_time, a.appoint_note as detail
    from Appoint a
    left join OPD_H p on(a.hn = p.hn and a.regNo = p.regNo)
    where p.hn = dbo.padc('${hn}',' ',7)`);
    return data;
  }

  /*ข้อมูลการรักษา >> OK*/
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    select top 1 ltrim(rtrim(v.hn)) + v.RegNo as seq
    ,convert(date, convert(char, v.VisitDate)) as date_serv
    ,CONVERT(VARCHAR(5),v.VisitDate, 108)  AS time_serv
    ,isnull((select PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn),'') as bloodgrp
    ,v.Weight AS weight, v.Height as height
    ,v.BMI as bmi
    ,v.Temperature AS temperature
    ,v.Pulse AS pr
    ,v.Breathe AS rr
    ,v.Hbloodpress as sbp
    ,v.Lbloodpress AS dbp
    ,isnull(SUBSTRING(v.Symtom,1,(case when CHARINDEX('PI:', v.Symtom) <= 0 then  100 else  CHARINDEX('PI:', v.Symtom) end)-1),'') as symptom
    ,v.deptCode as depcode
    ,(select deptDesc from DEPT d where d.deptCode = v.deptCode) as department
    ,v.ER_M as movement_score
    ,v.ER_V as vocal_score
    ,v.ER_E as eye_score
    ,v.ER_O2sat as oxygen_sat
    ,v.ER_Rt as pupil_right
    ,v.ER_Lt as pupil_left
    ,CONVERT(INT, v.ER_M)+CONVERT(INT, v.ER_V)+CONVERT(INT, v.ER_E) as gak_coma_sco
    ,isnull(ReferDiag,'') as diag_text
    from VitalSign v
    left join Refer as r on v.hn=r.Hn and v.RegNo=r.RegNo and r.ReferStatus <> 'I'
    left join PATDIAG dx on(v.hn=dx.Hn and v.RegNo=dx.regNo)
    where v.hn= dbo.padc('${hn}',' ',7) and v.RegNo = right('${seq}', 2)
    order by v.VitalSignNo desc
    `);
    return data;
  }
  /* ข้อมูล PE >> OK */
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
    , '' as pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }
  /* ข้อมูล HPI >> OK */
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq,
     ISNULL('PhysicalExam: ' + CAST(PEtxt AS VARCHAR(MAX)), '') + ' ' + 
     ISNULL('Treatment: ' + CAST(TreatmentTxt AS VARCHAR(MAX)), '') + ' ' + 
     ISNULL('คำวินิจฉัยแพทย์: ' + CAST(DIAGtxt AS VARCHAR(MAX)), '') AS pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    LEFT JOIN Physical_Exam ex WITH(NOLOCK) ON OPD_H.hn = ex.hn AND OPD_H.regNo = ex.regNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc
      `);
    return data;
  }
  async getBedsharing(db: Knex) {
    let data = await db.raw(`    
    SELECT ward_id as ward_code,ward_name,
    (select count(i.hn) from Ipd_h i where ward_id=w.ward_id and i.discharge_status='0') as ward_pt,
    w.ward_tot_bed as ward_bed,
    w.ward_tot_bed as ward_standard
    from Ward w where (ward_tot_bed<>'' and ward_tot_bed<>'99') and (w.UNUSES<>'Y' or w.UNUSES is null)
    `);
    return data;
  }

  /* OK */
  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , r.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , rtrim(t.titleName) as title_name
      , rtrim(p.firstName) as first_name
      , rtrim(p.lastName) as last_name
      , r.ReferRunno as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , isnull(d.qTimeOut,d.qTimeIn) as refertime
      , dr.doctorID as doctor
      , concat(dr.docName,' ',dr.docLName) as doctor_name
    from Refer r
    left join PATIENT p on r.Hn = p.hn
    LEFT JOIN PTITLE t ON p.titleCode = t.titleCode
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID 
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    left join Deptq_d as d on r.Hn = d.hn and r.RegNo = d.regNo 
  left join DOCC as dr on d.docCode=dr.docCode
    where r.ReferStatus = 'O' and convert(char, r.ReferDate - 5430000)  between  replace('${start_date}', '-', '') and replace('${end_date}', '-', '') 
    and r.cancelFlag is null and d.qStatus <> '3'
 and r.ReferHCODE<>'10671' and r.VUpHosp='Y'
 order by hn desc`);
    return data;
  }
  /* OK */
  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT
    ltrim(rtrim(Refer.Hn)) + Refer.RegNo as seq
      , Refer.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , RTRIM(PTITLE.titleName) as title_name
      , rtrim(PATIENT.firstName) as first_name
      , rtrim(PATIENT.lastName) as last_name
      , Refer.ReferRunno AS referno
        , convert(date, convert(char, Refer.ReferDate - 5430000)) as referdate
        , Refer.ReferHCODE AS to_hcode
          , HOSPCODE.NAME as to_hcode_name
          , isnull(rs.REASON_DESC, '') as refer_cause
          , isnull(d.qTimeOut,d.qTimeIn) as refertime
          , '' AS doctor
            , '' as doctor_name
    FROM Refer with (nolock)
    INNER JOIN PATIENT with (nolock) ON Refer.Hn = PATIENT.hn
    INNER JOIN PTITLE with (nolock) ON PATIENT.titleCode = PTITLE.titleCode
    LEFT JOIN HOSPCODE with (nolock) ON Refer.ReferHCODE = HOSPCODE.OFF_ID
    left join REFERRS rs on Refer.ReferReason = rs.REASON_CODE
    left join Deptq_d as d on Refer.Hn = d.hn and Refer.RegNo = d.regNo
    left join Ipd_h i on Refer.Hn = i.hn and Refer.RegNo = i.regist_flag
    WHERE Refer.ReferStatus = 'S' and convert(char, Refer.ReferDate - 5430000)  BETWEEN replace('${start_date}', '-', '') and replace('${end_date}', '-', '')
    `);
    return data;
  }

  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db.raw(`
    SELECT a.hn as seq
    ,convert(date,convert(char,a.appoint_date -5430000)) as receive_appoint_date
    ,a.appoint_time_from as receive_appoing_begintime
    ,a.appoint_time_to as receive_appoing_endtime
    ,dbo.TitleFullName(d.docName,d.docLName) as receive_appoing_doctor
    ,ltrim(rtrim(deptDesc)) as receive_appoing_chinic
    ,appoint_note as receive_text
    ,(RTRIM(pr.firstName)+' '+RTRIM(pr.lastName)) as receive_by 
    ,a.remark as receive_special_note
    ,concat(ltrim(rtrim(deptDesc)),'(',dep.deptCode,')') as receive_checkpoint
    ,concat('045-319200',' ต่อ ',teldept) as receive_teldept
    FROM Appoint a left join PATIENT p on p.hn=a.hn
    left join PTITLE pti on pti.titleCode=p.titleCode
    left join DOCC d on d.docCode=a.doctor
    left join DEPT dep on dep.deptCode=a.pre_dept_code
    left join profile pr on pr.UserCode=a.maker
    WHERE
    (a.app_type = 'A')
    and convert(char, a.appoint_date - 5430000)  = replace('${app_date}', '-', '')
    and a.hn='${hn}' order by a.keyin_time desc
        `);
    return data;
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
          select convert(date, convert(char, x.xreq_date - 5430000)) as xray_date,c.xproc_des as xray_name
          from XresHis x 
          left join OPD_H o (NOLOCK) on (o.hn = x.hn and o.regNo = x.regist_flag) 
          LEFT JOIN Xreq_h xh on(xh.hn = x.hn and xh.regist_flag = x.regist_flag and x.xrunno = xh.xrunno) 
          LEFT JOIN Xpart p ON(x.xpart_code = p.xpart_code) 
          LEFT JOIN Xreq_d d ON(x.xprefix = d.xprefix AND x.xrunno = d.xrunno) 
          LEFT JOIN Xcon xc on(x.xprefix = xc.xprefix) 
          LEFT JOIN Xproc c ON(d.xpart_code = c.xproc_part  AND d.xproc_code = c.xproc_code) 
          WHERE xc.xchr_code <> 'PAT' 
          AND x.hn = dbo.padc('${hn}',' ',7)
          AND x.regist_flag = right('${seq}', 2)
          order by  x.xreq_date DESC
         `);
    return data;
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
      SELECT ltrim(rtrim(ward_id)) as dep_code,ltrim(rtrim(ward_name)) as dep_name from Ward order by ward_name asc
    `);
    return data
  }
  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
      select hn from PatSS with(nolock) where CardID = '${cid}'
    `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT dbo.ce2dmy1(D.lastIssDate) AS lastIssDate1,D.NameDrug,LTRIM(RTRIM(D.dataExpres)) as dataExpres ,D.qty 
                           FROM     
                           (      
                           SELECT Patmed.hn, Patmed.invCode, MAX(Patmed.lastIssDate) AS SDate,lamedTimeText,lamedText        
                           FROM   Patmed WITH (NOLOCK) INNER JOIN                                                           
                                  OPD_H WITH (NOLOCK) ON Patmed.hn = OPD_H.hn AND Patmed.registNo = OPD_H.regNo INNER JOIN   
                                  Med_inv WITH (NOLOCK) ON Patmed.invCode = Med_inv.code                                         
                           WHERE  (Patmed.hn ='   8386') AND (OPD_H.ipdStatus = '0') AND (Patmed.invType = 'M') AND (Patmed.accQty > 0 or (Patmed.accQty=0 and Patmed.qty=0)) and Patmed.cancleMed is null AND  (Med_inv.CHRONIC = 'Y') OR    
                                  (Patmed.hn ='   8386') AND (OPD_H.ipdStatus <> '0') AND (Patmed.homeMed = 'Y') AND (Patmed.invType = 'M') AND (Patmed.accQty > 0 or (Patmed.accQty=0 and Patmed.qty=0)) and Patmed.cancleMed is null AND (Med_inv.CHRONIC = 'Y') AND (Patmed.amount>=0)                              
                           GROUP BY Patmed.hn, Patmed.invCode,lamedText,lamedTimeText 
                           ) M INNER JOIN   
                           (               
                           SELECT Patmed.hn, Patmed.registNo, Patmed.invCode, Med_inv.prod_type, RTRIM(Patmed.accQty) + '  ' + RTRIM(Med_inv.package) AS qty,
                                  Patmed.amount, Patmed.accQty, Patmed.accAmt, Patmed.lastIssDate, Patmed.lamedHow, Patmed.lamedUnit, Patmed.lamedTime,       
                                  CASE WHEN LEFT(Patmed.lamedTime, 1) <> '&' THEN '&' + Patmed.lamedTime ELSE Patmed.lamedTime END AS PatTime,               
                                  Lamed_3.lamed_code AS lamed_code3, Patmed.lamedSpecial, Patmed.lamedQty, Patmed.lamedPeriod, Patmed.unit, Med_inv.code,     
                                  Med_inv.name, Lamed_1.lamed_code AS lamed_code1, RTRIM(Lamed_1.lamed_name) + RTRIM(Patmed.lamedQty)                        
                                   + RTRIM(Lamed_2.lamed_name) + RTRIM(Lamed_3.lamed_name) AS total, Lamed_2.lamed_name AS UNIT,                              
                                  Lamed_2.lamed_code AS UNITCODE, Patmed.lamedDays, Patmed.lamedText,Patmed.lamedTimeText,                                    
                                  Lamed_3.alias, Patmed.reqNo,RTRIM(Med_inv.name) AS NameDrug,                                                                
                                  RTRIM(PATIENT.firstName) + '  ' + RTRIM(PATIENT.lastName) AS FullName,                                                    
                          CAST( RTRIM(isnull(CASE when Patmed.lamedEng='Y' then Lamed_1.lamed_eng  else Lamed_1.myLamed end,''))+' '+RTRIM(CASE when (Patmed.lamedQty='0' or isnull(Patmed.lamedQty,'')='') then Patmed.lamedQty  else Patmed.lamedQty end)+' '+ 
                          RTRIM(isnull(CASE when Patmed.lamedEng='Y' then Lamed_2.lamed_eng  else Lamed_2.myLamed end,'')) +' '+                                                                                                                                
                          (CASE when (Patmed.lamedTimeText is null or Patmed.lamedTimeText='') then RTRIM(isnull(CASE when Patmed.lamedEng='Y' then Lamed_3.lamed_eng  else Lamed_3.lamed_name end,'')) else RTRIM(ISNULL(Patmed.lamedTimeText,'')) end +' '+    
                          CASE when (Patmed.lamedText is null or Patmed.lamedText='') then RTRIM(isnull(CASE when Patmed.lamedEng='Y' then L4.lamed_eng  else L4.lamed_name end,'')) else RTRIM(isnull(Patmed.lamedText,''))END)                               
                                  AS VARCHAR(250)) as dataExpres       
                            FROM Patmed WITH (nolock) INNER JOIN       
                                  OPD_H WITH (NOLOCK) ON  Patmed.hn = OPD_H.hn AND Patmed.registNo = OPD_H.regNo LEFT OUTER JOIN                                       
                                  Lamed Lamed_3 WITH (nolock) ON CASE WHEN LEFT(Patmed.lamedTime, 1)                                                                   
                                  <> '&' THEN '&' + Patmed.lamedTime ELSE Patmed.lamedTime END = Lamed_3.lamed_code LEFT OUTER JOIN                                
                                  Lamed Lamed_1 ON CASE WHEN LEFT(Patmed.lamedHow, 1)                                                                                  
                                  <> '*' THEN '*' + Patmed.lamedHow ELSE Patmed.lamedHow END = Lamed_1.lamed_code LEFT OUTER JOIN                                  
                                  Med_inv WITH (NOLOCK) ON Patmed.invCode = Med_inv.code And (Med_inv.site = '1 ') And (Patmed.invType = 'M') LEFT OUTER JOIN      
                                  Lamed Lamed_2 ON CASE WHEN LEFT(Patmed.lamedUnit, 1)                                                                                 
                                  <> '!' THEN '!' + Patmed.lamedUnit ELSE Patmed.lamedUnit END = Lamed_2.lamed_code left join                                      
                                  Lamed L4 (nolock ) on   (L4.lamed_code = '@'+lamedSpecial or L4.lamed_code = lamedSpecial) LEFT OUTER JOIN                         
                                  PATIENT WITH (NOLOCK) ON Patmed.hn = PATIENT.hn                                                                                      
                            WHERE    (LTRIM(RTRIM(Patmed.hn)) ='${hn}') AND (OPD_H.ipdStatus = '0') AND (Patmed.invType = 'M')                                                
                                  AND (Patmed.accQty > 0 or (Patmed.accQty=0 and Patmed.qty=0)) and Patmed.cancleMed is null AND (Med_inv.CHRONIC = 'Y')            
                                  AND (DATEDIFF(MONTH,Patmed.lastIssDate,GETDATE())) <7 OR                                                                             
                                  (LTRIM(RTRIM(Patmed.hn)) = '${hn}') AND (OPD_H.ipdStatus <> '0') AND (Patmed.homeMed = 'Y')                                                  
                                  AND (Patmed.invType = 'M') AND (Patmed.accQty > 0 or (Patmed.accQty=0 and Patmed.qty=0)) and Patmed.cancleMed is null  AND (Med_inv.CHRONIC = 'Y') AND (Patmed.amount>=0)
          AND (DATEDIFF(MONTH,Patmed.lastIssDate,GETDATE()) <=3) AND Lamed_3.lamed_code <>'&ST'                                               
                            ) D ON M.hn=D.hn AND M.invCode=D.invCode AND M.SDate=D.lastIssDate  AND M.lamedText=D.lamedText AND M.lamedTimeText=D.lamedTimeText        
                            order by M.SDate Desc
        `);
    return data;
  }

  /* OK */
  async getServicesCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT top 3 o.hn, ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where o.hn = dbo.padc('${hn}',' ',7) GROUP BY o.hn,o.regNo,o.registDate,o.timePt,d.deptDesc
    order by seq desc
    `);
    return data;
  }

  async getProfileCoc(db: Knex, hn: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)) as hn,rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
    else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
    RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
    RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age 
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,ps.father as father_name 
    ,p.mother as mother_name
    ,'' as couple_name
    ,ps.relatives as contact_name
    ,ps.relationship as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data[0];
  }

}
