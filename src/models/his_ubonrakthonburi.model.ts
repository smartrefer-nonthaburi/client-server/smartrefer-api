import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisUbonrakThonburiModel {

    async getLogin(db: Knex, username: any, password: any) {
        // let data = await db.raw(`
        // select rtrim(tusstfno) as username,rtrim(TUSUSRNAM) as fullname,'11919' as  hcode 
        // from TABUSRV5PF where TUSSTFNO<>'' and rtrim(tusstfno) ='${username}' and rtrim(TUSUSRCOD)='${password}' and TUSACTFLG=''
        // `);
        // return data;
        let data = await db(`TABUSRV5PF`)
        .select(db.raw(`rtrim(tusstfno) as username`) , db.raw(`rtrim(TUSUSRNAM) as fullname`),db.raw(`'15078' as hcode `))
        .andWhereRaw(`TUSSTFNO <> '' and rtrim(tusstfno) = ? and rtrim(TUSUSRCOD)= ? and TUSACTFLG = ''`,[username,password]);
        return data;
      
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT CAST(OAPVN AS VARCHAR) +  RIGHT('00' + CAST(OAPSEQNO AS VARCHAR), 2) AS seq,TPRPRENAM AS title_name,RMSNAME AS first_name,RMSSURNAM AS last_name,
	[dbo].[Change_DateYMD2](OAPREGDTE) AS date_serv,
	[dbo].[FormatTime](OAPFRMTIM) AS  time_serv ,
	RTBTABNAM AS department
	FROM OPDAPPV5PF 
	INNER JOIN REGMASV5PF ON RMSHNREF = OAPHN
	INNER JOIN TABPREV5PF ON TPRCODREF = RMSPRENAM
	INNER JOIN REGTABV5PF ON RTBTABCOD = OAPDIVCOD AND RTBTABTYP = '01'
	WHERE OAPCRDSTS <> 'C'  and OAPHN = '${hn}'  and  CAST(OAPVN AS VARCHAR) +  RIGHT('00' + CAST(OAPSEQNO AS VARCHAR), 2) ='${seq}'`);
        return data;
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        Select Rdthn As hn ,Rmsidno As cid ,Rtrim((Select Tprprenam From Tabprev5pf Where Tprcodref = Rmsprenam)) As title_name
        ,Rtrim(Rmsname) As first_name ,Rtrim(Rmssurnam) As last_name ,'' As moopart ,Rtrim(Rdtadr) As addrpart
        ,Rdttbncod As tmbpart ,Rdtampcod As amppart , Rdtprvcod As chwpart
        ,[Dbo].[Gethbdhn_ymd](Rdthn) As brthdate 
        ,[Dbo].[Getageyymmddbyhn_ymd](Rdthn,Oapregdte) As age
        ,Rtrim([Dbo].[Get_contypconcod_io](Oapregdte,Rdthn,0,Oapvn,Oapseqno,'t')) As pttype_id
        ,Rtrim([Dbo].[Get_contypconcod_nam]([Dbo].[Get_contypconcod_io](Oapregdte,Rdthn,0,Oapvn,Oapseqno,'t'),'')) As pttype_name
        ,'' As  pttype_no ,'' As  hospmain ,'' As  hospmain_name ,'' As  hospsub ,'' As  hospsub_name
        ,[Dbo].[Change_dateymd2](Oapregdte) As segistdate
        ,[Dbo].[Change_dateymd2](Oapregdte) As visitdate
        ,Rtrim(Rdtmother) As father_name ,'' As mother_name ,Rtrim(Rdtspouse) As couple_name
        ,Rtrim(Rdtemrnam) As contact_name ,Rtrim(Rdtemrrlt) As contact_relation
        ,Rtrim(Rdttel) As contact_mobile 
         From Regmasv5pf
         Inner Join Regdetv5pf On Rmshnref = Rdthn
         Inner Join Opdappv5pf On Oaphn = Rmshnref And Oapcrdsts <> 'c'
         WHERE  RDTHN ='${hn}'  and  CAST(RTRIM(CAST(OAPVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(OAPSEQNO AS CHAR)),2) AS decimal) ='${seq}'
         `);
         return data;
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT case when HNMHOSCOD='01'  then '11919' end AS provider_code,HNMHOSNAM AS provider_name 
        FROM HOSNAMV5PF          
        `);
        return data;
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT PRDPRDNAM AS drug_name, RAGREMARK AS symptom, [dbo].[Change_DateYMD2](RAGDOCDTE) AS begin_date, [dbo].[Change_DateYMD2](RAGDOCDTE) AS daterecord
	FROM REGALGV5PF 
        INNER JOIN PRDMASV5PF ON PRDPRDNO = RAGPRDNO
        WHERE RAGHN ='${hn}'`);
        return data;
    }

    async getChronic(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT DISTINCT RTRIM(MDSDSECOD) AS icd_code ,[dbo].[Change_DateYMD2](MDSINDTE) AS [start_date] ,RTRIM(TDSNAMENG) AS icd_name
	FROM MDRDSEV5PF 
	INNER JOIN TABDSEV5PF ON MDSDSECOD = TDSDSECOD AND TDSDSEICD = '10'
	WHERE MDSIOPD = 'O'
	AND MDSDOCTYP = '01' 
	AND MDSDSECOD <> ''
        WHERE MDSHN ='${hn}'`);
        return data;
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT RTRIM(MDSVN) AS seq, [dbo].[Change_DateYMD2](OAPREGDTE) AS date_serv, [dbo].[FormatTime](OAPRQOTIM) AS time_serv
	,RTRIM(MDSDSECOD) AS icd_code, RTRIM(TDSNAMENG) AS cd_name
	,RTRIM([dbo].[GET_MDRDSE_COD](MDSIOPD,MDSINDTE,MDSHN,MDSVN,0,MDSDOCTYP)) AS diag_type
	,RTRIM((SELECT TDSNAMENG FROM TABDSEV5PF WHERE TDSDSEICD = '10' AND TDSDSECOD = [dbo].[GET_MDRDSE_COD](MDSIOPD,MDSINDTE,MDSHN,MDSVN,0,MDSDOCTYP))) AS DiagNote
	,'' AS diagtype_id
	FROM MDRDSEV5PF
	INNER JOIN OPDAPPV5PF ON OAPREGDTE = MDSINDTE AND OAPHN = MDSHN AND MDSVN = RTRIM(CAST(OAPVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(OAPSEQNO AS CHAR)),2)
	INNER JOIN TABDSEV5PF ON TDSDSECOD = MDSDSECOD AND TDSDSEICD = '10' 
        WHERE MDSIOPD = 'O' AND MDSDOCTYP = '01' AND MDSDSECOD <> ''
        and  RTRIM(MDSVN) ='${seq}' and   [dbo].[Change_DateYMD2](OAPREGDTE) = '${dateServe}' and mdshn =  '${hn}'
         `);
        return data;
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT CAST(RTRIM(CAST(IPDVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(IPDSEQNO AS CHAR)),2) AS decimal) AS seq 
	,IPDAN AS an ,IPDHN AS pid ,IPDHN as hn ,IPDDOCNO as referno
	,[dbo].[Change_DateYMD2](IPDDTE) AS referdate ,RTRIM(IPDHOSTO) AS to_hcode
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'))
	END AS pttype_id
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'),''))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'),''))
	END AS pttype_name
	,'' AS strength_id ,'' AS strength_name ,'' AS location_name ,'' AS station_name ,'' AS loads_id ,'' AS loads_name
	,RTRIM(IPDHOSTO) AS to_hcode_name ,RTRIM(IPDCAU) AS refer_cause
	,ISNULL([dbo].[FormatTime](IPDSECTIM),0) AS  refertime
	,'' AS doctor ,'' AS  doctor_name ,'' as refer_remark	
	FROM IPDREFV5PF
        where CAST(RTRIM(CAST(IPDVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(IPDSEQNO AS CHAR)),2) AS decimal) ='${seq}' and IPDDOCNO = '${referno}' and 
              IPDHN = '${hn}' and  [dbo].[Change_DateYMD2](IPDDTE  = '${dateServe}' 
        `);
        return data;
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT CAST(BTMVN1 AS VARCHAR) +  RIGHT('00' + CAST(BTMVN2 AS VARCHAR), 2) AS seq, [dbo].[Change_DateYMD2](BTMDOCDTE) AS date_serv, 
 [dbo].[FormatTime](BTMDOCTIM) AS time_serv, PRDPRDNAM AS drug_name, BTDISSQTY AS qty, PRDUM AS unit ,
 CAST(BTDMTHNAM AS CHAR) + CAST(BTDQTYNAM AS CHAR) AS usage_line1 ,
 CAST(BTDTIMNAM1 AS CHAR) + CAST(BTDTIMNAM2 AS CHAR) AS usage_line2,BTDWRNNAM1 AS usage_line3
 FROM BILTRMV5PF 
 INNER JOIN BILTRDV5PF ON BTDHN = BTMHN AND BTDDOCNO = BTMDOCNO 
 INNER JOIN PRDMASV5PF ON PRDPRDNO = BTDPRDNO
 and BTMSTSFLG >= '2'
        WHERE CAST(BTMVN1 AS VARCHAR) +  RIGHT('00' + CAST(BTMVN2 AS VARCHAR), 2) = '${seq}'  and [dbo].[Change_DateYMD2](BTMDOCDTE) =  '${dateServe}'  and BTDHN = '${hn}'`)
        return data;
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT [dbo].[Change_DateYMD2](LRQRQODTE) AS date_serv,[dbo].[FormatTime](LRQRQOTIM) AS time_serv, LRSLABTYP AS labgroup,LBTLABNAM AS lab_name,
 LRSRSTNAM AS lab_result, LRSRSTUM AS unit, LRSRSTNOR AS standard_result
 FROM LABRSTV5PF 
 INNER JOIN LABTABV5PF ON LBTLABTYP = LRSLABTYP AND LBTLABCOD = LRSLABCOD
 INNER JOIN LABRQOV5PF ON LRQDOCNO = LRSDOCNO    
        where CAST(LRQVN AS VARCHAR) + RIGHT('00' + CAST(LRQVNSEQ AS VARCHAR), 2) ='${seq}' and   [dbo].[Change_DateYMD2](LRQRQODTE) = '${dateServe}'  and LRQHN =  '${hn}' 
         `);
        return data;
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT OAPHN AS HN,CAST(OAPVN AS VARCHAR) + RIGHT('00' + CAST(OAPSEQNO AS VARCHAR), 2) AS seq, 
	[dbo].[Change_DateYMD2](OAPREGDTE) AS date_serv, [dbo].[Change_DateYMD2](DAPAPPDTE) AS date, [dbo].[FormatTime](DAPFRMTIM) AS time,RTBTABNAM AS department, 
	CAST(DAPDRORD1 AS CHAR) + CAST(DAPDRORD2 AS CHAR) + CAST(DAPDRORD3 AS CHAR) + CAST(DAPDRORD4 AS CHAR) + CAST(DAPDRORD5 AS CHAR) + CAST(DAPDRORD6 AS CHAR) AS detail, 
	[dbo].[FormatTime](DAPRQOTIM) AS time_serv
	FROM DRAPPV5PF
	INNER JOIN OPDAPPV5PF ON OAPHN = DAPAPPHN AND OAPREGDTE = DAPRQODTE
	INNER JOIN REGTABV5PF ON RTBTABCOD = DAPRQODIV AND RTBTABTYP = '01'
	where CAST(OAPVN AS VARCHAR) + RIGHT('00' + CAST(OAPSEQNO AS VARCHAR), 2) = '${seq}'  and dapapphn = '${seq}' and [dbo].[Change_DateYMD2](OAPREGDTE) =  '${dateServ}'
        `);
        return data;
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        select date_serv, time_serv, vaccine_code, vaccine_name
        from smartrefer_vaccines
        where hn='${hn}'`);
        return data;
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT MDSHN AS pid, MDSVN AS seq, [dbo].[Change_DateYMD2](OAPREGDTE) AS date_serv, [dbo].[FormatTime](OAPRQOTIM) AS time_serv, 
	MDSDSECOD AS procedure_code, TDSNAMENG AS procedure_name,
	ISNULL([dbo].[Change_DateYMD2](OMSINDTE),0) AS start_date, ISNULL([dbo].[FormatTime](OMSINTIM),0) AS start_time,
	ISNULL([dbo].[Change_DateYMD2](OMSOUTDTE),0) AS end_date, ISNULL([dbo].[FormatTime](OMSOUTTIM),0) AS end_time
	FROM MDRDSEV5PF
	INNER JOIN OPDAPPV5PF ON OAPHN = MDSHN AND OAPREGDTE = MDSINDTE AND MDSVN = RTRIM(CAST(OAPVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(OAPSEQNO AS CHAR)),2)
	INNER JOIN TABDSEV5PF ON TDSDSECOD = MDSDSECOD AND TDSDSEICD = '09'
	INNER JOIN ORCMASV5PF ON OMSHN = MDSHN AND OAPREGDTE = OMSINDTE
	WHERE MDSDOCTYP = '06' AND MDSVN <> 0 AND MDSDSECOD <> ''
        where MDSVN = '${seq}' and [dbo].[Change_DateYMD2](OAPREGDTE) = '${dateServe} and OAPHN = '${hn}
        `);
        return data;
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
         SELECT RTRIM(OAPVN) AS seq ,[dbo].[Change_DateYMD2](OAPREGDTE) AS date_serv , [dbo].[FormatTime](OAPRQOTIM) AS time_serv
	,RTRIM(RMSBLDGRP) AS bloodgrp
	,RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'06')) AS [weight]
	,RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'07')) AS height
	,RTRIM([dbo].[GET_BMI]([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'06'),[dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'07'))) AS bmi 
	,RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'05')) AS temperature
	,RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'03')) AS pr
	,RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'04')) AS rr
	,CASE
		WHEN RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')) = '' THEN ''
		ELSE LEFT(RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')),CHARINDEX('/',RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')))-1) 
	END AS sbp

	,CASE
		WHEN RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')) = '' THEN ''
		ELSE RIGHT(RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')),LEN(RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')))- CHARINDEX('/',RTRIM([dbo].[GET_OPDOTH](OAPREGDTE,OAPHN,'01')))) 
	END AS dbp
	,RTRIM(OAQFSTDIA) AS symptom
	,RTRIM(OAPDIVCOD) AS depcode
	,ISNULL(RTRIM((SELECT RTRIM(RTBTABNAM) FROM REGTABV5PF WHERE RTBTABTYP = '01' AND RTBTABCOD = OAPDIVCOD)),'') AS department
	,'' as movement_score
	,'' as vocal_score
	,'' as eye_score
	,'' as oxygen_sat
	,'' as gak_coma_sco
	,ISNULL(RTRIM((SELECT TOP 1 DDADIAG FROM DRDIAV5PF WHERE DDADTE = OAPREGDTE AND DDAHN = OAPHN AND DDAVN = OAPVN AND DDASEQNO = OAPSEQNO AND DDADOCTYP = '99')),'') AS diag_text
	,'' as pupil_right
	,'' as pupil_left
	FROM OPDAPPV5PF 
	INNER JOIN REGMASV5PF ON OAPHN = RMSHNREF
        WHERE OAPCRDSTS <> 'C'  and RTRIM(OAPVN) = '${seq}' and OAPREGDTE = '${dateServe}' and OAPHN = '${hn}'
        `);
        return data;
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT CAST(PRQVN AS VARCHAR) +  RIGHT('00' + CAST(PRQVNSEQ AS VARCHAR), 2) AS seq , PTTPTSNAM AS pe
	FROM PTSRQOV5PF 
	INNER JOIN PTSRQDV5PF ON PRQHN = PTDHN AND PRQDOCNO = PTDDOCNO
	INNER JOIN PTSTABV5PF ON PTTPTSTYP = PTDPTSTYP AND PTTPTSCOD = PTDPTSCOD
	WHERE PRQSTSFLG >= '2'  and  CAST(PRQVN AS VARCHAR) +  RIGHT('00' + CAST(PRQVNSEQ AS VARCHAR), 2) = '${seq}'
        and PRQRQODTE = '${dateServe}'  and  PRQHN = '${hn}'
        `);
        return data;
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) { 
        let data = await db.raw(`
        SELECT CAST(OAQVN AS VARCHAR) + '01' AS seq , OAQDIANAM AS hpi 
        FROM OPDAPQV5PF
        WHERE CAST(OAQVN AS VARCHAR) + '01' = '${seq}' and OAQREGDTE = '${dateServe}' and OAQHN ='${hn}'
        `);
        return data;
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT [dbo].[Change_DateYMD2](XRQRQODTE) AS xray_date, XTBXCUNAM AS xray_name
	FROM XCURQOV5PF 
	INNER JOIN XCURQDV5PF ON XRDDOCNO = XRQDOCNO AND XRDHN = XRQHN AND XRDXCUTYP = XRQXCUTYP
	INNER JOIN XCUTABV5PF ON XTBXCUTYP = XRDXCUTYP AND XTBXCUPRT = XRDXCUPRT AND XTBXCUCOD = XRDXCUCOD AND XTBXCUPOS = XRDXCUPOS
	WHERE XRQSTSFLG >= '2' and  CAST(xrqvn AS VARCHAR) +  RIGHT('00' + CAST(XRQVNSEQ AS VARCHAR), 2) = '${seq}' and  XRQHN  ='${hn}' and XRQRQODTE ='${dateServe}'
        `);
        return data;
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        SELECT DISTINCT RMMWARD AS ward_code 
	,RTRIM((SELECT RCFCFGNAM FROM ROMCFGV5PF WHERE RCFCFGTYP = 'W' AND RCFCFGCOD = RMMWARD)) AS ward_name
	,[dbo].[GET_ROMMAS_CT](RMMWARD,'OC')AS  ward_pt
	,RTRIM((SELECT RCFQTYBED FROM ROMCFGV5PF WHERE RCFCFGTYP = 'W' AND RCFCFGCOD = RMMWARD)) AS ward_bed
	,RTRIM((SELECT RCFQTYBED FROM ROMCFGV5PF WHERE RCFCFGTYP = 'W' AND RCFCFGCOD = RMMWARD)) AS ward_standard
	FROM ROMMASV5PF 
	WHERE RMMWARD <> ''  AND RMMACTFLG = ''        
        `);
        return data;
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        SELECT CAST(RTRIM(CAST(IPDVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(IPDSEQNO AS CHAR)),2) AS decimal) AS seq 
	,IPDHN AS hn ,IPDAN AS an 
	,RTRIM((SELECT TPRPRENAM FROM TABPREV5PF WHERE TPRCODREF = RMSPRENAM)) AS title_name
	,RTRIM(RMSNAME) AS  first_name ,RTRIM(RMSSURNAM) AS  last_name
	,IPDDOCNO as referno
	,[dbo].[Change_DateYMD2](IPDDTE) AS referdate 
	,'' AS location_name
	,RTRIM(IPDHOSTO) AS to_hcode
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'))
	END AS pttype_id
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'),''))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'),''))
	END AS pttype_name
	,'' AS strength_id ,RTRIM(IPDHOSTO) AS to_hcode_name 
	,RTRIM(IPDCAU) AS refer_cause
	,ISNULL([dbo].[FormatTime](IPDSECTIM),0) AS  refertime
	,'' AS doctor ,'' AS  doctor_name 
	FROM IPDREFV5PF 
	INNER JOIN REGMASV5PF ON RMSHNREF = IPDHN
        where [dbo].[Change_DateYMD2](IPDDTE) between '${start_date}' and '${end_date}'`);
	return data;
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
         SELECT CAST(RTRIM(CAST(IPDVN AS CHAR)) + RIGHT(RTRIM('0'+CAST(IPDSEQNO AS CHAR)),2) AS decimal) AS seq 
	,IPDHN AS hn ,IPDAN AS an 
	,RTRIM((SELECT TPRPRENAM FROM TABPREV5PF WHERE TPRCODREF = RMSPRENAM)) AS title_name
	,RTRIM(RMSNAME) AS  first_name ,RTRIM(RMSSURNAM) AS  last_name
	,IPDDOCNO as referno
	,[dbo].[Change_DateYMD2](IPDDTE) AS referdate 
	,'' AS location_name
	,RTRIM(IPDHOSTO) AS to_hcode
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'))
	END AS pttype_id
	,CASE
		WHEN IPDAN <> 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](0,IPDHN,IPDAN,0,0,'T'),''))
		WHEN IPDAN = 0 THEN RTRIM([dbo].[GET_CONTYPCONCOD_NAM]([dbo].[GET_CONTYPCONCOD_IO](IPDDOCDTE,IPDHN,0,IPDVN,IPDSEQNO,'T'),''))
	END AS pttype_name
	,'' AS strength_id ,RTRIM(IPDHOSTO) AS to_hcode_name 
	,RTRIM(IPDCAU) AS refer_cause
	,ISNULL([dbo].[FormatTime](IPDSECTIM),0) AS  refertime
	,'' AS doctor ,'' AS  doctor_name 
	FROM IPDREFV5PF 
	INNER JOIN REGMASV5PF ON RMSHNREF = IPDHN
        where [dbo].[Change_DateYMD2](IPDDTE) between '${start_date}' and '${end_date}'`);
        return data;
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        SELECT seq, receive_apppoint_date, receive_apppoint_beginttime,
        receive_apppoint_endtime, receive_apppoint_doctor, receive_apppoint_chinic, receive_text,
        receive_by
        from smartrefer_appoint
        WHERE hn ='${hn}' and receive_apppoint_date = '${app_date}'
        `);
        return data;
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`
          SELECT RTBTABCOD AS dep_code, RTBTABNAM 
          FROM REGTABV5PF 
          WHERE RTBTABTYP = '01'
        `);
        return data;

    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        SELECT DISTINCT RMSHNREF 
         FROM REGMASV5PF
         where RMSIDNO = '${cid}'
        `);
        return data;
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        select '' as hn, '' as drug_hospcode ,'' as drug_hospname,'' as drug_name,'' as drug_use,'' as drug_receive_date 
        `);
        return data;
    }

    async getServicesCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT hn,
        seq, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE hn = '${hn}'
        ORDER BY seq DESC limit 3
        
        `);
        return data;
    }

    async getProfileCoc(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select hn, cid, title_name,first_name, last_name,
        moopart, addrpart, tmbpart, amppart, chwpart, brthdate
        , age, pttype_id, pttype_name, pttype_no, hospmain
        , hospmain_name, hospsub, hospsub_name, registdate, visitdate
        , father_name, mother_name, couple_name, contact_name,contact_relation, contact_mobile
        FROM smartrefer_person
        WHERE hn ='${hn}'`);
        return data;
    }

}
