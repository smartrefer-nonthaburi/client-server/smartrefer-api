import Knex = require('knex');
const hospcode = process.env.HIS_CODE;
var md5 = require('md5');

export class HisHosxpv3Model {

  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data =  await db('opduser as o')
        .select('o.loginname as username', 'o.name as fullname')
        .select(db('opdconfig').first('hospitalcode').as('hcode').limit(1)) 
        .where('o.loginname', username)
        .andWhere('o.passweb', pass);
        return data;
  }

  async getServices(db: Knex, hn: any, seq: any) {
    // First query
    const query1 = db('ovst as o')
      .select(
        'o.vn as seq',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        db.raw("DATE_FORMAT(o.vstdate, '%Y-%m-%d') as date_serv"),
        db.raw("DATE_FORMAT(o.vsttime, '%h:%i:%s') as time_serv"),
        'c.department'
      )
      .innerJoin('kskdepartment as c', 'c.depcode', 'o.main_dep')
      .innerJoin('patient as p', 'p.hn', 'o.hn')
      .where('o.hn', hn)
      .andWhere('o.vn', seq);
  
    // Second query
    const query2 = db('an_stat as an')
      .select(
        'an.an as seq',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        db.raw("DATE_FORMAT(i.regdate, '%Y-%m-%d') as date_serv"),
        db.raw("DATE_FORMAT(i.regtime, '%h:%i:%s') as time_serv"),
        'w.name as department'
      )
      .innerJoin('ward as w', 'w.ward', 'an.ward')
      .innerJoin('patient as p', 'p.hn', 'an.hn')
      .innerJoin('ipt as i', 'i.an', 'an.an')
      .where('an.hn', hn)
      .andWhere('an.an', seq);
  
    // Combine both queries using UNION
    const data = await db.union([query1, query2]);
  
    return data;
  }

  async getProfile(db:Knex, hn:any, seq:any, referno:any) {
    const data = await db('ovst as o')
      .select(
        'p.hn as hn',
        'p.cid as cid',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        's.name as sex',
        'oc.name as occupation',
        db.raw(`IF((p.moopart = "" OR p.moopart = '-' OR p.moopart IS NULL), "00", IF(CHAR_LENGTH(p.moopart) < 2, LPAD(p.moopart, 2, "0"), p.moopart)) AS moopart`),
        'p.addrpart',
        'p.tmbpart',
        'p.amppart',
        'p.chwpart',
        'p.birthday as brthdate',
        db.raw(`CONCAT(LPAD(vn.age_y, 3, "0"), '-', LPAD(vn.age_m, 2, "0"), '-', LPAD(vn.age_d, 2, "0")) as age`),
        'o.pttype as pttype_id',
        't.name as pttype_name',
        'o.pttypeno as pttype_no',
        'o.hospmain',
        'c.name as hospmain_name',
        'o.hospsub',
        'h.name as hospsub_name',
        'p.firstday as registdate',
        'p.last_visit as visitdate',
        'p.fathername as father_name',
        'p.mathername as mother_name',
        'p.informrelation as contact_name',
        'p.informtel as contact_mobile',
        'oc.name as occupation'
      )
      .innerJoin('patient as p', 'p.hn', 'o.hn')
      .innerJoin('vn_stat as vn', 'vn.vn', 'o.vn')
      .leftJoin('pttype as t', 'o.pttype', 't.pttype')
      .leftJoin('hospcode as c', 'o.hospmain', 'c.hospcode')
      .leftJoin('hospcode as h', 'o.hospsub', 'h.hospcode')
      .leftJoin('sex as s', 's.code', 'p.sex')
      .leftJoin('occupation as oc', 'oc.occupation', 'p.occupation')
      .where('o.hn', hn)
      .orderBy('o.vstdate', 'desc')
      .limit(1); // ใช้ .first() เพื่อรับเฉพาะแถวแรก (เทียบเท่ากับ LIMIT 1)
  
    return data;
  }

  async  getHospital(db: Knex, hn: any) {
    const data = await db('opdconfig')
      .select(
        'hospitalcode as provider_code',
        'hospitalname as provider_name'
      )
      .limit(1); // ใช้ .first() เพื่อจำกัดผลลัพธ์เป็นแถวแรก (เทียบเท่ากับ LIMIT 1)
  
    return data;
  }

  async  getAllergyDetail(db: Knex, hn: any, referno: any) {
    const data = await db('opd_allergy')
      .select(
        db.raw(`REPLACE(agent, "\r\n", " ") as drug_name`),
        db.raw(`REPLACE(symptom, "\r\n", " ") as symptom`),
        'begin_date as begin_date',
        db.raw(`DATE(entry_datetime) as daterecord`)
      )
      .where('hn', hn);
  
    return data;
  }

  async  getChronic(db: Knex, hn: any, referno: any) {
    const data = await db('clinicmember as cm')
      .select(
        'c.icd10 as icd_code',
        'cm.regdate as start_date',
        db.raw(`IF(i.tname != '', i.tname, "-") as icd_name`)
      )
      .innerJoin('clinic as c', 'cm.clinic', 'c.clinic')
      .innerJoin('icd101 as i', 'i.code', 'c.icd10')
      .where('cm.hn', hn);
  
    return data;
  }

  async  getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    // Query 1: Diagnosis from `ovst`
    const query1 = db('ovst as o')
      .select(
        'o.vn as seq',
        db.raw(`DATE_FORMAT(o.vstdate, '%Y-%m-%d') as date_serv`),
        db.raw(`TIME(o.vsttime) as time_serv`),
        db.raw(
          `IF((d.icd10 = "" OR d.icd10 IS NULL), (SELECT ro.pdx FROM referout ro WHERE ro.vn = o.vn AND d.vn = ? LIMIT 1), d.icd10) as icd_code`,
          [seq]
        ),
        db.raw(`REPLACE(i.name, "\r\n", " ") as icd_name`),
        'd.diagtype as diag_type',
        db.raw(
          `IF((dt.provisional_dx_text = "" OR dt.provisional_dx_text IS NULL), (SELECT ro.pre_diagnosis FROM referout ro WHERE ro.vn = o.vn AND d.vn = ? LIMIT 1), dt.provisional_dx_text) as DiagNote`,
          [seq]
        ),
        db.raw(`'' as diagtype_id`)
      )
      .innerJoin('vn_stat as v', 'v.vn', 'o.vn')
      .innerJoin('ovstdiag as d', 'd.vn', 'o.vn')
      .innerJoin('icd101 as i', 'i.code', 'd.icd10')
      .innerJoin('referout as ro', 'ro.vn', 'o.vn')
      .leftJoin('ovstdiag_text as dt', 'o.vn', 'dt.vn')
      .where('d.vn', seq)
      .andWhereRaw('(o.an = "" OR o.an IS NULL)');
  
    // Query 2: Diagnosis from `ipt`
    const query2 = db('ipt as i')
      .select(
        'i.an as seq',
        db.raw(`DATE_FORMAT(i.dchdate, '%Y-%m-%d') as date_serv`),
        db.raw(`TIME(i.dchtime) as time_serv`),
        'r.icd10 as icd_code',
        db.raw(`REPLACE(c.name, "\r\n", " ") as icd_name`),
        'r.diagtype as diag_type',
        'c.name as DiagNote',
        'r.diagtype as diagtype_id'
      )
      .innerJoin(
        db
          .select(
            'an',
            'icd10',
            'diagtype',
            db.raw(`'ipdx' as f`)
          )
          .from('iptdiag as id')
          .where('id.an', seq)
          .unionAll(
            db
              .select(
                'vn as an',
                'pdx as icd10',
                db.raw(`'1' as diagtype`),
                db.raw(`'referdx' as f`)
              )
              .from('referout as ro')
              .where('ro.vn', seq)
          )
          .as('r'),
        'r.an',
        'i.an'
      )
      .innerJoin('icd101 as c', 'r.icd10', 'c.code')
      .whereRaw(
        `IF((SELECT COUNT(an) as total FROM (
          SELECT an, icd10, diagtype, 'ipdx' as f FROM iptdiag id WHERE id.an = ?
          UNION 
          SELECT vn as an, pdx as icd10, '1' as diagtype, 'referdx' as f FROM referout ro WHERE ro.vn = ?
        ) as r) > 1, r.f = 'ipdx', r.f = 'referdx')`,
        [seq, seq]
      );
  
    // Combine both queries with UNION
    const data = await db.unionAll([query1, query2]);
  
    return data;
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT a.seq,a.an,a.pid,a.hn,a.referno,a.referdate,a.to_hcode,a.pttype_id,a.strength_id,a.strength_name,a.location_name,a.station_name,a.to_hcode_name,a.refer_cause,a.refertime,a.doctor,a.doctor_name
    FROM (
    select if(LENGTH(r.vn) >=  9,r.vn,o2.vn) as seq,
    o2.an as an,
    r.hn as pid,
    r.hn as hn,
    cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
    r.refer_date as referdate,
    r.refer_hospcode as to_hcode,
    if(r.referout_emergency_type_id='3',1,2) as pttype_id,
    r.referout_emergency_type_id as strength_id,
    (select name from refer_cause where id = r.refer_cause ) as strength_name,
    r.refer_point as location_name,
    r.refer_point as station_name,
    (select name from hospcode where hospcode = refer_hospcode) as to_hcode_name,
    (select name from refer_cause where id = refer_cause) as refer_cause,
    refer_time as refertime,
    (select concat(licenseno,',',cid)  from doctor  where code = r.doctor) as doctor,
    (select name  from doctor where code = r.doctor) as doctor_name,
    '' as refer_remark
    from referout as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.an
    where cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(LENGTH(r.vn) >=  9,r.vn,o2.vn))  = '${seq}'
    UNION
    select if(o.an is null,r.vn,o.an) as seq,
    o2.an as an,
    o.hn as pid,
    o.hn as hn,
    cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
      DATE(r.reply_date_time) as referdate,
    r.dest_hospcode as to_hcode,
    "" as pttype_id,
    "" as strength_id,
    "" as strength_name,
    "" as location_name,
    "" as station_name,
    (select name from hospcode where hospcode = r.dest_hospcode) as to_hcode_name,
    ""as refer_cause,
      time(r.reply_date_time) as refertime,
    (select concat(licenseno,',',cid) from doctor where (code = o.doctor or code = o2.doctor)) as doctor,
    (select name  from doctor where (code = o.doctor or code = o2.doctor)) as doctor_name,
    '' as refer_remark
    from refer_reply as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.vn
    where cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(o.an is null,r.vn,o.an))  = '${seq}'
    ) a
    WHERE a.seq is not NULL
    `);
    return data[0];
  }


  async getRefer_knex(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    // Query 1: Referout data
    const query1 = db('referout as r')
      .select(
        db.raw(`IF(LENGTH(r.vn) >= 9, r.vn, o2.vn) as seq`),
        'o2.an as an',
        'r.hn as pid',
        'r.hn as hn',
        db.raw(`CAST(REPLACE(r.refer_number, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno`),
        'r.refer_date as referdate',
        'r.refer_hospcode as to_hcode',
        db.raw(`IF(r.referout_emergency_type_id = '3', 1, 2) as pttype_id`),
        'r.referout_emergency_type_id as strength_id',
        db.raw(`(SELECT name FROM refer_cause WHERE id = r.refer_cause) as strength_name`),
        'r.refer_point as location_name',
        'r.refer_point as station_name',
        db.raw(`(SELECT name FROM hospcode WHERE hospcode = r.refer_hospcode) as to_hcode_name`),
        db.raw(`(SELECT name FROM refer_cause WHERE id = r.refer_cause) as refer_cause`),
        'r.refer_time as refertime',
        db.raw(`(SELECT CONCAT(licenseno, ",", cid) FROM doctor WHERE code = r.doctor) as doctor`),
        db.raw(`(SELECT name FROM doctor WHERE code = r.doctor) as doctor_name`),
        db.raw(`'' as refer_remark`)
      )
      .leftJoin('ovst as o', 'r.vn', 'o.vn')
      .leftJoin('ovst as o2', 'r.vn', 'o2.an')
      .whereRaw(`CAST(REPLACE(r.refer_number, "/", "-") AS CHAR(20) CHARACTER SET UTF8) = ?`, [referno])
      .andWhereRaw(`IF(LENGTH(r.vn) >= 9, r.vn, o2.vn) = ?`, [seq]);
  
    // Query 2: Refer reply data
    const query2 = db('refer_reply as r')
      .select(
        db.raw(`IF(o.an IS NULL, r.vn, o.an) as seq`),
        'o2.an as an',
        'o.hn as pid',
        'o.hn as hn',
        db.raw(`CAST(REPLACE(r.doc_no, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno`),
        db.raw(`DATE(r.reply_date_time) as referdate`),
        'r.dest_hospcode as to_hcode',
        db.raw(`"" as pttype_id`),
        db.raw(`"" as strength_id`),
        db.raw(`"" as strength_name`),
        db.raw(`"" as location_name`),
        db.raw(`"" as station_name`),
        db.raw(`(SELECT name FROM hospcode WHERE hospcode = r.dest_hospcode) as to_hcode_name`),
        db.raw(`"" as refer_cause`),
        db.raw(`TIME(r.reply_date_time) as refertime`),
        db.raw(
          `(SELECT CONCAT(licenseno, ",", cid) FROM doctor WHERE (code = o.doctor OR code = o2.doctor)) as doctor`
        ),
        db.raw(
          `(SELECT name FROM doctor WHERE (code = o.doctor OR code = o2.doctor)) as doctor_name`
        ),
        db.raw(`'' as refer_remark`)
      )
      .leftJoin('ovst as o', 'r.vn', 'o.vn')
      .leftJoin('ovst as o2', 'r.vn', 'o2.vn')
      .whereRaw(`CAST(REPLACE(r.doc_no, "/", "-") AS CHAR(20) CHARACTER SET UTF8) = ?`, [referno])
      .andWhereRaw(`IF(o.an IS NULL, r.vn, o.an) = ?`, [seq]);
  
    // Combine queries with UNION as a subquery
    const unionQuery = db.unionAll([query1, query2]).as('a');
  
    // Final query applying `whereNotNull` to the subquery
    const data = await db
      .select('*')
      .from(unionQuery)
      .whereNotNull('seq');
  
    return data;
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    const query1 = db('ovst as o')
      .select(
        'o.vn as seq',
        db.raw(`DATE_FORMAT(i.rxdate, '%Y%m%d') as date_serv`),
        db.raw(`DATE_FORMAT(i.rxtime, '%h:%i:%s') as time_serv`),
        'd.name as drug_name',
        'i.qty as qty',
        'd.units as unit',
        db.raw(`
          IF(
            g.name1 IS NOT NULL,
            g.name1,
            s.name1
          ) as usage_line1
        `),
        db.raw(`
          IF(
            g.name2 IS NOT NULL,
            g.name2,
            s.name2
          ) as usage_line2
        `),
        db.raw(`
          IF(
            g.name3 IS NOT NULL,
            g.name3,
            s.name3
          ) as usage_line3
        `)
      )
      .leftJoin('opitemrece as i', 'o.vn', 'i.vn')
      .leftJoin('drugitems as d', 'i.icode', 'd.icode')
      .leftJoin('income as ic', 'i.income', 'ic.income')
      .leftJoin('drugusage as g', 'g.drugusage', 'i.drugusage')
      .leftJoin('sp_use as s', 's.sp_use', 'i.sp_use')
      .whereNull('o.an')
      .andWhere('i.vn', seq)
      .andWhere(function () {
        this.whereIn('i.income', [3, 4]) // หาก income เป็นตัวเลข
          .orWhere(db.raw('ic.group1 = "3"'));
      });
  
    const query2 = db('ovst as o')
      .select(
        'o.vn as seq',
        db.raw(`DATE_FORMAT(i.rxdate, '%Y%m%d') as date_serv`),
        db.raw(`DATE_FORMAT(i.rxtime, '%h:%i:%s') as time_serv`),
        'd.name as drug_name',
        'i.qty as qty',
        'd.units as unit',
        db.raw(`
          IF(
            g.name1 IS NOT NULL,
            g.name1,
            s.name1
          ) as usage_line1
        `),
        db.raw(`
          IF(
            g.name2 IS NOT NULL,
            g.name2,
            s.name2
          ) as usage_line2
        `),
        db.raw(`
          IF(
            g.name3 IS NOT NULL,
            g.name3,
            s.name3
          ) as usage_line3
        `)
      )
      .leftJoin('opitemrece as i', 'o.an', 'i.an')
      .leftJoin('drugitems as d', 'i.icode', 'd.icode')
      .leftJoin('income as ic', 'i.income', 'ic.income')
      .leftJoin('drugusage as g', 'g.drugusage', 'i.drugusage')
      .leftJoin('sp_use as s', 's.sp_use', 'i.sp_use')
      .whereNotNull('i.an')
      .andWhereRaw('i.vn IS NULL')
      .andWhere('o.vn', seq)
      .andWhere(function () {
        this.whereIn('i.income', [3, 4]) // หาก income เป็นตัวเลข
          .orWhere(db.raw('ic.group1 = "3"'));
      });
  
    const unionQuery = db
      .raw(`(${query1.toString()}) UNION ALL (${query2.toString()})`);
  
    const data = await db
      .select(
        'a.seq',
        db.raw('MIN(a.date_serv) as date_serv'),
        'a.time_serv',
        db.raw('REPLACE(a.drug_name, "\r\n", " ") as drug_name'),
        db.raw('SUM(a.qty) as qty'),
        'a.unit',
        db.raw('REPLACE(a.usage_line1, "\r\n", " ") as usage_line1'),
        db.raw('REPLACE(a.usage_line2, "\r\n", " ") as usage_line2'),
        db.raw('REPLACE(a.usage_line3, "\r\n", " ") as usage_line3')
      )
      .from(db.raw(`(${unionQuery}) as a`))
      .groupBy('a.drug_name');
  
    return data;
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    const data = await db('ovst as o')
      .select(
        'lh.receive_date as date_serv',
        'lh.receive_time as time_serv',
        'lh.form_name as labgroup',
        'lo.lab_items_name_ref as lab_name',
        db.raw(`REPLACE(lo.lab_order_result, "\r\n", " ") as lab_result`),
        'li.lab_items_unit as unit',
        db.raw(`
          IF(
            lo.lab_items_normal_value_ref = "",
            IF(
              (li.lab_items_normal_value IS NULL OR li.lab_items_normal_value = ''),
              "is null",
              li.lab_items_normal_value
            ),
            lo.lab_items_normal_value_ref
          ) as standard_result
        `)
      )
      .innerJoin('lab_head as lh', function () {
        this.on('o.vn', '=', 'lh.vn').orOn('o.an', '=', 'lh.vn');
      })
      .innerJoin('lab_order as lo', 'lh.lab_order_number', 'lo.lab_order_number')
      .innerJoin('lab_items as li', 'li.lab_items_code', 'lo.lab_items_code')
      .where('lh.vn', seq)
      //.andWhereNotNull('lo.lab_order_result');
  
    return data;
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
    const data = await db('oapp as o')
      .select(
        'o.vn as seq',
        'o.vstdate as date_serv',
        'o.nextdate as date',
        'o.nexttime as time',
        db.raw('(SELECT name FROM clinic WHERE clinic = o.clinic) as department'),
        'o.app_cause as detail',
        db.raw('TIME(ovst.vsttime) as time_serv')
      )
      .innerJoin('ovst', 'ovst.vn', 'o.vn')
      .where('o.vn', seq);
  
    return data;
  }

  async getVaccine(db: Knex, hn: any, referno: any) {
    const data = await db('ovst_vaccine as ov')
      .select(
        'o.vstdate as date_serv',
        'o.vsttime as time_serv',
        'pv.export_vaccine_code as vaccine_code',
        'pv.vaccine_name as vaccine_name'
      )
      .leftJoin('person_vaccine as pv', 'ov.person_vaccine_id', 'pv.person_vaccine_id')
      .leftJoin('ovst as o', 'o.vn', 'ov.vn')
      .where('o.hn', hn);
  
    return data;
  }

  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
  // Query 1: Procedure data from ovstdiag
  const query1 = db('ovst as o')
    .select(
      'o.hn as pid',
      'o.vn as seq',
      'o.vstdate as date_serv',
      'o.vsttime as time_serv',
      'od.icd10 as procedure_code',
      db.raw('REPLACE(ic.name, "\r\n", " ") as procedure_name'),
      db.raw('DATE_FORMAT(od.vstdate, "%Y%m%d") as start_date'),
      db.raw('DATE_FORMAT(od.vsttime, "%h:%i:%s") as start_time'),
      db.raw('DATE_FORMAT(od.vstdate, "%Y%m%d") as end_date'),
      db.raw('"00:00:00" as end_time')
    )
    .leftJoin('ovstdiag as od', 'od.vn', 'o.vn')
    .innerJoin('icd9cm1 as ic', 'ic.code', 'od.icd10')
    .where('o.vn', seq)
    .groupBy('o.vn', 'od.icd10');

  // Query 2: Procedure data from iptoprt
  const query2 = db('ovst as o')
    .select(
      'o.hn as pid',
      'o.vn as seq',
      'o.vstdate as date_serv',
      'o.vsttime as time_serv',
      'i.icd9 as procedure_code',
      db.raw('REPLACE(c.name, "\r\n", " ") as procedure_name'),
      db.raw('DATE_FORMAT(i.opdate, "%Y%m%d") as start_date'),
      db.raw('DATE_FORMAT(i.optime, "%h:%i:%s") as start_time'),
      db.raw('DATE_FORMAT(i.enddate, "%Y%m%d") as end_date'),
      db.raw('"00:00:00" as end_time')
    )
    .innerJoin('iptoprt as i', 'i.an', 'o.an')
    .innerJoin('icd9cm1 as c', 'c.code', 'i.icd9')
    .where('o.vn', seq)
    .groupBy('o.vn', 'i.icd9');

  // Combine both queries with UNION
  const data = await db.unionAll([query1, query2]);

  return data;
}

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    // Query 1: Data from `ovst`
    const query1 = db('ovst as o')
      .select(
        'o.vn as seq',
        'o.hn',
        db.raw(`DATE_FORMAT(o.vstdate, '%Y-%m-%d') as date_serv`),
        db.raw(`DATE_FORMAT(o.vsttime, '%h:%i:%s') as time_serv`),
        'p.bloodgrp as bloodgrp',
        's.bw as weight',
        's.height as height',
        's.bmi as bmi',
        's.temperature as temperature',
        's.pulse as pr',
        's.rr as rr',
        's.bps as sbp',
        's.bpd as dbp',
        db.raw(`REPLACE(s.cc, "\r\n", " ") as symptom`),
        'o.main_dep as depcode',
        'k.department as department',
        'n.gcs_m as movement_score',
        'n.gcs_v as vocal_score',
        'n.gcs_e as eye_score',
        'n.o2sat as oxygen_sat',
        db.raw('(n.gcs_e + n.gcs_v + n.gcs_m) as gak_coma_sco'),
        db.raw(`
          IF(
            ovstdiag_text.provisional_dx_text = "" OR ovstdiag_text.provisional_dx_text IS NULL,
            ro.pre_diagnosis,
            ovstdiag_text.provisional_dx_text
          ) as diag_text
        `),
        db.raw(`'' as pupil_right`),
        db.raw(`'' as pupil_left`),
        db.raw(`REPLACE(ro.pmh, "\r\n", " ") as pmh`)
      )
      .leftJoin('opdscreen as s', 'o.vn', 's.vn')
      .leftJoin('kskdepartment as k', 'k.depcode', 'o.main_dep')
      .innerJoin('patient as p', 'p.hn', 'o.hn')
      .leftJoin('referout as ro', 'ro.vn', 'o.vn')
      .leftJoin('er_nursing_detail as n', 'o.vn', 'n.vn')
      .leftJoin('ovstdiag_text', 'ovstdiag_text.vn', 'o.vn')
      .where('o.vn', seq);
  
    // Query 2: Data from `ipt`
    const query2 = db('ipt as o')
      .select(
        'o.vn as seq',
        'o.hn',
        db.raw(`DATE_FORMAT(o.regdate, '%Y-%m-%d') as date_serv`),
        db.raw(`DATE_FORMAT(o.regtime, '%h:%i:%s') as time_serv`),
        'p.bloodgrp as bloodgrp',
        's.bw as weight',
        's.height as height',
        's.bmi as bmi',
        's.temperature as temperature',
        's.pulse as pr',
        's.rr as rr',
        's.bps as sbp',
        's.bpd as dbp',
        db.raw(`REPLACE(REPLACE(s.cc, CHAR(13), ' '), CHAR(10), ' ') as symptom`),
        db.raw(`"" as depcode`),
        db.raw(`"" as department`),
        'n.gcs_m as movement_score',
        'n.gcs_v as vocal_score',
        'n.gcs_e as eye_score',
        'n.o2sat as oxygen_sat',
        db.raw('(n.gcs_e + n.gcs_v + n.gcs_m) as gak_coma_sco'),
        db.raw(`
          IF(
            ovstdiag_text.provisional_dx_text = "" OR ovstdiag_text.provisional_dx_text IS NULL,
            (SELECT ro.pre_diagnosis FROM referout ro WHERE ro.vn = o.vn AND o.an = ? LIMIT 1),
            ovstdiag_text.provisional_dx_text
          ) as diag_text
        `, [seq]),
        db.raw(`'' as pupil_right`),
        db.raw(`'' as pupil_left`),
        db.raw(`REPLACE(ro.pmh, "\r\n", " ") as pmh`)
      )
      .leftJoin('opdscreen as s', 'o.vn', 's.vn')
      .innerJoin('patient as p', 'p.hn', 'o.hn')
      .leftJoin('referout as ro', 'ro.vn', 'o.an')
      .leftJoin('er_nursing_detail as n', 'o.vn', 'n.vn')
      .leftJoin('ovstdiag_text', 'ovstdiag_text.vn', 'o.vn')
      .where('o.an', seq);
  
    // Combine both queries with UNION
    const data = await db.unionAll([query1, query2]);
  
    return data;
  }

  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    // Query 1: Physical examination from opdscreen
    const query1 = db('opdscreen as o')
      .select(
        'o.vn as seq',
        db.raw(`REPLACE(o.pe, "\r\n", " ") as pe`)
      )
      .where('o.vn', seq);
  
    // Query 2: Physical examination from ipt_discharge_note
    const query2 = db('ipt_discharge_note as d')
      .select(
        'i.vn as seq',
        db.raw(`REPLACE(d.note, "\r\n", " ") as pe`)
      )
      .leftJoin('ipt as i', 'i.an', 'd.an')
      .where('i.vn', seq);
  
    // Combine both queries with UNION ALL
    const unionQuery = db
      .unionAll([query1, query2])
      .as('v'); // ตั้งชื่อ subquery ว่า 'v'
  
    // Aggregate results by seq
    const data = await db
      .select(
        'v.seq',
        db.raw('GROUP_CONCAT(v.pe) as pe')
      )
      .from(unionQuery)
      .groupBy('v.seq'); // Group results by seq
  
    return data;
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    const query1 = db('referout as o')
      .select(
        'o.vn as seq',
        db.raw(`REPLACE(o.hpi, "\r\n", " ") as hpi`)
      )
      .where('o.vn', seq);
  
    const query2 = db('opdscreen as o')
      .select(
        'o.vn as seq',
        db.raw(`REPLACE(o.hpi, "\r\n", " ") as hpi`)
      )
      .where('o.vn', seq);
  
    const unionQuery = db
      .unionAll([query1, query2]) // รวม query1 และ query2
      .as('x'); // ตั้งชื่อ subquery ว่า 'x'
  
    const data = await db
      .select(
        'x.seq',
        db.raw(`REPLACE(GROUP_CONCAT(DISTINCT x.hpi), ",", " ") as hpi`)
      )
      .from(unionQuery); // ใช้ subquery ที่ตั้งชื่อว่า 'x'
  
    return data;
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    // Query 1: X-ray data for VN
    const query1 = db('xray_report as x')
      .select(
        'x.request_date as xray_date',
        db.raw('REPLACE(i.xray_items_name, "\r\n", " ") as xray_name')
      )
      .leftJoin('xray_items as i', 'i.xray_items_code', 'x.xray_items_code')
      .where('x.confirm', 'Y')
      .andWhere('x.confirm_read_film', '<>', 'Y')
      .andWhere('x.vn', seq);
  
    // Query 2: X-ray data for AN
    const query2 = db('xray_report as x')
      .select(
        'x.request_date as xray_date',
        db.raw('REPLACE(i.xray_items_name, "\r\n", " ") as xray_name')
      )
      .leftJoin('xray_items as i', 'i.xray_items_code', 'x.xray_items_code')
      .where('x.confirm', 'Y')
      .andWhere('x.an', seq)
      .groupBy('x.xn');
  
    // Combine both queries with UNION
    const data = await db.unionAll([query1, query2]);
  
    return data;
  }

  async getBedsharing(db: Knex) {
    const data = await db('bedno as b')
      .select(
        'w.ward as ward_code',
        'w.name as ward_name',
        db.raw('COUNT(i.an) as ward_pt'),
        db.raw('COUNT(DISTINCT b.bedno) as ward_bed'),
        'w.bedcount as ward_standard'
      )
      .leftJoin('roomno as r', 'r.roomno', 'b.roomno')
      .leftJoin('ward as w', 'w.ward', 'r.ward')
      .leftJoin('iptadm as a', 'a.bedno', 'b.bedno')
      .leftJoin('ipt as i', function () {
        this.on('i.an', '=', 'a.an').andOnNull('i.dchdate');
      })
      .where(function () {
        this.whereNull('b.bed_status_type_id').orWhere('b.bed_status_type_id', 1);
      })
      .andWhere('w.ward_active', '<>', 'N')
      .groupBy('w.name');
  
    return data;
  }
  async getReferOut(db: Knex, start_date: any, end_date: any) {
    const data = await db('referout as r')
      .select(
        'r.vn as seq',
        'r.hn',
        'o2.an as an',
        'pt.pname as title_name',
        'pt.fname as first_name',
        'pt.lname as last_name',
        db.raw(`CAST(REPLACE(r.refer_number, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno`),
        'r.refer_date as referdate',
        'r.refer_hospcode as to_hcode',
        'r.refer_point as location_name',
        db.raw(`IF(rt.referout_type_id = '1', '1', '2') as pttype_id`),
        db.raw(`IF(rt.referout_type_id = '1', 'Non Trauma', 'Trauma') as typept_name`),
        'r.referout_emergency_type_id as strength_id',
        db.raw(`
          CASE r.referout_emergency_type_id
            WHEN 1 THEN 'Resucitate'
            WHEN 2 THEN 'Emergency'
            WHEN 3 THEN 'Urgency'
            WHEN 4 THEN 'Semi Urgency'
            WHEN 5 THEN 'Non Urgency'
            ELSE ''
          END as strength_name
        `),
        'h.name as to_hcode_name',
        'rr.name as refer_cause',
        db.raw(`TIME(r.refer_time) as refertime`),
        'd.licenseno as doctor',
        'd.name as doctor_name'
      )
      .innerJoin('patient as pt', 'pt.hn', 'r.hn')
      .leftJoin('ovst as o2', 'r.vn', 'o2.an')
      .leftJoin('referout_type as rt', 'rt.referout_type_id', 'r.referout_type_id')
      .leftJoin('doctor as d', 'd.code', 'r.doctor')
      .leftJoin('rfrcs as rr', 'rr.rfrcs', 'r.rfrcs')
      .leftJoin('hospcode as h', 'h.hospcode', 'r.refer_hospcode')
      .whereBetween('r.refer_date', [start_date, end_date])
      .where(function () {
        this.where('r.referout_type_id', '1')
          .orWhere('r.referout_type_id', '2')
          .orWhere('r.referout_type_id', '3');
      })
      .whereNotIn('r.refer_number', function () {
        this.select('refer_reply.doc_no')
          .from('refer_reply')
          .whereNotNull('refer_reply.doc_no')
          .andWhereBetween('refer_reply.reply_date_time', [start_date, end_date]);
      })
      .groupBy('referno');
  
    return data;
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    const query1 = db('refer_reply as r')
      .select(
        db.raw('IF(o.an IS NULL, r.vn, o.an) as seq'),
        'o.an as an',
        'pt.hn',
        'pt.pname as title_name',
        'pt.fname as first_name',
        'pt.lname as last_name',
        db.raw('CAST(REPLACE(r.doc_no, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno'),
        db.raw('DATE(r.reply_date_time) as referdate'),
        'r.dest_hospcode as to_hcode',
        'h.name as to_hcode_name',
        'rr.name as refer_cause',
        db.raw('TIME(r.reply_date_time) as refertime'),
        'd.licenseno as doctor',
        'd.name as doctor_name'
      )
      .innerJoin('ovst as o', 'o.vn', 'r.vn')
      .innerJoin('patient as pt', 'pt.hn', 'o.hn')
      .leftJoin('rfrcs as rr', 'rr.rfrcs', 'o.rfrics')
      .leftJoin('hospcode as h', 'h.hospcode', 'r.dest_hospcode')
      .leftJoin('doctor as d', 'd.code', 'r.reply_doctor')
      .whereRaw('DATE(r.reply_date_time) BETWEEN ? AND ?', [start_date, end_date]);
  
    const query2 = db('referout as r')
      .select(
        db.raw('(CASE WHEN o2.an IS NULL THEN r.vn ELSE o2.an END) as seq'),
        'o2.an as an',
        'pt.hn',
        'pt.pname as title_name',
        'pt.fname as first_name',
        'pt.lname as last_name',
        db.raw('CAST(REPLACE(r.refer_number, "/", "-") AS CHAR(20) CHARACTER SET UTF8) as referno'),
        'r.refer_date as referdate',
        'r.refer_hospcode as to_hcode',
        'h.name as to_hcode_name',
        db.raw(`'' as refer_cause`),
        'r.refer_date as refertime',
        'd.licenseno as doctor',
        'd.name as doctor_name'
      )
      .innerJoin('patient as pt', 'pt.hn', 'r.hn')
      .leftJoin('ovst as o2', 'r.vn', 'o2.an')
      .leftJoin('referout_type as rt', 'rt.referout_type_id', 'r.referout_type_id')
      .leftJoin('doctor as d', 'd.code', 'r.doctor')
      .leftJoin('rfrcs as rr', 'rr.rfrcs', 'r.rfrcs')
      .leftJoin('hospcode as h', 'h.hospcode', 'r.refer_hospcode')
      .whereBetween('r.refer_date', [start_date, end_date])
      .andWhere(function () {
        this.where('r.referout_type_id', '4')
          .orWhere('r.referout_type_id', '5')
          .orWhere('r.referout_type_id', '6');
      });
  
    const data = await db.unionAll([query1, query2]);
  
    return data;
  }

  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    const data = await db('oapp as a')
      .select(
        'a.vn as seq',
        'a.nextdate as receive_apppoint_date',
        'a.nexttime as receive_apppoint_beginttime',
        'a.endtime as receive_apppoint_endtime',
        'a.app_user as receive_apppoint_doctor',
        db.raw('GROUP_CONCAT(DISTINCT c.name) as receive_apppoint_chinic'),
        db.raw('GROUP_CONCAT(DISTINCT a.app_cause) as receive_text'),
        db.raw(`'' as receive_by`)
      )
      .leftJoin('clinic as c', 'c.clinic', 'a.clinic')
      .where('a.hn', hn)
      .andWhere('a.vstdate', app_date);
  
    return data;
  }

  async getDepartment(db: Knex) {
    const data = await db('kskdepartment')
      .select('*', 'depcode as dep_code', 'department as dep_name');
  
    return data;
  }

  async getPtHN(db: Knex, cid: any) {
    const data = await db('patient')
      .select('hn')
      .where('cid', cid)
      .first(); // ดึงแถวแรกเท่านั้น
  
    return data;
  }

  async getMedrecconcile(db: Knex, hn: any) {
    const data = await db('opitemrece as o')
      .select(
        db.raw(`'' as drug_hospcode`),
        db.raw(`'' as drug_hospname`),
        db.raw(`CONCAT(s.name, ' ', s.strength, ' ', s.units) as drug_name`),
        db.raw(`
          IF(
            (SELECT CONCAT(d.name1, d.name2, d.name3) FROM drugusage d WHERE d.drugusage = o.drugusage) = '',
            (SELECT CONCAT(d.name1, d.name2, d.name3) FROM drugusage d WHERE d.drugusage = o.drugusage),
            (SELECT CONCAT(u.name1, u.name2, u.name3) FROM sp_use u WHERE u.sp_use = o.sp_use)
          ) as drug_use
        `),
        'o.rxdate as drug_receive_date'
      )
      .leftJoin('s_drugitems as s', 's.icode', 'o.icode')
      .leftJoin('drugusage as d', 'd.drugusage', 'o.drugusage')
      .leftJoin('drugitems as i', 'i.icode', 'o.icode')
      .leftJoin('sp_use as u', 'u.sp_use', 'o.sp_use')
      .where('o.hn', hn)
      .whereIn('o.income', ['03', '04'])
      .whereRaw('o.rxdate >= DATE_ADD(CURDATE(), INTERVAL -3 MONTH)')
      .groupBy('o.icode')
      .orderBy('o.rxdate');
  
    return data;
  }

  async  getServicesCoc(db: Knex, hn: any) {
    // Query 1: Outpatient Services
    const query1 = db('ovst as o')
      .select(
        'o.hn',
        'o.vn as seq',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        db.raw(`DATE_FORMAT(o.vstdate, '%Y-%m-%d') as date_serv`),
        db.raw(`DATE_FORMAT(o.vsttime, '%h:%i:%s') as time_serv`),
        'c.department as department'
      )
      .innerJoin('kskdepartment as c', 'c.depcode', 'o.main_dep')
      .innerJoin('patient as p', 'p.hn', 'o.hn')
      .where('o.hn', hn);
  
    // Query 2: Inpatient Services
    const query2 = db('an_stat as an')
      .select(
        'an.hn',
        'an.an as seq',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        db.raw(`DATE_FORMAT(i.regdate, '%Y-%m-%d') as date_serv`),
        db.raw(`DATE_FORMAT(i.regtime, '%h:%i:%s') as time_serv`),
        'w.name as department'
      )
      .innerJoin('ward as w', 'w.ward', 'an.ward')
      .innerJoin('patient as p', 'p.hn', 'an.hn')
      .innerJoin('ipt as i', 'i.an', 'an.an')
      .where('an.hn', hn);
  
    // Combine both queries with UNION
    const data = await db
      .unionAll([query1, query2])
      .orderBy('seq', 'desc') // Sort by seq descending
      .limit(3); // Limit to 3 rows
  
    return data;
  }

  async getProfileCoc(db: Knex, hn: any, seq: any) {
    const data = await db('patient as p')
      .select(
        'p.hn as hn',
        'p.cid as cid',
        'p.pname as title_name',
        'p.fname as first_name',
        'p.lname as last_name',
        's.name as sex',
        'oc.name as occupation',
        db.raw(`
          IF(
            (p.moopart = "" OR p.moopart = '-' OR p.moopart IS NULL), 
            "00", 
            IF(CHAR_LENGTH(p.moopart) < 2, LPAD(p.moopart, 2, "0"), p.moopart)
          ) AS moopart
        `),
        'p.addrpart',
        'p.tmbpart',
        'p.amppart',
        'p.chwpart',
        'p.birthday as brthdate',
        db.raw(`
          CONCAT(
            LPAD(TIMESTAMPDIFF(YEAR, p.birthday, NOW()), 3, '0'), '-',
            LPAD(MOD(TIMESTAMPDIFF(MONTH, p.birthday, NOW()), 12), 2, '0'), '-',
            LPAD(
              IF(
                DAY(p.birthday) > DAY(NOW()), 
                DAYOFMONTH(NOW()) - (DAY(p.birthday) - DAY(NOW())), 
                DAY(NOW()) - DAY(p.birthday)
              ), 
              2, '0'
            )
          ) as age
        `),
        'o.pttype as pttype_id',
        't.name as pttype_name',
        'o.pttypeno as pttype_no',
        'o.hospmain',
        'c.name as hospmain_name',
        'o.hospsub',
        'h.name as hospsub_name',
        'p.firstday as registdate',
        'p.last_visit as visitdate',
        'p.fathername as father_name',
        'p.mathername as mother_name',
        'p.informrelation as contact_name',
        'p.informtel as contact_mobile',
        'oc.name as occupation'
      )
      .innerJoin('ovst as o', 'o.hn', 'p.hn')
      .leftJoin('pttype as t', 'o.pttype', 't.pttype')
      .leftJoin('hospcode as c', 'o.hospmain', 'c.hospcode')
      .leftJoin('hospcode as h', 'o.hospsub', 'h.hospcode')
      .leftJoin('sex as s', 's.code', 'p.sex')
      .leftJoin('occupation as oc', 'oc.occupation', 'p.occupation')
      .where('p.hn', hn)
      .orderBy('o.vstdate', 'desc')
      .limit(1); // ใช้ .first() เพื่อรับแถวแรก (เทียบเท่ากับ LIMIT 1)
  
    return data;
  }



  
}
