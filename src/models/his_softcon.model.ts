import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisSoftConModel {

    async getLogin(db: Knex, username: any, password: any) {
        // console.log(db);
        
        let data = await db.raw(`
        select ltrim(rtrim(name)) as username
        ,ltrim(rtrim(displayname)) as fullname
        ,'14201' as hcode
        from SoftCon.[User] u 
        where ltrim(rtrim(u.name)) = '${username}'
        and u.password = dbo.[ComputeHash]('${password}')
        `);
        return data[0];

        // let data = await db(`SoftCon.[User] u`)
        // .select(db.raw(`ltrim(rtrim(name)) as username`) , db.raw(`ltrim(rtrim(displayname)) as fullname`),db.raw(`'14201' as hcode `))
        // .andWhereRaw(`ltrim(rtrim(u.name)) = ? and u.password = dbo.[ComputeHash](?)`,[username,password]);
        // return data;
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'OPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.ClinicVisitKey is not null
        and r.VisitKey = '${seq}'
        union all
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,s.Name as department
        ,'IPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        where r.AdmitKey is not null
        and r.VisitKey = '${seq}'
        `);
        return data[0];
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1
        pa.code as hn
        ,rfrout.patientkey 
        ,(case when ps.cid = '' or ps.cid is null then '9999999999999' else ps.cid end) as cid
        ,t.name as title_name
        ,ps.firstname as first_name
        ,ps.lastname as last_name
        ,case when ps.moo is null  then '00' when LEN(ps.moo)= 1  then '0'+ltrim(rtrim(ps.moo))
        else ltrim(rtrim(ps.moo))        end as moopart
        ,ps.addressno as addrpart
        ,right(ps.thsubdistrictkey,2)  as tmbpart
        ,right(left(ps.THSubdistrictKey,4),2) as amppart
        ,case when ps.thprovincekey = '' or ps.thprovincekey is null then '34' else ps.thprovincekey end as chwpart
        ,convert(varchar(10),ps.birthdt,121) as brthdate
        ,RIGHT('000'+LTRIM(STR(year(getdate()) - year(ps.birthdt))),2) + ''+ 
        RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, ps.birthdt, getdate()))%12)),0)+''+ 
        RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(ps.birthdt),DAY(GETDATE())),'-',''))),0)  as age 
        ,replace(ps.genderkey,'-','') as sex
        ,(case when ps.genderkey = '-1' then 'ชาย' 
        when ps.genderkey = '-2' then 'หญิง'
        else 'ไม่ระบุ' end) as sexname
        ,o.Name as occupation
        ,mc.Code as pttype_id  --รหัสสิทธิ์การรักษา
        ,mc.Name as pttype_name --สิทธิ์การรักษา
        ,rfrout.MCDocID as pttype_no --เลขที่สิทธิ์การรักษา
        ,h.Code as hospmain --รหัส รพ.สิทธิ์การรักษาหลัก
        ,l.Name as hospmain_name --รพ.สิทธิ์การรักษา
        ,hr.Code as hospsub -- รหัส รพ.สิทธิ์การรักษารอง
        ,lr.Name as hospsub_name --รพ.สิทธิ์การรักษารอง
        ,convert(varchar(10),pa.docdt,121) as registdate 
        ,convert(varchar(10),rfrout.docdt,121) as visitdate
        ,concat(ps.fatherfirstname,' ',ps.fatherlastname) as father_name
        ,concat(ps.motherfirstname,' ',ps.motherlastname) as mother_name
        ,concat(ps.spousefirstname,' ',ps.spouselastname) as couple_name
        ,ps.contactpersonname as contact_name
        ,ps.contactpersonrelation as contact_relation
        ,ps.contactpersontelephone as contact_mobile 
        FROM ClinicVisit cv
        join Visit v ON v.VisitKey = cv.VisitKey
        join Patient pa ON pa.PatientKey = cv.PatientKey
        join person ps on ps.PersonKey  = pa.PatientKey 
        left JOIN CNDX cx ON cx.ClinicVisitKey = cv.ClinicVisitKey AND cx.DXTypeKey = 1
        left JOIN Icd10 ic ON ic.Icd10Code = cx.Icd10Code
        JOIN ServiceUnit su ON su.ServiceUnitKey = cv.ServiceUnitKey
        JOIN Department d ON d.DepartmentKey = su.DepartmentKey
        JOIN VisitMC vmc ON vmc.VisitMCKey = cv.MainVisitMCKey
        JOIN PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
        JOIN dbo.MC ON mc.MCKey = pmc.MCKey
        left join referout rfrout on pa.PatientKey = rfrout.PatientKey 
        left join title t on ps.titlekey = t.titlekey
        left join Occupation o on ps.occupationkey = o.occupationkey
        left join dbo.Hospital h ON h.HospitalKey = rfrout.HospitalKey
        left join dbo.Hospital hr ON hr.HospitalKey = rfrout.MinorHospitalKey
        left join dbo.LegalEntity l ON l.LegalEntityKey = h.HospitalKey
        left join dbo.LegalEntity lr ON lr.LegalEntityKey = rfrout.MinorHospitalKey
        WHERE 
        pa.code = '${hn}' and 
        cv.ClinicVisitSKey <> 32
        order by cv.docdt desc
        `);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT '14201' as provider_code,'โรงพยาบาลมะเร็งอุบลราชธานี' as provider_name
        `);
        return data;
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT drg.name as drug_name
        ,adr.note as symptom 
        ,convert(varchar(10),adr.adrcardissuedate,121) as begin_date
        ,convert(varchar(10),adr.recorddt,121) as daterecord
        FROM PatientADR adr
        inner join DrugGenericName drg on adr.druggenericnamekey = drg.druggenericnamekey
		JOIN dbo.Patient p ON p.PatientKey = adr.PatientKey
        where p.Code = '${hn}'
        `);
        return data;
    }

    async getChronic(db: Knex, hn: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT r.VisitKey as seq, 
        convert(varchar(10),r.DocDT,121) as date_serv,
        convert(varchar,r.DocDT,108) as time_serv ,
        d.Icd10Code as icd_code,
        d.Icd10Name as icd_name,
        d.DXTypeKey as diag_type, 
        REPLACE(REPLACE(r.DXNote, CHAR(13), ''), CHAR(10), '') AS DiagNote,
        d.DXTypeKey as diagtype_id
        FROM ReferOut r 
        LEFT JOIN ReferOutDX d on d.ReferOutKey = r.ReferOutKey
        WHERE r.VisitKey = ${seq}
        `);
        return data;
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,'' as an
        ,r.patientkey as pid
        ,pt.Code as hn
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,h.Code AS to_hcode
        ,'nontrauma' as pttype_id -- รหัสของ Trauma / Non Trauma
        ,'Non Trauma' as pttype_name -- ข้อมูลเป็น Trauma / Non Trauma
        ,(case when e.ordernum = '' or e.ordernum is null 
         then '5' else e.ordernum end) as strength_id
        ,(case e.ordernum 
                    when 1 then 'Resucitate'
                    when 2 then 'Emergency'
                    when 3 then 'Urgency'
                    when 4 then 'Semi Urgency'
                    when 5 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,d.Name as location_name --จุดส่งต่อ เช่น อายุรกรรม / เวชปฏิบัติทั่วไป / อุบัติเหตุ-ฉุกเฉิน / ตึกผู้ป่วยใน  --แผน
        ,s.Name as station_name -- แผนกที่นำส่ง เช่น OPD , IPD , ER 
        ,r.code as loads_id   -- รหัสการนำส่ง       
        ,rm.Name as loads_name  -- การนำส่ง เช่น รถ Ambulance , รถ Ambulance พร้อมพยาบาล , ไปเอง , เจ้าหน้าที่นำส่ง , EMS
        ,le.Name AS to_hcode_name --หาชื่อ hcode
        ,refercause.name as refer_cause
        ,convert(varchar,r.DocDT,108) as refertime
        ,em.ProfessionPermitID as doctor --เอาเลข ว.แพทย์ ฟิวด์นี้ไม่รู้ถูกไหม
        ,doctor.fullname as doctor_name
        , REPLACE(REPLACE(r.currentsicknote, CHAR(13), ''), CHAR(10), '') AS refer_remark --doctor note
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        left join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        left join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join person doctor on r.doctorkey = doctor.personkey
        left join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		left JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
		left JOIN dbo.Employee em ON em.employeeKey = r.doctorKey
		left JOIN dbo.ReferMethod rm ON rm.ReferMethodKey = r.ReferMethodKey
		left JOIN dbo.Department d ON d.DepartmentKey = s.DepartmentKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
     LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
        where r.ClinicVisitKey is not null
        and r.VisitKey = '${seq}' and replace(r.code,'/','-') = '${referno}'
        
        union all 
        
        select top 1 r.VisitKey as seq
        ,replace(a.an,'/','-') as an
        ,r.patientkey as pid
        ,pt.Code as hn
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,h.Code AS to_hcode
        ,'nontrauma' as pttype_id -- รหัสของ Trauma / Non Trauma
        ,'Non Trauma' as pttype_name -- ข้อมูลเป็น Trauma / Non Trauma
        ,(case when e.ordernum = '' or e.ordernum is null 
         then '5' else e.ordernum end) as strength_id
        ,(case e.ordernum 
                    when 1 then 'Resucitate'
                    when 2 then 'Emergency'
                    when 3 then 'Urgency'
                    when 4 then 'Semi Urgency'
                    when 5 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,d.Name as location_name --จุดส่งต่อ เช่น อายุรกรรม / เวชปฏิบัติทั่วไป / อุบัติเหตุ-ฉุกเฉิน / ตึกผู้ป่วยใน
        ,s.Name as station_name -- แผนกที่นำส่ง เช่น OPD , IPD , ER 
        ,rm.Code as loads_id   -- รหัสการนำส่ง       
        ,rm.Name as loads_name  -- การนำส่ง เช่น รถ Ambulance , รถ Ambulance พร้อมพยาบาล , ไปเอง , เจ้าหน้าที่นำส่ง , EMS
        ,le.Name as to_hcode_name --หาชื่อ hcode
        ,refercause.name as refer_cause
        ,convert(varchar,r.DocDT,108) as refertime
        ,em.ProfessionPermitID as doctor --เอาเลข ว.แพทย์ ฟิวด์นี้ไม่รู้ถูกไหม
        ,doctor.fullname as doctor_name
        , REPLACE(REPLACE(r.currentsicknote, CHAR(13), ''), CHAR(10), '') AS refer_remark --doctor note
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        left join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join admit a on r.admitkey = a.admitkey
        left join person doctor on r.doctorkey = doctor.personkey
        left join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		left JOIN dbo.Employee em ON em.employeeKey = r.doctorKey
		left JOIN dbo.ReferMethod rm ON rm.ReferMethodKey = r.ReferMethodKey
		left JOIN dbo.Department d ON d.DepartmentKey = s.DepartmentKey
		left JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
    LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
        where r.AdmitKey is not null
        and r.VisitKey = '${seq}' and replace(r.code,'/','-') = '${referno}'
        `);
        return data;
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select  
        mh.VisitKey as seq
        ,convert(varchar(10),mh.docdt,121) as date_serv
        ,convert(varchar,mh.docdt,108) as time_serv
        ,mi.itemname as drug_name
        ,mi.IssueQty as qty
        ,mi.itemunitname as unit 
        ,REPLACE(REPLACE(mi.usagenote, CHAR(13), ''), CHAR(10), '')  as usage_line1
        ,'' as usage_line2
        ,'' as usage_line3
        from medrequest mi
        inner join medrequestheader mh on mi.medrequestheaderkey = mh.medrequestheaderkey
        where mh.VisitKey ='${seq}'
        order by docdt DESC
        `);
        return data;
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT
        l.VisitKey as seq,
		convert(varchar(10),l.DocDT,121) as date_serv,
		convert(varchar,l.DocDT,108) as time_serv ,
		lx.THTHText as labgroup,
		lr.ItemName as lab_name,        
		ISNULL( lod.Name , ls.ResultValue) +
		CASE WHEN lod.Name IS NOT NULL AND ls.ResultValue IS NOT NULL THEN ls.ResultValue ELSE '' END as lab_result,        
		lr.ItemUnitName as unit, 
		CASE when (CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),lo.NormalValueNote)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), lo.NormalValueNote) END ) is null then '.' else 
		CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),lo.NormalValueNote)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), lo.NormalValueNote) END
		end as standard_result
        from LabRequestHeader l
		JOIN dbo.Person ps ON ps.PersonKey = l.PatientKey
		JOIN LabRequest lr on l.LabRequestHeaderKey = lr.LabRequestHeaderKey
		LEFT JOIN LabResult ls on ls.LabRequestKey = lr.LabRequestKey
		LEFT JOIN LabOutput lo on lo.LabOutputKey = ls.LabOutputKey
		LEFT JOIN dbo.LabOutputDomain lod ON lod.LabOutputDomainKey = ls.LabOutputDomainKey
		JOIN SoftCon.ReferenceX lx ON lx.ReferenceKey = l.LabTypeKey AND EnumClassName = 'LabType'
		LEFT JOIN dbo.LabOutputRef lor ON lor.LabOutputKey = lo.LabOutputKey AND lor.GenderKey = ps.GenderKey        
		WHERE l.VisitKey = '${seq}' and isnull(ls.ResultValue,'') <> ''
		AND l.IsCanceled = 0
        `);
        return data;
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
     SELECT 
        v.PatientKey as pid,
        v.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        i.Code as procedure_code,	
        r.ItemName as procedure_name,
        convert(varchar(10),h.DocDT,121) as start_date,	
        convert(varchar,h.DocDT,108) as start_time,
        convert(varchar(10),h.ExpectIssueDate,121) as end_date,
        convert(varchar,h.ExpectIssueTime,108) as end_time
		from ORRequestHeader h 
		INNER JOIN Visit v on  v.VisitKey = h.VisitKey
		INNER JOIN ORRequest r on r.ORRequestHeaderKey = h.ORRequestHeaderKey
		JOIN dbo.Item i ON i.ItemKey = r.ItemKey
		WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        '' as bloodgrp,
        c.Weight as weight,
        c.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        c.CCNote as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        vs.ConciousScore as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        LEFT JOIN Visit v on r.VisitKey = v.VisitKey
        INNER JOIN ClinicVisit c ON c.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ClinicVisitVS vs on vs.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ServiceUnit s on s.serviceunitkey = c.serviceunitkey
        WHERE r.ClinicVisitKey is not null
        and v.VisitKey = '${seq}'

        UNION all 

        select 	top 1 r.VisitKey as seq,
        convert(varchar(10),a.DocDT,121) as date_serv, 
        convert(varchar,a.DocDT,108) as time_serv, 
        '' as bloodgrp,
        a.Weight as weight,
        a.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        a.PENurseNote as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        vs.ConciousScore as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        INNER JOIN Admit a ON a.AdmitKey = r.AdmitKey
        INNER JOIN AdmitVS vs on vs.AdmitKey = a.AdmitKey
        LEFT JOIN ClinicVisit c ON c.ClinicVisitKey = a.ClinicVisitKey
        LEFT JOIN ServiceUnit s on c.serviceunitkey = s.serviceunitkey
        WHERE r.AdmitKey is not null 
        and a.VisitKey = '${seq}'
        `);
        return data;
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PENote, CHAR(13), ''), CHAR(10), '') as pe FROM ClinicVisit c WHERE  c.VisitKey = ${seq}
        `);
        return data;
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PINote, CHAR(13), ''), CHAR(10), '') as hpi FROM ClinicVisit c 
WHERE c.VisitKey = ${seq}
        `);
        return data;
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        // console.log('start_date',start_date);
        // console.log('end_date',end_date);
        
        let data = await db.raw(`
        SELECT r.VisitKey as seq 
        ,pt.Code as hn
        ,replace(a.an,'/','-') as AN
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,convert(varchar,r.DocDT,108) as refertime
        ,s.Name as location_name
        ,h.Code as to_hcode
        ,mc.code as pttype_id
        ,mc.name as pttype_name
        ,(case when e.ordernum = '' or e.ordernum is null 
        then '5' else e.ordernum end) as strength_id
        ,(case e.ordernum 
                    when 1 then 'Resucitate'
                    when 2 then 'Emergency'
                    when 3 then 'Urgency'
                    when 4 then 'Semi Urgency'
                    when 5 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,le.Name as to_hcode_name
        ,refercause.name as refer_cause
        ,r.doctorkey as doctor
        ,doctor.fullname as doctor_name
        ,r.CurrentSickNote as refer_remark
        FROM ReferOut  r 
        join person p on r.patientkey = p.personkey
        JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join admit a on r.admitkey = a.admitkey
        join person doctor on r.doctorkey = doctor.personkey
        join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
		LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
		JOIN dbo.VisitMC vmc ON vmc.VisitMCKey = c.MainVisitMCKey
		JOIN dbo.PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
		JOIN dbo.MC ON mc.MCKey = pmc.MCKey
        where r.IsReferBack = 0
        and convert(varchar(10),r.DocDT,121) between '${start_date}' and '${end_date}'   and  c.ClinicVisitSKey <> 32
        UNION  
        SELECT 
        r.VisitKey as seq  
        ,pt.Code as hn 
        , replace(a.an,'/','-') as AN
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,convert(varchar,r.DocDT,108) as refertime
        ,s.Name as location_name
        ,h.Code as to_hcode
        ,mc.code as pttype_id
        ,mc.name as pttype_name
        ,(case when e.ordernum = '' or e.ordernum is null then '5' else e.ordernum end) as strength_id
        ,(case e.ordernum 
                    when 1 then 'Resucitate'
                    when 2 then 'Emergency'
                    when 3 then 'Urgency'
                    when 4 then 'Semi Urgency'
                    when 5 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name 
        ,le.Name as to_hcode_name  
        ,refercause.name as refer_cause
        ,r.doctorkey as doctor
        ,doctor.fullname as doctor_name
        ,r.CurrentSickNote as refer_remark
        FROM ReferOut  r 
        JOIN person p on r.patientkey = p.personkey
        JOIN Title t on p.titlekey = t.titlekey
        JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        LEFT JOIN admit a on r.admitkey = a.admitkey
        JOIN ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
        LEFT JOIN ClinicVisit c ON c.ClinicVisitKey = a.ClinicVisitKey
        INNER JOIN AdmitVS vs on vs.AdmitKey = a.AdmitKey
        JOIN dbo.VisitMC vmc ON vmc.VisitMCKey = c.MainVisitMCKey
		JOIN dbo.PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
		JOIN dbo.MC ON mc.MCKey = pmc.MCKey  
		LEFT JOIN Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
		LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
		JOIN ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		join person doctor on r.doctorkey = doctor.personkey
        where r.IsReferBack = 0
        and convert(varchar(10),r.DocDT,121) between '${start_date}' and '${end_date}' 
        and r.AdmitKey is not null   and c.ClinicVisitSKey  <> 32
        `);
        // console.log(data);
        
        return data;
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        SELECT r.VisitKey as seq 
        ,pt.Code as hn
        ,replace(a.an,'/','-') as an
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,h.Code as to_hcode
        ,le.Name as to_hcode_name
        ,refercause.name as refer_cause
        ,convert(varchar,r.DocDT,108) as refertime
        ,r.doctorkey as doctor
        ,doctor.fullname as doctor_name
        FROM ReferOut  r 
        join person p on r.patientkey = p.personkey
        LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        join Title t on p.titlekey = t.titlekey
        join ServiceUnit s on r.serviceunitkey = s.serviceunitkey
        left join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join admit a on r.admitkey = a.admitkey
        join person doctor on r.doctorkey = doctor.personkey
        join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
    	LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
        where r.IsReferBack = 1
        and convert(varchar(10),r.DocDT,121) between '${start_date}' and '${end_date}'
        `);
        return data;
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT h.DocDT, x.ItemName  
        FROM RadRequest as x  
        INNER JOIN RadRequestHeader h on x.RadRequestHeaderKey = h.RadRequestHeaderKey
        WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`
        SELECT 
          s.Code as dep_code , 
          s.Name as dep_name 
        from ServiceUnit s
       WHERE s.IsDisabled = 0 
        `);
        return data;
    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        SELECT pt.Code as hn 
        from Person p 
        INNER JOIN patient pt on pt.PatientKey = p.PersonKey
        WHERE p.Cid = '${cid}'
        `);
        return data[0];
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getServicesCoc(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select top 3
        cv.VisitKey as seq, 
        pa.code as hn ,
        t.Name as title_name,
        ps.firstname as first_name,
        ps.lastname as last_name,
        convert(varchar(10),cv.DocDT,121) as date_serv,
        convert(varchar,cv.DocDT,108) as time_serv,
        su.Name as department,
        case when a.an is null or a.an = ''  then 'OPD' else 'IPD' end DEP
        from Patient pa inner join Person ps on pa.PatientKey = ps.PersonKey 
        left join Title t on ps.titlekey = t.titlekey
        left join ClinicVisit cv on cv.PatientKey = pa.PatientKey 
        left join ServiceUnit su on su.ServiceUnitKey = cv.ServiceUnitKey         
        left join Admit a on a.ClinicVisitKey = cv.ClinicVisitKey 
        where pa.Code = '${hn}'
        order by cv.DocDT desc
        `);
        return data;
    }

    async getProfileCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        select top 1
        pa.code as hn
        ,rfrout.patientkey 
        ,(case when ps.cid = '' or ps.cid is null then '9999999999999' else ps.cid end) as cid
        ,t.name as title_name
        ,ps.firstname as first_name
        ,ps.lastname as last_name
        ,case when ps.moo is null  then '00' when LEN(ps.moo)= 1  then '0'+ltrim(rtrim(ps.moo))
        else ltrim(rtrim(ps.moo))        end as moopart
        ,ps.addressno as addrpart
        ,right(ps.thsubdistrictkey,2)  as tmbpart
        ,right(left(ps.THSubdistrictKey,4),2) as amppart
        ,case when ps.thprovincekey = '' or ps.thprovincekey is null then '34' else ps.thprovincekey end as chwpart
        ,convert(varchar(10),ps.birthdt,121) as brthdate
        ,RIGHT('000'+LTRIM(STR(year(getdate()) - year(ps.birthdt))),2) + ''+ 
        RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, ps.birthdt, getdate()))%12)),0)+''+ 
        RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(ps.birthdt),DAY(GETDATE())),'-',''))),0)  as age 
        ,replace(ps.genderkey,'-','') as sex
        ,(case when ps.genderkey = '-1' then 'ชาย' 
        when ps.genderkey = '-2' then 'หญิง'
        else 'ไม่ระบุ' end) as sexname
        ,o.Name as occupation
        ,mc.Code as pttype_id  --รหัสสิทธิ์การรักษา
        ,mc.Name as pttype_name --สิทธิ์การรักษา
        ,rfrout.MCDocID as pttype_no --เลขที่สิทธิ์การรักษา
        ,h.Code as hospmain --รหัส รพ.สิทธิ์การรักษาหลัก
        ,l.Name as hospmain_name --รพ.สิทธิ์การรักษา
        ,hr.Code as hospsub -- รหัส รพ.สิทธิ์การรักษารอง
        ,lr.Name as hospsub_name --รพ.สิทธิ์การรักษารอง
        ,convert(varchar(10),pa.docdt,121) as registdate 
        ,convert(varchar(10),rfrout.docdt,121) as visitdate
        ,concat(ps.fatherfirstname,' ',ps.fatherlastname) as father_name
        ,concat(ps.motherfirstname,' ',ps.motherlastname) as mother_name
        ,concat(ps.spousefirstname,' ',ps.spouselastname) as couple_name
        ,ps.contactpersonname as contact_name
        ,ps.contactpersonrelation as contact_relation
        ,ps.contactpersontelephone as contact_mobile 
        FROM ClinicVisit cv
        join Visit v ON v.VisitKey = cv.VisitKey
        join Patient pa ON pa.PatientKey = cv.PatientKey
        join person ps on ps.PersonKey  = pa.PatientKey 
        left JOIN CNDX cx ON cx.ClinicVisitKey = cv.ClinicVisitKey AND cx.DXTypeKey = 1
        left JOIN Icd10 ic ON ic.Icd10Code = cx.Icd10Code
        JOIN ServiceUnit su ON su.ServiceUnitKey = cv.ServiceUnitKey
        JOIN Department d ON d.DepartmentKey = su.DepartmentKey
        JOIN VisitMC vmc ON vmc.VisitMCKey = cv.MainVisitMCKey
        JOIN PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
        JOIN dbo.MC ON mc.MCKey = pmc.MCKey
        left join referout rfrout on pa.PatientKey = rfrout.PatientKey 
        left join title t on ps.titlekey = t.titlekey
        left join Occupation o on ps.occupationkey = o.occupationkey
        left join dbo.Hospital h ON h.HospitalKey = rfrout.HospitalKey
        left join dbo.Hospital hr ON hr.HospitalKey = rfrout.MinorHospitalKey
        left join dbo.LegalEntity l ON l.LegalEntityKey = h.HospitalKey
        left join dbo.LegalEntity lr ON lr.LegalEntityKey = rfrout.MinorHospitalKey
        WHERE 
        pa.code = '${hn}' and 
        cv.ClinicVisitSKey <> 32
        order by cv.docdt desc
        `);
        return data[0];
    }

}

