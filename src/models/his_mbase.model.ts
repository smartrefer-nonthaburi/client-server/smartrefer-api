import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisMbaseModel {
  async getLogin(db: Knex, username: any, password: any) {
    // console.log("xxxx");

    // let data = await db.raw(`
    //      SELECT a.CID as username , a.cid as fullname,(select offid from gcoffice limit 1) as hcode   
    //      from  staff a
    //      WHERE a.CID = '${username}' 
    //      and SUBSTRING(a.CID, 10, 13) = '${password}'
    //      `);
    // return data[0];
    let data = await db(`staff`)
    .select(`CID as username` , `CID as fullname`)
    .select(db('gcoffice').first('offid').as('hcode').limit(1))
    .where(`CID`,username)
    .andWhereRaw(`SUBSTRING(CID, 10, 13) = ?`,[password]);
    return data;
  }

  async getServices(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT DISTINCT
    o.VISIT_ID as seq,
CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
      END AS title_name 
    ,p.fname as first_name,p.lname as last_name, 
    DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
		TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv,  
    d.UNIT_NAME as department
    FROM refers r
		LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
    LEFT JOIN cid_hn b ON o.hn = b.hn
    LEFT JOIN population p ON b.cid = p.cid 
    LEFT JOIN service_units d ON o.unit_reg = d.unit_id
    WHERE  o.hn ='${hn}' and r.visit_id = '${seq}'
AND r.IS_CANCEL = 0 
AND r.VISIT_ID NOT IN (SELECT ir.visit_id FROM ipd_reg ir WHERE ir.IS_CANCEL =0)
LIMIT 1       
 UNION
SELECT 
i.VISIT_ID as seq,
CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
      END AS title_name 
,p.fname as first_name,p.lname as last_name, 
DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv, 
    e.UNIT_NAME as department
from refers r 
LEFT  JOIN ipd_reg i ON r.VISIT_ID = i.VISIT_ID AND i.IS_CANCEL = 0
LEFT  JOIN opd_visits b ON i.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
LEFT  JOIN cid_hn c ON b.hn = c.hn 
LEFT JOIN population p ON c.cid = p.cid
LEFT  JOIN service_units e ON i.WARD_NO = e.UNIT_ID
AND r.IS_CANCEL = 0
WHERE  b.hn ='${hn}' and r.visit_id = '${seq}'
AND r.IS_CANCEL = 0 LIMIT 1
    
   `);
    return data[0];
  }

  async getProfile(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    select o.HN as hn, 
           p.cid as cid,
					CASE 
        WHEN PRENAME not in('') THEN PRENAME
        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'พระภิกษุ'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='1' THEN 'นาย'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE='1' THEN 'นางสาว'
    ELSE 'นาง'
    END AS title_name
          , p.FNAME as first_name
					, p.LNAME as last_name
					, SUBSTR(p.TOWN_ID,7,2) as moopart
					, trim(p.HOME_ADR) as addrpart
					, SUBSTR(p.TOWN_ID,5,2) as tmbpart
					, SUBSTR(p.TOWN_ID,3,2) as amppart
					, SUBSTR(p.TOWN_ID,1,2) as chwpart
					, p.BIRTHDATE as brthdate
          , concat(lpad(timestampdiff(year,p.birthdate,now()),3,'0'),'-'
         ,lpad(mod(timestampdiff(month,p.birthdate,now()),12),2,'0'),'-'
         ,lpad(if(day(p.birthdate)>day(now()),dayofmonth(now())-(day(p.birthdate)-day(now())),day(now())-day(p.birthdate)),2,'0')) as age
				  , CASE
						WHEN p.SEX = 1 THEN 'ชาย'
						WHEN p.SEX = 2 THEN 'หญิง'
						END as sex
				  , replace(replace(oc.oc_name,char(13),''),char(10),'') as occupation
          , m.INSCL as pttype_id
					 , m.INSCL_NAME as pttype_name
           , g.uc_cardid as pttype_no
					 , g.HOSPMAIN  as hospmain
					 , hop.HOSP_NAME as hospmain_name
					 , g.HOSPSUB as hospsub
					 , hs.HOSP_NAME as hospsub_name
           , g.UC_REGISTER as registdate
           , max(date(o.REG_DATETIME)) as visitdate
           , p.FATHER as father_name
	         , p.MOTHER as mother_name
           ,p.TELEPHONE as contact_mobile
           ,'' as couple_name
		      , p.contact as contact_name
	      	, '' as contact_relation
    FROM population as p 
        INNER JOIN cid_hn c on p.CID = c.CID
	INNER JOIN opd_visits o ON c.HN = o.HN
        LEFT JOIN towns t ON p.town_id = t.town_id
        LEFT  JOIN main_inscls m ON p.INSCL = m.INSCL
        LEFT JOIN uc_inscl g ON p.CID = g.CID AND (g.date_abort >= date(o.REG_DATETIME) or day(g.date_abort)=0) 
        LEFT JOIN hospitals hop ON g.HOSPMAIN = hop.HOSP_ID
				LEFT JOIN occupations oc ON p.oc_id = oc.oc_id
	      LEFT JOIN hospitals hs ON g.HOSPSUB = hs.HOSP_ID
        WHERE o.hn =  '${hn}'
			  
        `);
    return data[0];
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
        SELECT a.offid as provider_code,b.HOSP_NAME as provider_name 
        FROM gcoffice a
        INNER JOIN hospitals b on a.offid = b.HOSP_ID`);
    return data[0];
  }

  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT
    a.VISIT_ID as seq,
    DATE(a.REG_DATETIME) date_serv,
    TIME(a.REG_DATETIME)  time_serv,
    c.DRUG_THO as  vaccine_code,
		c.DRUG_NAME as vaccine_name
    FROM  opd_visits a
    INNER JOIN prescriptions b ON a.VISIT_ID = b.VISIT_ID  AND b.IS_CANCEL = 0
    INNER JOIN drugs_tho  c  ON b.DRUG_ID  = c.DRUG_ID
		AND c.DRUG_THO IN('010','041','042','043','DHB','DHB2','DHB3','101','021','031','044','051','061','071','085','MEAS')
		OR c.DRUG_THO BETWEEN 'JE1' AND 'JE3'
		OR c.DRUG_THO BETWEEN 'OPV1' AND 'OPV5'
		OR c.DRUG_THO BETWEEN 'DTP1' AND 'DTP5'
		OR c.DRUG_THO BETWEEN 'TT1' AND 'TT5'
    WHERE a.IS_CANCEL = 0
		AND c.DRUG_ID <> ' '
        AND a.HN  = '${hn}'
        `);
    return data[0];
  }

  async getChronic(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
        SELECT a.ICD10 as icd_code, a.DX_DATE as start_date, i.ICD_NAME as icd_name
        FROM chronic_reg a
        LEFT JOIN icd10new i ON LEFT(code,3) = a.ICD10
		    INNER JOIN cid_hn c ON a.cid = c.cid
        WHERE c.hn ='${hn}'
		    GROUP BY a.ICD10 `);
    return data[0];
  }

  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
        SELECT b.DRUG_NAME as drug_name,
replace(replace(a.ALLERGY_NOTE,char(13),''),char(10),'') as symptom ,
a.ADRDATE as begin_date,date(a.UPD_DT) as daterecord
FROM cid_drug_allergy a
INNER JOIN drugs b ON a.DRUG_ID = b.DRUG_ID
INNER JOIN cid_hn c ON a.cid = c.cid 
        WHERE hn ='${hn}'`);
    return data[0];
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT o.VISIT_ID as seq , replace(replace(replace(SUBSTR(o.history,LOCATE('PRESENT ILLNESS',o.history)+17) ,char(13),''),char(10),''),'""','') as hpi FROM opd_visits o WHERE  o.visit_id = '${seq}' group by o.visit_id
    `);
    return data[0];
  }

  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
  SELECT o.VISIT_ID as seq , replace(replace(replace(o.pexam,char(13),''),char(10),''),'""','') as pe FROM opd_visits o WHERE  o.visit_id = '${seq}' group by o.visit_id
   `);
    return data[0];
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT o.VISIT_ID as seq,
    DATE_FORMAT(date(o.REG_DATETIME),'%Y-%m-%d') as date_serv, 
    TIME_FORMAT(time(o.REG_DATETIME),'%h:%i:%s') as time_serv, 
    '' as bloodgrp,
    o.WEIGHT as weight,
    o.HEIGHT as height,
    '' as bmi,
    o.BODY_TEMP as temperature,
    o.PULSE_RATE as pr,
    o.RESP_RATE as rr,
    o.BP_SYST as sbp,
    o.BP_DIAS as dbp,
    replace(replace(replace(SUBSTR(o.history,LOCATE('CHIEF COMPLAINT',o.history),
    LOCATE('PRESENT ILLNESS',o.history)- 
    LOCATE('CHIEF COMPLAINT',o.history)) ,char(13),''),char(10),''),'""','')  as symptom,
    s.UNIT_ID as depcode,
    s.UNIT_NAME as department,
CASE 
 WHEN (o.gcs_m = '' or o.gcs_m = 0) THEN '6'
ELSE o.gcs_m
END as movement_score,
 CASE 
 WHEN (o.gcs_v = '' or o.gcs_v = 0) THEN '5'
ELSE o.gcs_v
END as vocal_score,
CASE 
 WHEN (o.gcs_e = '' or o.gcs_e = 0)  THEN '4'
ELSE o.gcs_e
END as eye_score,
    '' as oxygen_sat,
    '' as gak_coma_sco,
    i.nickname as diag_text,
    '' as pupil_right,
    '' as pupil_left
    FROM opd_visits  o 
    LEFT JOIN service_units s ON o.unit_reg = s.unit_id
    INNER JOIN opd_diagnosis od ON  od.visit_id = o.visit_id and od.dxt_id = 1 and od.is_cancel = 0
    INNER JOIN icd10new i ON i.icd10 = od.icd10
        WHERE o.VISIT_ID = '${seq}' 
    `);
    return data[0];
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT a.VISIT_ID as seq, date_format(a.REG_DATETIME,'%Y-%m-%d') as date_serv, time(a.REG_DATETIME) as time_serv
      , c.code as icd_code,IF(c.ICD_NAME!='', c.ICD_NAME, c.NICKNAME) as icd_name
      , b.DXT_ID as diag_type, '' as DiagNote, b.DXT_ID  as diagtype_id
      FROM opd_visits a
  INNER JOIN opd_diagnosis b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
  INNER JOIN icd10new c ON b.ICD10 = c.ICD10
      WHERE a.visit_id ='${seq}'
      `);
    return data[0];
  }

  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT DISTINCT
    a.hn as pid,
    a.VISIT_ID AS seq,
    DATE(a.REG_DATETIME) AS date_serv,
    TIME(a.REG_DATETIME) AS time_serv,
    c. CODE AS procedure_code,
    IF(c.TNAME = '', c.NICKNAME, c.TNAME) AS procedure_name,
    
    DATE(b.OP_DT) AS start_date,
TIME(b.OP_DT) AS start_time,
   IF(DATE(b.OP_END)!='0000-00-00', DATE(b.OP_END), DATE(b.OP_DT)) AS end_date,
'0000-00-00' AS end_time
FROM
    opd_visits a
INNER JOIN opd_operations b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
INNER JOIN icd9cm c ON b.icd9 = c.ICD9 AND left(c.code,4) <> 'XXXX' AND c.code <> ''
WHERE a.VISIT_ID = '${seq}'
                 `);
    return data[0];
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
         SELECT a.VISIT_ID as seq,
    DATE_FORMAT(date(a.REG_DATETIME),'%Y%m%d') as date_serv,
    time(a.REG_DATETIME) as time_serv,
    CONCAT(trim(c.DRUG_NAME),' ',d1.DFORM_SNAME,' (',cast(c.STRENGTH as DECIMAL(8,0)),' ',s.strength_name,'/',cast(c.ST_NUM_UUNIT as DECIMAL(8,0)),'',u.UUNIT_NAME,')') as drug_name,
		b.rx_amount as qty,
    d.UUNIT_NAME as unit ,
replace(replace(replace(CONCAT(REPLACE(b.RX_DOSE,'.00','')," ", us.uunit_name,"  ",r.ROUTE_NAME," ",f.FRQ_NAME),char(13),''),char(10),''),'""','') as usage_line1 ,
replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line2,
replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
FROM opd_visits a
INNER JOIN prescriptions b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL =0
INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
INNER JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
INNER JOIN routes r ON r.route_id = b.route_id 
INNER JOIN frequency f on f.frq_id = b.frq_id
INNER JOIN strength_units s ON s.strength_unit = c.strength_unit
INNER JOIN dosage_forms d1 ON d1.DFORM_ID = c.DFORM_ID
INNER JOIN usage_units u ON c.ST_TXT_UUNIT = u.UUNIT_ID
INNER JOIN usage_units us ON c.UUNIT_ID = us.UUNIT_ID 
WHERE a.VISIT_ID = '${seq}'
UNION
 SELECT a.VISIT_ID as seq,
    DATE_FORMAT(date(b.order_dt),'%Y%m%d') as date_serv,
    time(b.order_dt) as time_serv,
    CONCAT(trim(c.DRUG_NAME),' ',d1.DFORM_SNAME,'(',cast(c.STRENGTH as DECIMAL(8,0)),' ',s.strength_name,'/',cast(c.ST_NUM_UUNIT as DECIMAL(8,0)),'',u.UUNIT_NAME,')') as drug_name,
		'' as qty,
    d.UUNIT_NAME as unit ,
replace(replace(replace(CONCAT(REPLACE(b.RX_DOSE,'.00','')," ", us.uunit_name,"  ",r.ROUTE_NAME," ",f.FRQ_NAME),char(13),''),char(10),''),'""','') as usage_line1 ,
replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line2,
replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
FROM ipd_reg a
INNER JOIN ipd_cont b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL =0
INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
INNER JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
INNER JOIN routes r ON r.route_id = b.route_id 
INNER JOIN frequency f on f.frq_id = b.frq_id
INNER JOIN strength_units s ON s.strength_unit = c.strength_unit
INNER JOIN dosage_forms d1 ON d1.DFORM_ID = c.DFORM_ID
INNER JOIN usage_units u ON c.ST_TXT_UUNIT = u.UUNIT_ID
INNER JOIN usage_units us ON c.UUNIT_ID = us.UUNIT_ID 
 WHERE a.VISIT_ID = '${seq}'
  UNION
 SELECT a.VISIT_ID as seq,
    DATE_FORMAT(date(b.order_dt),'%Y%m%d') as date_serv,
    time(b.order_dt) as time_serv,
    CONCAT(trim(c.DRUG_NAME),' ',d1.DFORM_SNAME,'(',cast(c.STRENGTH as DECIMAL(8,0)),' ',s.strength_name,'/',cast(c.ST_NUM_UUNIT as DECIMAL(8,0)),'',u.UUNIT_NAME,')') as drug_name,
		'' as qty,
    d.UUNIT_NAME as unit ,
replace(replace(replace(CONCAT(REPLACE(b.RX_DOSE,'.00','')," ", us.uunit_name,"  ",r.ROUTE_NAME," ",f.FRQ_NAME),char(13),''),char(10),''),'""','') as usage_line1 ,
replace(replace(replace(c.INFO_PATIENT,char(13),''),char(10),''),'""','') as usage_line2,
replace(replace(replace(c.ATTENTION,char(13),''),char(10),''),'""','') as usage_line3
FROM ipd_reg a
INNER JOIN ipd_oneday b ON a.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL =0
INNER JOIN drugs c ON b.DRUG_ID = c.DRUG_ID
INNER JOIN usage_units d ON c.PACKAGE = d.UUNIT_ID
INNER JOIN routes r ON r.route_id = b.route_id 
INNER JOIN frequency f on f.frq_id = b.frq_id
INNER JOIN strength_units s ON s.strength_unit = c.strength_unit
INNER JOIN dosage_forms d1 ON d1.DFORM_ID = c.DFORM_ID
INNER JOIN usage_units u ON c.ST_TXT_UUNIT = u.UUNIT_ID
INNER JOIN usage_units us ON c.UUNIT_ID = us.UUNIT_ID 
  WHERE a.VISIT_ID = '${seq}'
  `);
    return data[0];
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT
      DATE_FORMAT(date(a.LREQ_DT),'%Y%m%d') as date_serv,TIME_FORMAT((a.LREQ_DT),'%h:%i:%s') as time_serv
      ,b.LAB_GROUP as labgroup,b.LAB_NAME as lab_name
      ,replace(replace(replace(trim(a.LAB_RESULT),char(13),''),char(10),''),'^','')  as lab_result
   , '' as unit,  b.normal_val as standard_result
      FROM lab_requests a
      INNER JOIN lab_lists b ON a.LAB_ID = b.LAB_ID AND b.IS_CANCEL =0
      WHERE a.VISIT_ID ='${seq}'
   AND a.is_cancel = 0
   AND b.is_secret = 0
  `);
    return data[0];
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
    let data = await db.raw(`
      SELECT o.visit_id as seq
,date(o.REG_DATETIME) as date_serv
,a.ap_date as date
, '00:00:00' as time
, s.unit_name as department
, replace(replace(replace(a.ap_memo,char(13),''),char(10),''),'""','') as detail
, time(o.REG_DATETIME) as time_serv
        FROM appoints a
	LEFT  JOIN opd_visits o ON a.hn = o.hn AND a.ap_date = date(o.reg_datetime)
        INNER JOIN service_units s on a.unit_id = s.unit_id
        WHERE  o.VISIT_ID = '${seq}' 
`);
    return data[0];
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
  SELECT x.VISIT_ID as seq,
  date(x.XREQ_DATETIME) as xray_date,
  xl.XL_NAME as xray_name 
  FROM xray_requests x
  INNER JOIN xray_lists xl ON xl.XL_ID = x.XL_ID
  WHERE x.VISIT_ID = '${seq}'
  `);
    return data[0];
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
  SELECT a.VISIT_ID as seq,
      CASE
        when isnull(b.ADM_ID) THEN ''
        ELSE b.ADM_ID END as an,  
      c.hn as pid, c.hn as hn, 
      a.REFER_ID as referno,
      a.HOSP_ID as to_hcode,
      CASE 
        WHEN a.VISIT_ID in (SELECT ac.VISIT_ID from accidents ac)  THEN '1'
        WHEN a.VISIT_ID not in (SELECT ac.VISIT_ID from accidents ac)  THEN '2'
      END  as pttype_id,
      CASE 
        WHEN a.VISIT_ID in (SELECT ac.VISIT_ID from accidents ac)  THEN 'truama'
        WHEN a.VISIT_ID not in (SELECT ac.VISIT_ID from accidents ac)  THEN 'non truama'
      END as pttype_name,
      c.PT_STATES as strength_id,
      (case c.PT_STATES 
        when 1 then 'Resucitate'
        when 2 then 'Emergency'
        when 3 then 'Urgency'
        when 4 then 'Semi Urgency'
        when 5 then 'Non Urgency'
        else '' end ) as strength_name,
      a.UNIT_ID as location_name,
      CASE 
        WHEN a.UNIT_ID in ('22','23','38','39') THEN 'IPD'
        WHEN a.UNIT_ID = '11'   THEN 'ER'
        WHEN a.UNIT_ID not in ('11','22','23','38','39') THEN 'OPD'
      END as station_name,
      h.HOSP_NAME as to_hcode_name,
      replace(replace(replace(a.rf_reason,char(13),''),char(10),''),'""','') as refer_cause,
      date(a.RF_DT) as referdate, time(a.RF_DT) as refertime,
      CONCAT(trim(s.licence), ',', s.cid) as doctor,
      CONCAT(trim(e.fname),' ',e.lname) as doctor_name
      FROM refers a
      LEFT JOIN ipd_reg b ON a.visit_id = b.visit_id AND b.is_cancel = 0
      LEFT JOIN opd_visits c ON a.visit_id = c.visit_id AND c.is_cancel =0
      LEFT JOIN cid_hn d ON c.hn = d.hn 
      LEFT JOIN population e ON d.cid = d.cid
      INNER JOIN hospitals h ON a.hosp_id = h.hosp_id
      LEFT JOIN opd_diagnosis od ON c.VISIT_ID = od.VISIT_ID AND od.IS_CANCEL = 0  AND od.DXT_ID = 1
      INNER JOIN staff s ON od.STAFF_ID = s.staff_id AND s.cid = e.cid
      WHERE  a.VISIT_ID  ='${seq}' and a.REFER_ID = '${referno}'
      AND a.is_cancel =0
      `);
    return data[0];
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT 
    r.VISIT_ID as seq,
    o.HN as hn,
  CASE 
    WHEN ISNULL(i.adm_id)  THEN ''
    ELSE i.ADM_ID
    END as an,
  
    CASE 
               WHEN p.PRENAME not in('') THEN p.PRENAME
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
               WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
               ELSE 'นาง' 
    END AS title_name, 
    p.FNAME as first_name, 
    p.LNAME as last_name,
    r.REFER_ID as referno,
    date(r.RF_DT) as referdate,
      CASE 
        WHEN u.UNIT_ID in ('22','23','38','39','50','55') THEN 'IPD'
        WHEN u1.UNIT_ID = '11'   THEN 'ER'
        ELSE u1.unit_name
      END  as location_name,
    r.HOSP_ID as to_hcode,
    CASE 
          WHEN o.VISIT_ID in (SELECT ac.VISIT_ID from accidents ac)  THEN '1'
          WHEN o.VISIT_ID not in (SELECT ac.VISIT_ID from accidents ac)  THEN '2'
          END  as pttype_id,
    CASE 
          WHEN o.VISIT_ID in (SELECT ac.VISIT_ID from accidents ac)  THEN 'truama'
          WHEN o.VISIT_ID not in (SELECT ac.VISIT_ID from accidents ac)  THEN 'non truama'
           END as pttype_name,
    CASE
      WHEN o.PT_STATES = '' THEN '5'
           ELSE o.PT_STATES 
         END as strength_id,
                     (case o.PT_STATES 
                         when 1 then 'Resucitate'
                         when 2 then 'Emergency'
                         when 3 then 'Urgency'
                         when 4 then 'Semi Urgency'
                         when 5 then 'Non Urgency'
                         else 'Non Urgency' 
                         end ) as strength_name,
            h.HOSP_NAME as to_hcode_name,
            replace(replace(r.crf_reason,char(13),''),char(10),'') as refer_cause,
            time(r.RF_DT) as refertime,
            r.STAFF_ID as doctor,
            CONCAT(trim(p1.fname),' ',p1.lname) as doctor_name
       FROM 
            refers as r
            LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
            LEFT JOIN cid_hn c ON o.HN = c.HN 
            LEFT JOIN population p ON c.CID = p.CID
            LEFT JOIN staff s ON s.STAFF_ID = r.STAFF_ID
            LEFT JOIN population p1 ON s.CID = p1.CID
            LEFT JOIN hospitals h ON r.HOSP_ID = h.HOSP_ID
            LEFT JOIN ipd_reg i ON o.VISIT_ID = i.VISIT_ID AND i.IS_CANCEL =0
            LEFT JOIN service_units u ON i.WARD_NO = u.UNIT_ID
            LEFT JOIN service_units u1 ON o.unit_reg = u1.UNIT_ID
          WHERE date(r.RF_DT) between '${start_date}' and '${end_date}'
      	 AND r.is_cancel = 0
	        AND r.rf_type = 2	
        	AND left(h.hosp_id,3) not in ('036','037','998')
      `);
    return data[0];
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
  
    SELECT r.VISIT_ID as seq,
    o.HN as hn,
    CASE 
              WHEN p.PRENAME not in('') THEN p.PRENAME
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'พระภิกษุ'
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
              WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE='1' THEN 'นางสาว'
              ELSE 'นาง' 
   END AS title_name, 
   p.FNAME as first_name, 
   p.LNAME as last_name,
   r.REFER_ID as referno,
   date(r.RF_DT) as referdate,
   r.HOSP_ID as to_hcode,
   h.HOSP_NAME as to_hcode_name,
   replace(replace(replace(r.rf_reason,char(13),''),char(10),''),'""','') as refer_cause,
   time(r.RF_DT) as refertime,
   IF(od.STAFF_ID is null,'0010',od.STAFF_ID) as doctor,
   IF(p1.fname is NULL,'ประจักษ์  สีลาชาติ',CONCAT(trim(p1.fname),' ',p1.lname)) as doctor_name
     FROM refers as r
          LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
           LEFT JOIN cid_hn c ON o.HN = c.HN 
           LEFT JOIN population p ON c.CID = p.CID
           LEFT JOIN opd_diagnosis od ON od.visit_id = o.visit_id
           LEFT JOIN staff s ON s.STAFF_ID = r.STAFF_ID
           LEFT JOIN staff s1 ON s1.STAFF_ID = od.STAFF_ID
           LEFT JOIN population p1 ON s1.CID = p1.CID
           LEFT JOIN hospitals h ON r.HOSP_ID = h.HOSP_ID
    WHERE 
    date(r.RF_DT) between '${start_date}' and '${end_date}'
  #date(r.RF_DT) between '2022-05-01' and NOW()
   AND r.is_cancel = 0
   AND r.RF_TYPE = 1
   AND r.HOSP_ID NOT IN ('10669','12269','21984','10954','11919','11443','11920')
   GROUP BY r.VISIT_ID`);
    return data[0];
  }

  async getBedsharing(db: Knex) {
    let data = await db.raw(`SELECT  CURDATE() AS regdate, CURTIME() AS time , a.offid AS hcode,
      b.UNIT_ID AS ward_code, b.UNIT_NAME AS ward_name, 
      CASE 
      WHEN b.UNIT_ID = '22' THEN 2
      WHEN b.UNIT_ID = '38' THEN 30
      WHEN b.UNIT_ID = '39' THEN 30
      END AS ward_std,
      CASE 
      WHEN b.UNIT_ID = '22' THEN 9
      WHEN b.UNIT_ID = '38' THEN 30
      WHEN b.UNIT_ID = '39' THEN 30
      END AS ward_bed, COUNT(i.ADM_ID) AS ward_pt
      FROM gcoffice a, service_units b
      LEFT JOIN .ipd_reg i ON b.UNIT_ID = i.WARD_NO AND i.IS_CANCEL = 0 AND i.DSC_DT = 0  AND i.DSC_STATUS = 0 
      AND i.DSC_TYPE = 0 AND i.BED_NO <> ''
      WHERE b.UNIT_ID IN ('22','38','39')
      GROUP BY b.UNIT_ID`);
    return data[0];
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
            select unit_id as dep_code,unit_name as dep_name from service_units
        `);
    return data[0];

  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
        select c.HN as hn from population as p 
        INNER JOIN cid_hn c on p.CID = c.CID
        where p.CID = '${cid}'
        `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        select '' as drug_hospcode ,'' as drug_hospname,'' as drug_name,'' as drug_use,'' as drug_receive_date
        `);
    return data[0];
  }

  async getServicesCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT DISTINCT
    o.VISIT_ID as seq,
    o.hn,
CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
      END AS title_name 
    ,p.fname as first_name,p.lname as last_name, 
    DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
		TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv,  
    d.UNIT_NAME as department
    FROM refers r
		LEFT JOIN opd_visits o ON r.VISIT_ID = o.VISIT_ID AND o.IS_CANCEL = 0
    LEFT JOIN cid_hn b ON o.hn = b.hn
    LEFT JOIN population p ON b.cid = p.cid 
    LEFT JOIN service_units d ON o.unit_reg = d.unit_id
    WHERE  o.hn ='${hn}'
AND r.IS_CANCEL = 0 
AND r.VISIT_ID NOT IN (SELECT ir.visit_id FROM ipd_reg ir WHERE ir.IS_CANCEL =0)       
 UNION
SELECT 
i.VISIT_ID as seq,
c.hn,
CASE 
             WHEN p.PRENAME not in('') THEN p.PRENAME
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW())< '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE  = '4'THEN 'พระภิกษุ'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='1' THEN 'นาย'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'
             WHEN TIMESTAMPDIFF(year,p.BIRTHDATE,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE ='1' THEN 'นางสาว'
             ELSE 'นาง' 
      END AS title_name 
,p.fname as first_name,p.lname as last_name, 
DATE_FORMAT(date(r.RF_DT),'%Y-%m-%d') as date_serv, 
TIME_FORMAT(time(r.RF_DT),'%h:%i:%s') as time_serv, 
    e.UNIT_NAME as department
from refers r 
LEFT  JOIN ipd_reg i ON r.VISIT_ID = i.VISIT_ID AND i.IS_CANCEL = 0
LEFT  JOIN opd_visits b ON i.VISIT_ID = b.VISIT_ID AND b.IS_CANCEL = 0
LEFT  JOIN cid_hn c ON b.hn = c.hn 
LEFT JOIN population p ON c.cid = p.cid
LEFT  JOIN service_units e ON i.WARD_NO = e.UNIT_ID
AND r.IS_CANCEL = 0
WHERE  b.hn ='${hn}'
AND r.IS_CANCEL = 0 
ORDER BY seq DESC LIMIT 3
    
   `);
    return data[0];
  }

  async getProfileCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    select o.HN as hn, 
           p.cid as cid,
					CASE 
        WHEN PRENAME not in('') THEN PRENAME
        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'สามเณร'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '20' AND p.sex='1' AND p.MARRIAGE = '4'THEN 'พระภิกษุ'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='1' THEN 'เด็กชาย'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='1' THEN 'นาย'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) < '15'  AND p.sex='2' THEN 'เด็กหญิง'

        WHEN TIMESTAMPDIFF(year,p.birthdate,NOW()) >= '15' AND p.sex='2' AND p.MARRIAGE='1' THEN 'นางสาว'
    ELSE 'นาง'
    END AS title_name
          , p.FNAME as first_name
					, p.LNAME as last_name
					, SUBSTR(p.TOWN_ID,7,2) as moopart
					, trim(p.HOME_ADR) as addrpart
					, SUBSTR(p.TOWN_ID,5,2) as tmbpart
					, SUBSTR(p.TOWN_ID,3,2) as amppart
					, SUBSTR(p.TOWN_ID,1,2) as chwpart
					, p.BIRTHDATE as brthdate
          , concat(lpad(timestampdiff(year,p.birthdate,now()),3,'0'),'-'
         ,lpad(mod(timestampdiff(month,p.birthdate,now()),12),2,'0'),'-'
         ,lpad(if(day(p.birthdate)>day(now()),dayofmonth(now())-(day(p.birthdate)-day(now())),day(now())-day(p.birthdate)),2,'0')) as age
				  , CASE
						WHEN p.SEX = 1 THEN 'ชาย'
						WHEN p.SEX = 2 THEN 'หญิง'
						END as sex
				  , replace(replace(oc.oc_name,char(13),''),char(10),'') as occupation
          , m.INSCL as pttype_id
					 , m.INSCL_NAME as pttype_name
           , g.uc_cardid as pttype_no
					 , g.HOSPMAIN  as hospmain
					 , hop.HOSP_NAME as hospmain_name
					 , g.HOSPSUB as hospsub
					 , hs.HOSP_NAME as hospsub_name
           , g.UC_REGISTER as registdate
           , max(date(o.REG_DATETIME)) as visitdate
           , p.FATHER as father_name
	         , p.MOTHER as mother_name
           ,p.TELEPHONE as contact_mobile
           ,'' as couple_name
		      , p.contact as contact_name
	      	, '' as contact_relation
    FROM population as p 
        INNER JOIN cid_hn c on p.CID = c.CID
	INNER JOIN opd_visits o ON c.HN = o.HN
        LEFT JOIN towns t ON p.town_id = t.town_id
        LEFT  JOIN main_inscls m ON p.INSCL = m.INSCL
        LEFT JOIN uc_inscl g ON p.CID = g.CID AND (g.date_abort >= date(o.REG_DATETIME) or day(g.date_abort)=0) 
        LEFT JOIN hospitals hop ON g.HOSPMAIN = hop.HOSP_ID
				LEFT JOIN occupations oc ON p.oc_id = oc.oc_id
	      LEFT JOIN hospitals hs ON g.HOSPSUB = hs.HOSP_ID
        WHERE o.hn =  '${hn}'
			  
        `);
    return data[0];
  }

}

