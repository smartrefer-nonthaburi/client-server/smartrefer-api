import Knex = require('knex');
const hospcode = process.env.HIS_CODE;
var md5 = require('md5');

export class HisHosxpv3ArjaroModel {

  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data =  await db('opduser as o')
        .select('o.loginname as username', 'o.name as fullname')
        .select(db('opdconfig').first('hospitalcode').as('hcode').limit(1)) 
        .where('o.loginname', username)
        .andWhere('o.passweb', pass);
        return data;
  }

  async getServices(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT
          o.vn as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
          c.department as department
    FROM ovst as o
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.hn ='${hn}' and o.vn = '${seq}' 
    UNION
    SELECT
          an.an as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(i.regdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(i.regtime),'%h:%i:%s') as time_serv,
          w.name as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}' and an.an = '${seq}'
    `);
    return data[0];
  }

  async getProfile(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`
    select p.hn as hn, 
    p.cid as cid, 
    p.pname as title_name,
    p.fname as first_name,
    p.lname as last_name,
    s.name as sex,
    oc.name as occupation,
    IF((p.moopart ="" or p.moopart='-'  or p.moopart is null), "00", IF(CHAR_LENGTH(p.moopart ) < 2, LPAD(p.moopart ,2,"0"),p.moopart ) ) AS moopart,
    p.addrpart,
    p.tmbpart,
    p.amppart,
    p.chwpart,
    p.birthday as brthdate,
    concat(lpad(timestampdiff(year,p.birthday,now()),3,'0'),'-',
    lpad(mod(timestampdiff(month,p.birthday,now()),12),2,'0'),'-',
    lpad(if(day(p.birthday)>day(now()),dayofmonth(now())-(day(p.birthday)-day(now())),day(now())-day(p.birthday)),2,'0')) as age,
    o.pttype as pttype_id,
    t.name as pttype_name,
    o.pttypeno as pttype_no,
    o.hospmain,
    c.name as hospmain_name,
    o.hospsub,
    h.name as hospsub_name,
    p.firstday as registdate,
    p.last_visit as visitdate,
    p.fathername as father_name,
    p.mathername as mother_name,
    p.informrelation as contact_name,
    p.informtel as contact_mobile,
    oc.name as occupation
  FROM patient as p 
  INNER JOIN ovst as o on o.hn=p.hn
  left join pttype as t on o.pttype=t.pttype
  left join hospcode as c on o.hospmain=c.hospcode
  left join hospcode as h on o.hospsub=h.hospcode
  LEFT JOIN sex s on s.code = p.sex 
  LEFT JOIN occupation oc on oc.occupation = p.occupation
  WHERE p.hn = '${hn}' ORDER BY o.vstdate DESC 
  LIMIT 1
    `);
    return data[0];
  }

  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`
      SELECT hospitalcode as provider_code,
             hospitalname as provider_name 
      from opdconfig limit 1
     `);
    return data[0];
  }

  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT agent as drug_name, 
           symptom as symptom ,
           begin_date as begin_date,
           date(entry_datetime) as daterecord
    FROM opd_allergy
    WHERE hn ='${hn}'
    `);
    return data[0];
  }

  async getChronic(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT c.icd10 as icd_code,
           cm.regdate as start_date, 
           IF(i.tname!='', i.tname, "-") as icd_name
    FROM clinicmember as cm 
    inner join clinic c on cm.clinic = c.clinic
    INNER JOIN icd101 as i on i.code = c.icd10
    WHERE cm.hn ='${hn}'
    `);
    return data[0];
  }

  async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT o.vn as seq,
        date_format(o.vstdate,'%Y-%m-%d') as date_serv,
        time(o.vsttime) as time_serv,
      if((d.icd10="" or d.icd10 is null),(select ro.pdx from referout ro where ro.vn=o.vn and d.vn ='${seq}' limit 1),d.icd10) as icd_code,
        i.name as icd_name,
        d.diagtype as diag_type, 
    if((dt.provisional_dx_text="" or dt.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and d.vn ='${seq}' limit 1) ,dt.provisional_dx_text) as DiagNote,     
        '' as diagtype_id          
    FROM ovst as o
    INNER JOIN vn_stat v on v.vn=o.vn
    INNER JOIN ovstdiag d on d.vn = o.vn
    INNER JOIN icd101 as i on i.code = d.icd10
    INNER JOIN referout as ro on ro.vn=o.vn
    left JOIN ovstdiag_text dt on o.vn = dt.vn 
    WHERE d.vn ='${seq}' 
    and (o.an = '' or o.an is null)
    UNION
    SELECT  i.an as seq, 
        date_format(i.dchdate,'%Y-%m-%d') as date_serv, 
        time(i.dchtime) as time_serv,
      r.icd10 as icd_code,
      c.name as icd_name,
        r.diagtype as diag_type, 
        c.name as DiagNote,           
        r.diagtype as diagtype_id          
    FROM ipt i
    INNER JOIN (
    select  an,icd10,diagtype,'ipdx' as f from iptdiag id  where id.an = '${seq}'
    union 
    select vn as an,pdx as icd10,'1' as diagtype,'referdx' as f from referout ro where ro.vn='${seq}'
    ) as r on r.an=i.an
    inner join icd101 as c on r.icd10 = c.code
    where if((select count(an) as total from (
    select  an,icd10,diagtype,'ipdx' as f from iptdiag id  where id.an = '${seq}'
    union 
    select vn as an,pdx as icd10,'1' as diagtype,'referdx' as f from referout ro where ro.vn='${seq}'
    )  as r )> 1 , r.f='ipdx' ,r.f='referdx')
    `);
    return data[0];
  }

  async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT a.seq,a.an,a.pid,a.hn,a.referno,a.referdate,a.to_hcode,a.pttype_id,a.strength_id,a.strength_name,a.location_name,a.station_name,a.to_hcode_name,a.refer_cause,a.refertime,a.doctor,a.doctor_name
    FROM (
    select if(LENGTH(r.vn) >=  9,r.vn,o2.vn) as seq,
    o2.an as an,
    r.hn as pid,
    r.hn as hn,
    cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
    r.refer_date as referdate,
    r.hospcode as to_hcode,
    if(r.referout_emergency_type_id='3',1,2) as pttype_id,
    r.refer_cause as strength_id,
    (select name from refer_cause where id = r.refer_cause limit 1 ) as strength_name,
    r.refer_point as location_name,
    r.refer_point as station_name,
    (select name from hospcode where hospcode = r.hospcode limit 1 ) as to_hcode_name,
    (select name from refer_cause where id = refer_cause) as refer_cause,
    refer_time as refertime,
    (select concat(licenseno,',',cid)  from doctor  where code = r.doctor) as doctor,
    (select name  from doctor where code = r.doctor limit 1 ) as doctor_name,
    '' as refer_remark
    from referout as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.an
    where cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(LENGTH(r.vn) >=  9,r.vn,o2.vn))  = '${seq}'
    UNION
    select if(o.an is null,r.vn,o.an) as seq,
    o2.an as an,
    o.hn as pid,
    o.hn as hn,
    cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
      DATE(r.reply_date_time) as referdate,
    r.dest_hospcode as to_hcode,
    "" as pttype_id,
    "" as strength_id,
    "" as strength_name,
    "" as location_name,
    "" as station_name,
    (select name from hospcode where hospcode = r.dest_hospcode limit 1 ) as to_hcode_name,
    ""as refer_cause,
      time(r.reply_date_time) as refertime,
    (select concat(licenseno,',',cid) from doctor where (code = o.doctor or code = o2.doctor)) as doctor,
    (select name  from doctor where (code = o.doctor or code = o2.doctor) limit 1 ) as doctor_name,
    '' as refer_remark
    from refer_reply as r
    left outer join ovst o on r.vn = o.vn
    left outer join ovst o2 on r.vn = o2.vn
    where cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) = '${referno}'
    AND (if(o.an is null,r.vn,o.an))  = '${seq}'
    ) a
    WHERE a.seq is not NULL
    `);
    return data[0];
  }

  async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT a.seq,a.date_serv,a.time_serv,a.drug_name,a.qty,a.unit,a.usage_line1,a.usage_line2,a.usage_line3
    FROM (
    select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           (i.qty) as qty,
           d.units as unit ,
           if((select name1 from drugusage g where g.drugusage = i.drugusage) !='', (select name1 from drugusage g where g.drugusage = i.drugusage), (select s.name1 from sp_use s where s.sp_use = i.sp_use))  as usage_line1,
           if((select name2 from drugusage g where g.drugusage = i.drugusage) !='', (select name2 from drugusage g where g.drugusage = i.drugusage), (select s.name2 from sp_use s where s.sp_use = i.sp_use))  as usage_line2,
           if((select name3 from drugusage g where g.drugusage = i.drugusage) !='', (select name3 from drugusage g where g.drugusage = i.drugusage), (select s.name3 from sp_use s where s.sp_use = i.sp_use))  as usage_line3
    FROM ovst o
    left outer join opitemrece as i on (o.vn = i.vn)
    left outer Join drugitems as d ON i.icode = d.icode
    WHERE (o.an is null or o.an = "" ) and i.icode in(select icode from drugitems) and i.income in ("03","04") and i.vn = '${seq}'
    union
    select o.vn as seq,
           DATE_FORMAT(date(i.rxdate),'%Y%m%d') as date_serv,
           DATE_FORMAT(time(i.rxtime),'%h:%i:%s') as time_serv,
           d.name as drug_name,
           (i.qty) as qty,
           d.units as unit ,
           if((select name1 from drugusage g where g.drugusage = i.drugusage) !='', (select name1 from drugusage g where g.drugusage = i.drugusage), (select s.name1 from sp_use s where s.sp_use = i.sp_use))  as usage_line1,
           if((select name2 from drugusage g where g.drugusage = i.drugusage) !='', (select name2 from drugusage g where g.drugusage = i.drugusage), (select s.name2 from sp_use s where s.sp_use = i.sp_use))  as usage_line2,
           if((select name3 from drugusage g where g.drugusage = i.drugusage) !='', (select name3 from drugusage g where g.drugusage = i.drugusage), (select s.name3 from sp_use s where s.sp_use = i.sp_use))  as usage_line3
    FROM ovst o
    left outer join opitemrece as i on (o.an = i.an)
    left outer Join drugitems as d ON i.icode = d.icode
    WHERE (i.an is not null and i.vn is null ) and i.icode in(select icode from drugitems)  and i.income in ("03","04") and o.vn = '${seq}'
    ) a
    WHERE a.seq is not NULL
    `);
    return data[0];
  }

  async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    select receive_date as 'date_serv',
    receive_time as 'time_serv',
    form_name as 'labgroup',
    lab_items_name_ref as 'lab_name',
    REPLACE(lab_order_result,"\r\n"," ") as lab_result,
    li.lab_items_unit as 'unit',
    if(lo.lab_items_normal_value_ref ="",if((li.lab_items_normal_value is null or li.lab_items_normal_value=''),"is null",li.lab_items_normal_value),lo.lab_items_normal_value_ref) as standard_result
    from ovst o
    inner join lab_head lh on o.vn = lh.vn or o.an = lh.vn
    inner join lab_order lo on lh.lab_order_number = lo.lab_order_number
    inner join lab_items li on li.lab_items_code = lo.lab_items_code
    where lh.vn = '${seq}' and lo.lab_order_result is not NULL
    `);
    return data[0];
  }

  async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT o.vn as seq, 
           o.vstdate as date_serv, 
           o.nextdate as date, 
           o.nexttime as time, 
           (select name from clinic where clinic = o.clinic ) as department, 
           o.app_cause as detail, 
           time(ovst.vsttime) as time_serv
    FROM oapp as o 
    INNER JOIN ovst on ovst.vn = o.vn
    WHERE o.vn ='${seq}'
    `);
    return data[0];
  }

  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`
    SELECT o.vstdate as date_serv,
           o.vsttime as time_serv,
           pv.export_vaccine_code as vaccine_code,
           pv.vaccine_name as vaccine_name
    from ovst_vaccine ov
    left outer join  person_vaccine pv on ov.person_vaccine_id = pv.person_vaccine_id     
    LEFT OUTER JOIN  ovst o on o.vn = ov.vn
    WHERE o.hn = '${hn}'
    `);
    return data[0];
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT o.hn as pid,
    o.vn as seq,
    o.vstdate as date_serv,	
    o.vsttime as time_serv,
    od.icd10 as procedure_code,
    ic.name as procedure_name,
    DATE_FORMAT(date(od.vstdate),'%Y%m%d') as start_date,	
    DATE_FORMAT(time(od.vsttime),'%h:%i:%s') as start_time,
    DATE_FORMAT(date(od.vstdate),'%Y%m%d') as end_date,
    '00:00:00' as end_time
    FROM ovst o
    LEFT OUTER JOIN ovstdiag od on od.vn = o.vn
    INNER JOIN icd9cm1 ic on ic.code = od.icd10 
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,od.icd10
    UNION
    SELECT o.hn as pid,
        o.vn as seq,
        o.vstdate as date_serv,	
        o.vsttime as time_serv,
        i.icd9 as procedure_code,
        c.name as procedure_name,
        DATE_FORMAT(date(i.opdate),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(i.optime),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(i.enddate),'%Y%m%d') as end_date,
        '00:00:00' as end_time
    FROM ovst o
    inner JOIN iptoprt i on i.an = o.an
    INNER JOIN icd9cm1 c on c.code = i.icd9
    WHERE o.vn = '${seq}'
    GROUP BY o.vn,i.icd9   
    `);
    return data[0];
  }

  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
   select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           REPLACE(s.cc,"\r\n"," ") as symptom,
           o.main_dep as depcode,
           k.department as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if((ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and o.vn ='${seq}' limit 1) ,ovstdiag_text.provisional_dx_text) as diag_text,
      '' as pupil_right, '' as pupil_left,REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ovst as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    LEFT JOIN kskdepartment k ON k.depcode = o.main_dep
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.vn
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.vn  = '${seq}'
    UNION
    select o.vn as seq,o.hn,
           DATE_FORMAT(date(o.regdate),'%Y-%m-%d') as date_serv,
           DATE_FORMAT(time(o.regtime),'%h:%i:%s') as time_serv,
           p.bloodgrp as bloodgrp,
           s.bw as weight,
           s.height as height,
           s.bmi as bmi,
           s.temperature as temperature,
           s.pulse as pr,
           s.rr as rr,
           s.bps as sbp,
           s.bpd as dbp,
           replace(replace(s.cc,char(13),' '),char(10),' ') as symptom,
           ""as depcode,
          "" as department,
           n.gcs_m as movement_score,n.gcs_v as vocal_score,n.gcs_e as eye_score,n.o2sat as oxygen_sat,(n.gcs_e+n.gcs_v+n.gcs_m) as gak_coma_sco,
           if((ovstdiag_text.provisional_dx_text="" or ovstdiag_text.provisional_dx_text is null),(select ro.pre_diagnosis from referout ro where ro.vn=o.vn and o.an ='${seq}' limit 1) ,ovstdiag_text.provisional_dx_text) as diag_text,
          '' as pupil_right, '' as pupil_left,REPLACE(ro.pmh,"\r\n"," ") as pmh
    FROM ipt as o
    LEFT JOIN opdscreen as s on o.vn = s.vn
    INNER JOIN patient as p ON p.hn = o.hn
    LEFT JOIN referout as ro on ro.vn=o.an
    LEFT JOIN  er_nursing_detail as n ON o.vn=n.vn
    left outer join ovstdiag_text on ovstdiag_text.vn=o.vn
    WHERE o.an  ='${seq}'
    `);
    return data[0];
  }

  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
        SELECT o.vn as seq, REPLACE(o.pe,"\r\n"," ") as pe
        FROM opdscreen o 
        WHERE o.vn  = '${seq}'
        `);
    return data[0];
  }

  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
            select x.seq, REPLACE(GROUP_CONCAT(DISTINCT x.hpi),","," ") as hpi from 
            (
             SELECT o.vn as seq , REPLACE(o.hpi,"\r\n"," ") as hpi 
              FROM referout as o WHERE o.vn = '${seq}'
             union
             SELECT o.vn as seq , REPLACE(o.hpi,"\r\n"," ") as hpi 
                FROM opdscreen as o WHERE o.vn = '${seq}'
             ) as x
    `);
    return data[0];
  }

  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.vn = '${seq}' 
    UNION
    SELECT x.request_date as xray_date ,i.xray_items_name  as xray_name
    FROM xray_report x
    left outer join xray_items i on i.xray_items_code = x.xray_items_code  
    WHERE x.confirm = 'Y'  and x.confirm_read_film <> 'Y' and x.an = '${seq}' 
    GROUP BY x.xn
    `);
    return data[0];
  }

  async getBedsharing(db: Knex) {
    let data = await db.raw(`
  select 
    w.ward as ward_code,
    w.name as ward_name,
    count(i.an) as ward_pt,
    count(DISTINCT(b.bedno)) as ward_bed, 
    w.bedcount as ward_standard
    from bedno b  
    left outer join roomno r on r.roomno = b.roomno  
    left outer join ward w on w.ward = r.ward  
    left outer join iptadm a on a.bedno = b.bedno  
    left outer join ipt i on i.an = a.an and i.dchdate is null
where b.bed_status_type_id is null
and w.ward_active<>"N"
    group by w.name
    `);
    return data[0];
  }

  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
           SELECT r.vn as seq,r.hn,
           o2.an as an,
           pt.pname as title_name,
           pt.fname as first_name, 
           pt.lname as last_name,
           cast(replace(r.refer_number,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
           r.refer_date as referdate,
           r.hospcode as to_hcode,
           r.refer_point as location_name, 
           if(rt.referout_type_id = '1','1','2') as pttype_id,
           if(rt.referout_type_id = '1' = '1','Non Trauma','Trauma') as typept_name,
           r.referout_emergency_type_id as strength_id, 
           (case r.referout_emergency_type_id
                 when 1 then 'Resucitate'
                 when 2 then 'Emergency'
                 when 3 then 'Urgency'
                 when 4 then 'Semi Urgency'
                 when 5 then 'Non Urgency'
                 else '' end 
           ) as strength_name,
           h.name as to_hcode_name,
           rr.name as refer_cause,
           time(r.refer_time) as refertime, 
           d.licenseno as doctor,
           d.name as doctor_name
    FROM referout r
    INNER JOIN patient pt on pt.hn = r.hn
    left outer join ovst o2 on r.vn = o2.an
    LEFT OUTER JOIN referout_type rt on rt.referout_type_id = r.referout_type_id
    LEFT OUTER JOIN doctor d on d.code = r.doctor
    LEFT OUTER JOIN rfrcs  rr on rr.rfrcs = r.rfrcs
    LEFT OUTER JOIN hospcode h on h.hospcode = r.hospcode
    WHERE r.refer_date between '${start_date}' and '${end_date}'  
    AND  r.refer_number not in (SELECT refer_reply.doc_no FROM refer_reply WHERE (refer_reply.doc_no is not null) AND Date(refer_reply.reply_date_time) between '${start_date}' and '${end_date}'  )
    GROUP BY referno
    `);
    return data[0];
  }

  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT if(o.an is null,r.vn,o.an) as seq,
           o.an as an,
           pt.hn,
           pt.pname as title_name,
           pt.fname as first_name, 
           pt.lname as last_name,
           cast(replace(r.doc_no,"/",'-') as char(20) CHARACTER SET UTF8) as referno,
           DATE(r.reply_date_time) as referdate,
           r.dest_hospcode as to_hcode,
           h.name as to_hcode_name,
           rr.NAME as refer_cause,
           time(r.reply_date_time) as refertime,
           d.licenseno as doctor,
           d.name as doctor_name
    from refer_reply r
    INNER JOIN ovst o on o.vn = r.vn
    INNER JOIN patient pt on pt.hn = o.hn
    LEFT OUTER JOIN rfrcs rr on rr.rfrcs=o.rfrics 
    LEFT OUTER JOIN hospcode h on h.hospcode = r.dest_hospcode
    LEFT OUTER JOIN doctor d on d.code = r.reply_doctor
    WHERE date(r.reply_date_time) between '${start_date}' and '${end_date}'
    `);
    return data[0];
  }

  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db.raw(`
    SELECT a.vn as seq,
           a.nextdate as receive_apppoint_date,
           a.nexttime as receive_apppoint_beginttime,
           a.endtime as receive_apppoint_endtime,
           a.app_user as receive_apppoint_doctor,
           group_concat(distinct c.name) as receive_apppoint_chinic,
           group_concat(distinct a.app_cause) as receive_text,
           '' as receive_by
    FROM oapp a
    LEFT OUTER JOIN clinic c on c.clinic = a.clinic
    WHERE a.hn = '${hn}' AND a.vstdate = '${app_date}'
    `);
    return data[0];
  }

  async getDepartment(db: Knex) {
    let data = await db.raw(`
    select *, depcode as dep_code,department as dep_name from kskdepartment WHERE on_desk = 'Y';
    `);
    return data[0];
  }

  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
      select hn from patient where cid = '${cid}'
        `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        select  '' as drug_hospcode ,'' as drug_hospname
        ,concat(s.name,' ',s.strength,' ',s.units) as drug_name
        ,if((select concat(d.name1,d.name2,d.name3) from drugusage d where d.drugusage = o.drugusage) = ''
        ,(select concat(d.name1,d.name2,d.name3) from drugusage d where d.drugusage = o.drugusage)
        ,(select CONCAT(u.name1,u.name2,u.name3) from sp_use u where u.sp_use = o.sp_use)) as drug_use
        ,o.rxdate as drug_receive_date
        from opitemrece o
        left  join s_drugitems s on s.icode=o.icode
        left  join drugusage d on d.drugusage=o.drugusage
        left  join drugitems i on i.icode=o.icode
        left  join sp_use u on u.sp_use=o.sp_use
        where o.hn='${hn}'  
        and o.income in ("03","04")
        and o.rxdate>=date_add(curdate(),interval -3 month)
        group by o.icode
        order by o.rxdate
        `);
    return data[0];
  }
  
  async getServicesCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    SELECT
    o.hn,
          o.vn as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(o.vstdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(o.vsttime),'%h:%i:%s') as time_serv,
          c.department as department
    FROM ovst as o
    INNER JOIN kskdepartment as c ON c.depcode = o.main_dep
    INNER JOIN patient as p	ON p.hn = o.hn
    WHERE o.hn ='${hn}'
    UNION
    SELECT
    an.hn,
          an.an as seq,
          p.pname as title_name,
          p.fname as first_name,
          p.lname as last_name,
          DATE_FORMAT(date(i.regdate),'%Y-%m-%d') as date_serv,
          DATE_FORMAT(time(i.regtime),'%h:%i:%s') as time_serv,
          w.name as department
    FROM an_stat as an
    INNER JOIN ward as w ON w.ward = an.ward
    INNER JOIN patient as p ON p.hn = an.hn
    INNER JOIN ipt as i On i.an = an.an
    WHERE an.hn ='${hn}'
    ORDER BY seq DESC limit 3
    `);
    return data[0];
  }

  async getProfileCoc(db: Knex, hn: any) {
    let data = await db.raw(`
    select p.hn as hn, 
    p.cid as cid, 
    p.pname as title_name,
    p.fname as first_name,
    p.lname as last_name,
    s.name as sex,
    oc.name as occupation,
    IF((p.moopart ="" or p.moopart='-'  or p.moopart is null), "00", IF(CHAR_LENGTH(p.moopart ) < 2, LPAD(p.moopart ,2,"0"),p.moopart ) ) AS moopart,
    p.addrpart,
    p.tmbpart,
    p.amppart,
    p.chwpart,
    p.birthday as brthdate,
    concat(lpad(timestampdiff(year,p.birthday,now()),3,'0'),'-',
    lpad(mod(timestampdiff(month,p.birthday,now()),12),2,'0'),'-',
    lpad(if(day(p.birthday)>day(now()),dayofmonth(now())-(day(p.birthday)-day(now())),day(now())-day(p.birthday)),2,'0')) as age,
    o.pttype as pttype_id,
    t.name as pttype_name,
    o.pttypeno as pttype_no,
    o.hospmain,
    c.name as hospmain_name,
    o.hospsub,
    h.name as hospsub_name,
    p.firstday as registdate,
    p.last_visit as visitdate,
    p.fathername as father_name,
    p.mathername as mother_name,
    p.informrelation as contact_name,
    p.informtel as contact_mobile,
    oc.name as occupation
  FROM patient as p 
  INNER JOIN ovst as o on o.hn=p.hn
  left join pttype as t on o.pttype=t.pttype
  left join hospcode as c on o.hospmain=c.hospcode
  left join hospcode as h on o.hospsub=h.hospcode
  LEFT JOIN sex s on s.code = p.sex 
  LEFT JOIN occupation oc on oc.occupation = p.occupation
  WHERE p.hn = '${hn}' ORDER BY o.vstdate DESC 
  LIMIT 1
    `);
    return data[0];
  }
  
}
