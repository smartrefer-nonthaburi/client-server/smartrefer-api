import Knex = require('knex');
var md5 = require('md5');
const hospcode:any = process.env.HIS_CODE;

export class HisHomcModel {
  /* OK */
  async getLogin(db: Knex, username: any, password: any) {
    let pass = md5(password).toUpperCase();
    let data = await db(`profile`)
    .select(db.raw(`RTRIM(LTRIM(profile.UserCode)) as username`),db.raw(`CONCAT(RTRIM(LTRIM(profile.firstName)),' ',RTRIM(LTRIM(profile.lastName))) as fullname`),db.raw(`'10669' as hcode`))
    .whereRaw(`RTRIM(LTRIM(profile.UserCode)) = ?`,[username])
    .andWhereRaw(`profile.PassCode = ?`,[pass])
    return data[0];
  }
  /* OK */
  async getHospital_knex(db: Knex, hn: any) {
    // let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '${hospcode}'`);
    let data = await db('HOSPCODE') .select('OFF_ID as provider_code', db.raw('rtrim(NAME) as provider_name')) .where('OFF_ID', hospcode);
    return data;
  }
  async getHospital(db: Knex, hn: any) {
    let data = await db.raw(`SELECT OFF_ID as provider_code,rtrim(NAME) as provider_name from HOSPCODE where OFF_ID = '${hospcode}'`);
    return data;
  }
  
  // ข้อมูลพื้นฐานผู้ป่วย >> OK
  async getProfile_knex(db: Knex, hn: any, seq: any, referno: any) {
  
      let data = await db('PATIENT as p') .leftJoin('PTITLE as t', 'p.titleCode', 't.titleCode') .leftJoin('PatSS as ps', 'p.hn', 'ps.hn') .leftJoin('OPD_H as o', 'p.hn', 'o.hn') .leftJoin('Bill_h as i', function() { this.on('o.hn', '=', 'i.hn') .andOn('o.regNo', '=', 'i.regNo'); }) .leftJoin('Paytype as pt', 'i.useDrg', 'pt.pay_typecode') .leftJoin('DEPT as d', 'o.dept', 'd.deptCode') .leftJoin('HOSPCODE as h1', 'i.HMAIN', 'h1.OFF_ID') .leftJoin('HOSPCODE as h2', 'i.HSUB', 'h2.OFF_ID') .select( db.raw('LTRIM(RTRIM(p.hn)) as hn'), db.raw('RTRIM(t.titleName) as title_name'), db.raw('RTRIM(p.firstName) as first_name'), db.raw('RTRIM(p.lastName) as last_name'), db.raw('RTRIM(ps.CardID) AS cid'), 'p.moo as moopart', db.raw("(RTRIM(LTRIM(p.addr1)) + ' ' + RTRIM(LTRIM(p.addr2))) as addrpart"), 'p.tambonCode as tmbpart', db.raw("SUBSTRING(p.regionCode, 3, 2) as amppart"), db.raw("SUBSTRING(p.areaCode, 3, 2) as chwpart"), db.raw("CONVERT(date, CONVERT(char, CONCAT(SUBSTRING(p.birthDay, 1, 4), CASE WHEN SUBSTRING(p.birthDay, 5, 2) = '00' THEN '01' ELSE SUBSTRING(p.birthDay, 5, 2) END, CASE WHEN SUBSTRING(p.birthDay, 7, 2) = '00' THEN '01' ELSE SUBSTRING(p.birthDay, 7, 2) END) - 5430000)) as brthdate"), db.raw("(CASE WHEN RIGHT(p.birthDay, 4) = '0000' THEN RIGHT('000' + LTRIM(STR(YEAR(GETDATE()) + 543 - LEFT(p.birthDay, 4))), 3) + '-00-00' ELSE RIGHT('000' + LTRIM(STR(YEAR(GETDATE()) + 543 - YEAR(p.birthDay))), 3) + '-' + RIGHT('00' + LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, GETDATE())) % 12)), 2) + '-' + RIGHT('00' + LTRIM(STR(REPLACE(DATEDIFF(DAY, DAY(p.birthDay), DAY(GETDATE())), '-', ''))), 2) END) as age"), 'i.useDrg as pttype_id', db.raw('RTRIM(pt.pay_typedes) as pttype_name'), db.raw('RTRIM(LTRIM(ps.SocialID)) as pttype_no'), 'i.HMAIN as hospmain', 'h1.NAME as hospmain_name', 'i.HSUB as hospsub', 'h2.NAME as hospsub_name', db.raw('CONVERT(date, CONVERT(char, o.registDate - 5430000)) as registDate'), db.raw('(SELECT TOP 1 CONVERT(date, CONVERT(char, VisitDate - 5430000)) FROM PATDIAG WITH(NOLOCK) WHERE Hn = p.hn GROUP BY Hn, VisitDate ORDER BY VisitDate DESC) as VisitDate'), 'ps.father as father_name', 'p.mother as mother_name', db.raw("'' as couple_name"), 'ps.relatives as contact_name', 'ps.relationship as contact_relation', db.raw("'' as contact_mobile"), db.raw("(CASE WHEN p.sex = 'ช' THEN 'ชาย' ELSE 'หญิง' END) as sex"), db.raw('(SELECT occdes FROM Occup WHERE p.occupation = occcode) as occupation') ) .where(db.raw("p.hn = dbo.padc(?, ' ', 7)", [hn])) .orderBy('o.registDate', 'desc');
    return data[0];
  }
  async getProfile(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT TOP 1 ltrim(rtrim(p.hn)),rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
    , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
    , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
    ,convert(date,convert(char, concat(substring(p.birthDay,1,4), case when substring(p.birthDay,5,2)='00' then '01' else substring(p.birthDay,5,2) end, case when substring(p.birthDay,7,2)='00' then '01' else substring(p.birthDay,7,2) end) -5430000)) as brthdate
    ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
    else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
    RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
    RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age
    , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
    , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
    ,convert(date,convert(char,o.registDate -5430000))  as registDate
    ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
     ,ps.father as father_name 
    ,p.mother as mother_name
    ,'' as couple_name
    ,ps.relatives as contact_name
    ,ps.relationship as contact_relation
    ,'' as contact_mobile
    ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
    ,(select occdes from Occup where p.occupation = occcode) as occupation
    FROM PATIENT p
    LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
    LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
    LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
    LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
    LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
    LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
    LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
    LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
    WHERE p.hn=dbo.padc('${hn}',' ',7)
    ORDER BY o.registDate DESC`);
    return data[0];
  }
  async getVaccine_knex(db: Knex, hn: any, referno: any) {
    // let data = await db.raw(`select convert(date,convert(char,o.registDate -5430000)) as date_serv,
    // CONVERT (time,(left (o.timePt,2)+':'+right (o.timePt,2))) as time_serv,
    // p.VACCODE as vaccine_code,v.VACNAME as vaccine_name 
    // from PPOP_EPI p
    // left join OPD_H o on(o.hn = p.HN and p.REGNO = o.regNo)
    // left join PPOP_VACCINE v on(v.VACCODE = p.VACCODE)
    // where p.HN = dbo.padc('${hn}',' ',7)`);
    let data = await db('PPOP_EPI as p') .select( db.raw('convert(date, convert(char, o.registDate - 5430000)) as date_serv'), db.raw("CONVERT(time, (left(o.timePt, 2) + ':' + right(o.timePt, 2))) as time_serv"), 'p.VACCODE as vaccine_code', 'v.VACNAME as vaccine_name' ) .leftJoin('OPD_H as o', function () { this.on('o.hn', 'p.HN').andOn('p.REGNO', 'o.regNo'); }) .leftJoin('PPOP_VACCINE as v', 'v.VACCODE', 'p.VACCODE') .where('p.HN', db.raw(`dbo.padc(?, ' ', 7)`, [hn]));
    return data;
  }
  async getVaccine(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`select convert(date,convert(char,o.registDate -5430000)) as date_serv,
    CONVERT (time,(left (o.timePt,2)+':'+right (o.timePt,2))) as time_serv,
    p.VACCODE as vaccine_code,v.VACNAME as vaccine_name 
    from PPOP_EPI p
    left join OPD_H o on(o.hn = p.HN and p.REGNO = o.regNo)
    left join PPOP_VACCINE v on(v.VACCODE = p.VACCODE)
    where p.HN = dbo.padc('${hn}',' ',7)`);
    return data;
  }
  async getChronic(db: Knex, hn: any, referno: any) {
    let data = await db('PATDIAG as p') .distinct() .select( db.raw("rtrim(p.ICDCode) as icd_code"), db.raw("rtrim(ic.DES) as icd_name"), "p.VisitDate as start_date" ) .leftJoin('OPD_H as o', function() { this.on('o.hn', '=', 'p.Hn') .andOn('o.regNo', '=', 'p.regNo'); }) .leftJoin('ICD101 as ic', 'ic.CODE', 'p.ICDCode') .where('m.hn', db.raw("dbo.padc(?, ' ', 7)", [hn])) .andWhere('p.DiagType', 'I') .andWhere('p.dxtype', '1') .andWhere(function() { this.whereBetween('p.ICDCode', ['I60', 'I698']) .orWhereBetween('p.ICDCode', ['J45', 'J46']) .orWhereBetween('p.ICDCode', ['I10', 'I159']) .orWhereBetween('p.ICDCode', ['A15', 'A199']) .orWhereBetween('p.ICDCode', ['E10', 'E149']) .orWhereBetween('p.ICDCode', ['F30', 'F399']) .orWhereBetween('p.ICDCode', ['J43', 'J449']) .orWhereBetween('p.ICDCode', ['J429', 'J429']) .orWhereBetween('p.ICDCode', ['I20', 'I259']) .orWhereBetween('p.ICDCode', ['I05', 'I099']) .orWhereBetween('p.ICDCode', ['I26', 'I289']) .orWhereBetween('p.ICDCode', ['I30', 'I528']) .orWhereBetween('p.ICDCode', ['G80', 'G839']) .orWhereBetween('p.ICDCode', ['D50', 'D649']) .orWhereBetween('p.ICDCode', ['N17', 'N19']); }) .orderBy('p.VisitDate', 'asc');
     return data[0];
  }

  // ประวัติแพ้ยา >> OK
  async getAllergyDetail_knex(db: Knex, hn: any, referno: any) {
    let data = await db('medalery as m') .leftJoin('Med_inv as v', 'm.medCode', 'v.abbr') .select({ drug_name: 'v.gen_name', symptom: 'm.alergyNote', begin_date: db.raw( "CONVERT(date, LEFT(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)), 4) + '-' + RIGHT(LEFT(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)), 6), 2) + '-' + RIGHT(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)), 2))" ), daterecord: db.raw( "CONVERT(date, LEFT(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)), 4) + '-' + RIGHT(LEFT(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)), 6), 2) + '-' + RIGHT(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)), 2))" ), }) .where('m.hn', db.raw("dbo.padc(?, ' ', 7)", [hn])) .orderBy('m.updDate', 'desc');
    return data;
  }
  async getAllergyDetail(db: Knex, hn: any, referno: any) {
    let data = await db.raw(`select v.gen_name as drug_name,m.alergyNote as symptom 
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updDate)),2)) as begin_date
    ,convert(date,left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),4)+'-'+right(left(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),6),2)+'-'+right(dbo.ymd2cymd(dbo.ce2ymd(m.updTime)),2)) as daterecord
    from medalery m left join Med_inv v on m.medCode=v.abbr where m.hn=dbo.padc('${hn}',' ',7) order by m.updDate desc`);
    return data;
  }

  /* OK */
  async getServices_knex(db: Knex, hn: any, seq: any, referno: any) {
    let data:any = db.select([ db.raw('ltrim(rtrim(o.hn)) + o.regNo as seq'), db.raw('convert(date,convert(char,o.registDate -5430000)) as date_serv'), db.raw('left(o.timePt,2)+\':\'+right (o.timePt,2) as time_serv'), 'd.deptDesc as department' ]) .from('OPD_H as o') .leftJoin('DEPT as d', 'd.deptCode', 'o.dept') .leftJoin('Refer as r', 'o.hn', 'r.Hn') .where(db.raw('o.hn = dbo.padc(?, " ", 7)', [hn])) .andWhere(db.raw('o.regNo = right(?, 2)', [seq])) .unionAll( db .select([ db.raw('i.hn+i.regist_flag as seq'), db.raw('convert(date,convert(char,i.admit_date -5430000)) as date_serv'), db.raw('left(i.admit_time,2)+\':\'+right(i.admit_time,2) as time_serv'), 'w.ward_name as department' ]) .from('Ipd_h as i') .leftJoin('Ward as w', 'w.ward_id', 'i.ward_id') .leftJoin('Refer as r', 'i.hn', 'r.Hn') .where(db.raw('i.hn = dbo.padc(?, " ", 7)', [hn])) .andWhere(db.raw('i.regist_flag = right(?, 2)', [seq])) );
    return data[0];
  }
  async getServices(db: Knex, hn: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
    ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
    from OPD_H as o 
    left join DEPT d on(d.deptCode = o.dept) 
    left join Refer r on o.hn=r.Hn 
    where o.hn = dbo.padc('${hn}',' ',7) and o.regNo =  right('${seq}' ,2)
    union all 
    SELECT i.hn+i.regist_flag as seq
    ,convert(date,convert(char,i.admit_date -5430000)) as date_serv
    ,left(i.admit_time,2)+':'+right(i.admit_time,2) as time_serv
    ,w.ward_name as department
    from Ipd_h as i
    left join Ward w on w.ward_id = i.ward_id
    left join Refer r on i.hn = r.Hn
    where i.hn = dbo.padc('${hn}',' ',7) and i.regist_flag = right('${seq}' ,2)
    `);
    return data[0];
  }
  // การวินิจฉัย  >> OK
  async getDiagnosis_knex(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data =db('PATDIAG as p') .select( db.raw("LTRIM(RTRIM(p.Hn)) + '-' + p.regNo AS seq"), db.raw("CONVERT(date, CONVERT(char, p.VisitDate - 5430000)) AS date_serv"), db.raw("SUBSTRING(o.timePt, 1, 2) + ':' + SUBSTRING(o.timePt, 3, 4) AS time_serv"), 'p.ICDCode AS icd_code', db.raw("ICD101.CODE + '>' + ICD101.DES AS icd_name"), 'p.DiagType AS diag_type', db.raw("ISNULL(p.DiagNote, '') AS DiagNote"), 'p.dxtype AS diagtype_id' ) .leftJoin('OPD_H as o', function () { this.on('o.hn', '=', 'p.Hn').andOn('o.regNo', '=', 'p.regNo'); }) .leftJoin('ICD101', 'p.ICDCode', 'ICD101.CODE') .whereIn('p.pt_status', ['O', 'Z']) .andWhere(function () { this.whereIn('p.DiagType', ['I', 'E']) .andWhere('o.hn', db.raw("dbo.padc(?, ' ', 7)", ['${hn}'])) .andWhere(db.raw("RIGHT(?, 2)", ['${vn}']), '=', 'p.regNo'); }) .orderBy('p.VisitDate', 'desc');
    return data;
  }
  async getDiagnosis(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`SELECT ltrim(rtrim(p.Hn)) + '-' + p.regNo AS seq
    ,convert(date,convert(char,p.VisitDate -5430000)) AS date_serv
    ,SUBSTRING(o.timePt,1,2)+':'+SUBSTRING(o.timePt,3,4) as time_serv
    ,p.ICDCode as icd_code,ICD101.CODE + '>' + ICD101.DES AS icd_name
    , p.DiagType AS diag_type,isnull(p.DiagNote,'') as DiagNote 
    ,p.dxtype AS diagtype_id
    FROM PATDIAG p
    left join OPD_H o on(o.hn = p.Hn and o.regNo = p.regNo) 
    LEFT JOIN ICD101 ON p.ICDCode = ICD101.CODE 
    WHERE (p.pt_status in('O','Z')) and p.DiagType in('I','E')
    AND o.hn =dbo.padc('${hn}',' ',7) 
    AND p.regNo = right('${vn}' ,2)
    order by p.VisitDate desc`);
    return data;
  }
  /* ข้อมูล Refer >> OK */
  async getRefer_knex(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db('Refer as r') .distinct() .select( db.raw("LTRIM(RTRIM(r.Hn)) + r.RegNo as seq"), db.raw("ISNULL(i.ladmit_n, '') as an"), db.raw("LTRIM(RTRIM(r.Hn)) as pid"), db.raw("LTRIM(RTRIM(r.Hn)) as hn"), db.raw("LTRIM(RTRIM(r.ReferRunno)) as referno"), db.raw("CONVERT(date, CONVERT(char, r.ReferDate - 5430000)) as referdate"), 'r.ReferHCODE as to_hcode', 'h.NAME as to_hcode_name', db.raw("ISNULL(rs.REASON_DESC, '') as refer_cause"), db.raw("FORMAT(GETDATE(), N'HH:mm') as refertime"), 'dd.docCode', db.raw("CONCAT(c.docName, ' ', c.docLName) as doctor_name"), 's.CardID', db.raw("CONCAT(dd.docCode, ',', c.DoctorCID) as doctor") ) .leftJoin('HOSPCODE as h', 'r.ReferHCODE', 'h.OFF_ID') .leftJoin('REFERRS as rs', 'r.ReferReason', 'rs.REASON_CODE') .leftJoin('Ipd_h as i', function() { this.on('r.Hn', '=', 'i.hn').andOn('r.RegNo', '=', 'i.regist_flag'); }) .leftJoin('Deptq_d as dd', function() { this.on('dd.hn', '=', 'r.Hn').andOn('dd.regNo', '=', 'r.RegNo'); }) .leftJoin('DOCC as c', 'c.docCode', 'dd.docCode') .leftJoin('PATIENT as p', 'p.hn', 'r.Hn') .leftJoin('PatSS as s', 'p.hn', 's.hn') .where('r.ReferStatus', '<>', 'I') .andWhere(db.raw("r.Hn = dbo.padc(?, ' ', 7)", [hn])) .andWhere(db.raw("r.RegNo = RIGHT(?, 2)", [vn])) .andWhere('r.ReferRunno', referno);
    return data;
  }
  async getRefer(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`
    select DISTINCT ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , isnull(i.ladmit_n, '') as an
      , ltrim(rtrim(r.Hn)) as pid
      , ltrim(rtrim(r.Hn)) as hn
      , ltrim(rtrim(r.ReferRunno)) as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'HH:mm') as refertime
      , concat(c.docName,' ',c.docLName) as doctor_name
      , CONCAT(dd.docCode, ',', c.DoctorCID) as doctor
    from Refer r
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
  LEFT JOIN Deptq_d dd on (dd.hn = r.Hn and dd.regNo = r.RegNo)
  LEFT JOIN DOCC c on (c.docCode = dd.docCode)
  LEFT JOIN PATIENT P on (P.hn = r.Hn)
  left join PatSS s on(P.hn=s.hn)
    where r.ReferStatus <> 'I' 
    and r.Hn = dbo.padc('${hn}',' ',7) and r.RegNo = right('${vn}', 2) and r.ReferRunno = '${referno}'
    `);
    return data;
  }

  /* OK */
  async getProcedure_knex(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db('ORREQ_H as o') .leftJoin('OPD_H as p', function() { this.on('o.HN', '=', 'p.hn') .andOn('o.REGNO', '=', 'p.regNo'); }) .select( db.raw("LTRIM(RTRIM(p.hn)) + o.REGNO as seq"), 'o.OR_DATE as date_serv', 'o.OR_TIME as time_serv', 'o.ORCODE as procedure_code', 'o.ORDESC as icd_name', 'o.START_DATE as start_date', 'o.END_DATE as end_date' ) .whereRaw(`o.HN = dbo.padc(?, ' ', 7)`, [hn]) .andWhereRaw('o.REGNO = RIGHT(?, 2)', [vn]) .orderBy('date_serv', 'desc');
    return data;
  }
  async getProcedure(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + o.REGNO as seq,
      (o.OR_DATE) as date_serv, o.OR_TIME as time_serv, o.ORCODE as procedure_code, o.ORDESC as icd_name,
      (o.START_DATE) as start_date, (o.END_DATE) as end_date
    from ORREQ_H o
    left join OPD_H p on(o.HN = p.hn and o.REGNO = p.regNo)
    where o.HN = dbo.padc('${hn}',' ',7) and o.REGNO = right('${vn}', 2) 
    order by date_serv desc `);
    return data;
  }

  /* OK */
  async getDrugs_knex(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(` select x.* from ( select DISTINCT ltrim(rtrim(pm.hn)) + pm.registNo as seq, convert(date, convert(char, h.registDate - 5430000)) as date_serv, SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) as time_serv, m.name as drug_name, RTRIM(pm.accAmt) as qty, RTRIM(pm.unit) as unit, RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1, '' as usage_line2, '' as usage_line3 from Patmed pm(NOLOCK) left join OPD_H h(NOLOCK) on (h.hn = pm.hn and h.regNo = pm.registNo) left join Med_inv m(NOLOCK) on (pm.invCode = m.code and m.site = '1') where pm.hn = dbo.padc('${hn}', ' ', 7) and h.ipdStatus = '0' AND pm.registNo = right('${vn}', 2) union all select DISTINCT i.hn + i.regist_flag as seq, convert(date, convert(char, i.admit_date - 5430000)) as date_serv, left(i.admit_time, 2) + ':' + right(i.admit_time, 2) as time_serv, m.name as drug_name, pm.accAmt as qty, m.unit as unit, RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) as usage_line1, '' as usage_line2, '' as usage_line3 from Patmed pm(NOLOCK) left join Ipd_h i(NOLOCK) on (i.hn = pm.hn and i.regist_flag = pm.registNo) left join Med_inv m(NOLOCK) on (pm.invCode = m.code and m.site = '1') where pm.hn = dbo.padc('${hn}', ' ', 7) and i.ladmit_n != '' AND i.regist_flag = right('${vn}', 2) ) as x order by x.date_serv desc, x.time_serv desc `);
    return data;
  }
  async getDrugs(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(` SELECT x.* FROM ( SELECT DISTINCT LTRIM(RTRIM(pm.hn)) + pm.registNo AS seq, CONVERT(date, CONVERT(char, h.registDate - 5430000)) AS date_serv, SUBSTRING(h.timePt, 1, 2) + ':' + SUBSTRING(h.timePt, 3, 4) AS time_serv, m.name AS drug_name, RTRIM(pm.accQty) AS qty, RTRIM(pm.unit) AS unit, LTRIM(RTRIM(ISNULL(CASE WHEN pm.lamedEng = 'N' THEN hx.lamed_name ELSE hx.lamed_name END, ''))) + ' ' + (CASE WHEN LTRIM(RTRIM(ISNULL(pm.lamedQty, ''))) <> '' AND LTRIM(RTRIM(ISNULL(pm.lamedQty, ''))) <> '0' THEN LTRIM(RTRIM(ISNULL(pm.lamedQty, ''))) ELSE LTRIM(RTRIM(ISNULL(pm.qtyPerFeed, ''))) END) + ' ' + LTRIM(RTRIM(u.lamed_name)) + ' ' + (LTRIM(RTRIM(ISNULL(t.lamed_name, ''))) + ' ' + ' ' + (RTRIM(ISNULL(s.lamed_name, '')) + ' ')) AS usage_line1, '' AS usage_line2, '' AS usage_line3 FROM Patmed pm (NOLOCK) LEFT JOIN OPD_H h (NOLOCK) ON (h.hn = pm.hn AND h.regNo = pm.registNo) LEFT JOIN Med_inv m (NOLOCK) ON (pm.invCode = m.code AND m.site = '1') LEFT JOIN Lamed hx (NOLOCK) ON ('*' + pm.lamedHow = hx.lamed_code OR pm.lamedHow = hx.lamed_code) LEFT JOIN Lamed u (NOLOCK) ON ('!' + pm.lamedUnit = u.lamed_code OR pm.lamedUnit = u.lamed_code) LEFT JOIN Lamed t (NOLOCK) ON ('&' + pm.lamedTime = t.lamed_code OR pm.lamedTime = t.lamed_code) LEFT JOIN Lamed s (NOLOCK) ON ('@' + pm.lamedSpecial = s.lamed_code OR pm.lamedSpecial = s.lamed_code) WHERE pm.hn = dbo.padc(?, ' ', 7) AND h.ipdStatus = '0' AND pm.registNo = RIGHT(?, 2) UNION ALL SELECT DISTINCT i.hn + i.regist_flag AS seq, CONVERT(date, CONVERT(char, i.admit_date - 5430000)) AS date_serv, LEFT(i.admit_time, 2) + ':' + RIGHT(i.admit_time, 2) AS time_serv, m.name AS drug_name, pm.accQty AS qty, m.unit AS unit, RTRIM(pm.lamedTimeText) + ' ' + RTRIM(pm.lamedText) AS usage_line1, '' AS usage_line2, '' AS usage_line3 FROM Patmed pm (NOLOCK) LEFT JOIN Ipd_h i (NOLOCK) ON (i.hn = pm.hn AND i.regist_flag = pm.registNo) LEFT JOIN Med_inv m (NOLOCK) ON (pm.invCode = m.code AND m.site = '1') WHERE pm.hn = dbo.padc(?, ' ', 7) AND i.ladmit_n != '' AND i.regist_flag = RIGHT(?, 2) ) AS x ORDER BY x.date_serv DESC, x.time_serv DESC `, [hn, vn, hn, vn]);
    return data;
  }

  async getLabs(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    let data = await db.raw(` 
    select
    convert(date, convert(char, a.res_date - 5430000)) as date_serv
      , left(a.res_time, 2) + ':' + right(a.res_time, 2) as time_serv
      , isnull(a.lab_code, '') as labgroup
      , a.result_name as lab_name
      , replace(replace(a.real_res,'""',''),'!','') as lab_result
      , s.result_unit as unit
      , a.low_normal + '-' + a.high_normal as standard_result
    from Labres_d a(nolock)
    left join OPD_H o(nolock) on(o.hn = a.hn and o.regNo = a.reg_flag)
    left join Labreq_h b(nolock) on(a.req_no = b.req_no)
    left join Labre_s s(nolock) on(a.lab_code = s.lab_code and a.res_item = s.res_run_no and s.labType = a.lab_type)
    LEFT JOIN Lab lb ON(a.lab_code = lb.labCode and lb.labType = a.lab_type)
    left join Ward w on(w.ward_id = b.reqFrom)
    left join PatSS ss on(ss.hn = a.hn)
    where a.hn = dbo.padc('${hn}',' ',7) AND a.real_res<>''
    AND a.reg_flag = right('${vn}', 2)
    order by a.res_date asc ,left(a.res_time, 2) + ':' + right(a.res_time, 2) desc
    `);
    //console.log(data);

    return data;
  }

  /* OK */
  async getLabs_Knex(db: Knex, hn: any, dateServe: any, vn: any, referno: any) {
    
    // let data = await db.select([ db.raw("CONVERT(DATE, CONVERT(CHAR, a.res_date - 5430000)) AS date_serv"), db.raw("LEFT(a.res_time, 2) + ':' + RIGHT(a.res_time, 2) AS time_serv"), db.raw("ISNULL(a.lab_code, '') AS labgroup"), db.raw("a.result_name AS lab_name"), db.raw("REPLACE(REPLACE(a.real_res, '\"\"', ''), '!', '') AS lab_result"), "s.result_unit AS unit", db.raw("a.low_normal + '-' + a.high_normal AS standard_result") ]) .from("Labres_d as a") .leftJoin("OPD_H as o", function() { this.on("o.hn", "=", "a.hn") .andOn("o.regNo", "=", "a.reg_flag"); }) .leftJoin("Labreq_h as b", "a.req_no", "=", "b.req_no") .leftJoin("Labre_s as s", function() { this.on("a.lab_code", "=", "s.lab_code") .andOn("a.res_item", "=", "s.res_run_no") .andOn("s.labType", "=", "a.lab_type"); }) .leftJoin("Lab as lb", function() { this.on("a.lab_code", "=", "lb.labCode") .andOn("lb.labType", "=", "a.lab_type"); }) .leftJoin("Ward as w", "w.ward_id", "=", "b.reqFrom") .leftJoin("PatSS as ss", "ss.hn", "=", "a.hn") .whereRaw("a.hn = dbo.padc(?, ' ', 7)", [`${hn}`]) .andWhere("a.real_res", "<>", "") .andWhereRaw("a.reg_flag = RIGHT(?, 2)", [`${vn}`]) .orderBy([{ column: "a.res_date", order: "asc" }, { column: db.raw("LEFT(a.res_time, 2) + ':' + RIGHT(a.res_time, 2)"), order: "desc" }]);
    //console.log(data);

    // return data;
  }

  async getAppointment_knex(db: Knex, hn: any, dateServe: any, referno: any) {

    let data = await db('Appoint as a') .leftJoin('OPD_H as p', function() { this.on('a.hn', '=', 'p.hn') .andOn('a.regNo', '=', 'p.regNo'); }) .select( db.raw(`ltrim(rtrim(p.hn)) + p.regNo as seq`), 'a.pre_dept_code as clinic', db.raw(`convert(date, convert(char, p.registDate - 5430000)) as date_serv`), db.raw(`SUBSTRING(p.timePt, 1, 2) + ':' + SUBSTRING(p.timePt, 3, 4) as time_serv`), db.raw(`convert(date, convert(char, a.appoint_date - 5430000)) as appoint_date`), 'a.appoint_time_from as appoint_time', 'a.appoint_note as detail' ) .where('p.hn', db.raw(`dbo.padc('${hn}', ' ', 7)`));
    return data;
  }
  async getAppointment(db: Knex, hn: any, dateServe: any, referno: any) {
    let data = await db.raw(`select ltrim(rtrim(p.hn)) + p.regNo as seq, a.pre_dept_code as clinic,
      convert(date, convert(char, p.registDate - 5430000)) as date_serv,
      SUBSTRING(p.timePt, 1, 2) + ':' + SUBSTRING(p.timePt, 3, 4) as time_serv,
      convert(date, convert(char, a.appoint_date - 5430000)) as appoint_date,
      a.appoint_time_from as appoint_time, a.appoint_note as detail
    from Appoint a
    left join OPD_H p on(a.hn = p.hn and a.regNo = p.regNo)
    where p.hn = dbo.padc('${hn}',' ',7)`);
    return data;
  }
  /*ข้อมูลการรักษา >> OK*/
  async getNurture_knex(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
  let data = await db('VitalSign as v') .select( db.raw(`ltrim(rtrim(v.hn)) + v.RegNo as seq`), db.raw(`convert(date, convert(char, v.VisitDate)) as date_serv`), db.raw(`CONVERT(VARCHAR(5),v.VisitDate, 108) AS time_serv`), db.raw(`(select PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn) as bloodgrp`), 'v.Weight as weight', 'v.Height as height', 'v.BMI as bmi', 'v.Temperature as temperature', 'v.Pulse as pr', 'v.Breathe as rr', 'v.Hbloodpress as sbp', 'v.Lbloodpress as dbp', 'v.Symtom as symptom', 'v.deptCode as depcode', db.raw(`(select deptDesc from DEPT d where d.deptCode = v.deptCode) as department`), 'v.GCS_M as movement_score', 'v.GCS_V as vocal_score', 'v.GCS_E as eye_score', 'v.O2sat as oxygen_sat', 'v.pupil_right as pupil_right', 'v.pupil_left as pupil_left', db.raw(`CONVERT(INT, v.GCS_M)+CONVERT(INT, v.GCS_V)+CONVERT(INT, v.GCS_E) as gak_coma_sco`), db.raw(`(select top 1 ltrim(rtrim(dx2.DiagNote)) from PATDIAG dx2 where dx2.Hn = v.hn and dx2.regNo=v.RegNo and dx2.dxtype='1' and dx2.DiagType='I') as diag_text`) ) .leftJoin('PATDIAG as dx', function() { this.on('v.hn', '=', 'dx.Hn') .andOn('v.RegNo', '=', 'dx.regNo'); }) .where('v.hn', db.raw(`dbo.padc(?, ' ', 7)`, [hn])) .andWhere('v.RegNo', db.raw(`right(?, 2)`, [seq])) .orderBy('v.VitalSignNo', 'desc') .limit(1);
    return data;
  }
  async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
   select top 1 ltrim(rtrim(v.hn)) + v.RegNo as seq
    ,convert(date, convert(char, v.VisitDate)) as date_serv
    ,CONVERT(VARCHAR(5),v.VisitDate, 108)  AS time_serv
    ,(select PATIENT.bloodGroup from PATIENT where PATIENT.bloodGroup <> '' and PATIENT.hn = v.hn) as bloodgrp
    ,v.Weight AS weight, v.Height as height
    ,v.BMI as bmi
    ,v.Temperature AS temperature
    ,v.Pulse AS pr
    ,v.Breathe AS rr
    ,v.Hbloodpress as sbp
    ,v.Lbloodpress AS dbp
    ,v.Symtom as symptom
    ,v.deptCode as depcode
    ,(select deptDesc from DEPT d where d.deptCode = v.deptCode) as department
    ,v.GCS_M as movement_score
    ,v.GCS_V as vocal_score
    ,v.GCS_E as eye_score
    ,v.O2sat as oxygen_sat
    ,v.pupil_right as pupil_right
    ,v.pupil_left as pupil_left
    ,CONVERT(INT, v.GCS_M)+CONVERT(INT, v.GCS_V)+CONVERT(INT, v.GCS_E) as gak_coma_sco
    ,(select top 1 ltrim(rtrim(dx2.DiagNote)) from PATDIAG dx2 where dx2.Hn = v.hn and dx2.regNo=v.RegNo and dx2.dxtype='1' and dx2.DiagType='I') as diag_text
    from VitalSign v
    left join PATDIAG dx on(v.hn=dx.Hn and v.RegNo=dx.regNo)
    where v.hn= dbo.padc('${hn}',' ',7) and v.RegNo = right('${seq}', 2)
    order by v.VitalSignNo desc
      `);
    return data;
  }

  /* ข้อมูล PE >> OK */
  async getPhysical_knex(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db('OPD_H') .select(db.raw("ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq"), db.raw("'' as pe")) .innerJoin('PATDIAG', function() { this.on('OPD_H.hn', '=', 'PATDIAG.Hn') .andOn('OPD_H.regNo', '=', 'PATDIAG.regNo'); }) .innerJoin('ICD101', 'PATDIAG.ICDCode', 'ICD101.CODE') .innerJoin('DOCC', 'PATDIAG.DocCode', 'DOCC.docCode') .leftOuterJoin('VitalSign', function() { this.on('OPD_H.hn', '=', 'VitalSign.hn') .andOn('OPD_H.regNo', '=', 'VitalSign.RegNo'); }) .where(db.raw("OPD_H.hn = dbo.padc(?, ' ', 7)", [`${hn}`])) .andWhere(db.raw("PATDIAG.regNo = right(?, 2)", [`${seq}`])) .andWhere('PATDIAG.pt_status', 'O') .orderBy('PATDIAG.regNo', 'desc');
    return data;
  }
  async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
    SELECT ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
    , '' as pe
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }

  /* ข้อมูล HPI >> OK */
  async getPillness_knex(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = db('OPD_H') .select( db.raw('ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq'), db.raw("'' as hpi") ) .innerJoin('PATDIAG', function () { this.on('OPD_H.hn', '=', 'PATDIAG.Hn') .andOn('OPD_H.regNo', '=', 'PATDIAG.regNo'); }) .innerJoin('ICD101', 'PATDIAG.ICDCode', 'ICD101.CODE') .innerJoin('DOCC', 'PATDIAG.DocCode', 'DOCC.docCode') .leftOuterJoin('VitalSign', function () { this.on('OPD_H.hn', '=', 'VitalSign.hn') .andOn('OPD_H.regNo', '=', 'VitalSign.RegNo'); }) .where(db.raw(`OPD_H.hn = dbo.padc(?, ' ', 7)`, [hn])) .andWhere(db.raw(`PATDIAG.regNo = right(?, 2)`, [seq])) .andWhere('PATDIAG.pt_status', 'O') .orderBy('PATDIAG.regNo', 'desc');
    return data;
  }
  async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`SELECT
    ltrim(rtrim(OPD_H.hn)) + PATDIAG.regNo AS seq
      , '' as hpi
    FROM OPD_H
    INNER JOIN PATDIAG ON OPD_H.hn = PATDIAG.Hn and OPD_H.regNo = PATDIAG.regNo
    INNER JOIN ICD101 ON PATDIAG.ICDCode = ICD101.CODE
    INNER JOIN DOCC ON PATDIAG.DocCode = DOCC.docCode
    left outer JOIN VitalSign ON OPD_H.hn = VitalSign.hn and OPD_H.regNo = VitalSign.RegNo
    WHERE OPD_H.hn = dbo.padc('${hn}',' ',7) AND PATDIAG.regNo = right('${seq}', 2) AND (PATDIAG.pt_status = 'O')
    order by PATDIAG.regNo desc

      `);
    return data;
  }

  async getBedsharing_knex(db: Knex) {
    let data = await db('Ward as w') .select([ 'ward_id as ward_code', db.raw(` case when w.ward_id = '121' then 'gen1' when w.ward_id = '125' then 'gen2' when w.ward_id = '123' then 'gen3' when w.ward_id = '124' then 'gen4' when w.ward_id = '131' then 'ศัลย์ทางเดินปัสสาวะ' when w.ward_id = 's11' then 'CVT' when w.ward_id = '122' then 'Trauma' when w.ward_id = '541' then 'Chest' when w.ward_id = 'w55' then 'Neuro หญิง' when w.ward_id = 'w43' then 'Neuro ชาย' when w.ward_id = 'w51' then 'ศัลย์เด็ก' when w.ward_id = 'w53' then 'ศัลย์ตกแต่ง' when w.ward_id = '211' then 'สูติกรรม 1' when w.ward_id = '221' then 'สูติกรรม 2' when w.ward_id = '132' then 'สูติกรรม 3' when w.ward_id = '231' then 'นรีเวชกรรม' when w.ward_id = '241' then 'Ortho ชาย 1' when w.ward_id = '142' then 'Ortho ชาย 2' when w.ward_id = '242' then 'Ortho หญิง' when w.ward_id = '144' then 'Spinal Unit' when w.ward_id = '331' then 'อช.ชั้น 4' when w.ward_id = '341' then 'อช.ชั้น 5' when w.ward_id = '862' then 'อช.ชั้น 6' when w.ward_id = '841' then 'อญชั้น 4' when w.ward_id = '232' then 'อญชั้น 5' when w.ward_id = '321' then 'อญชั้น 6' when w.ward_id = 'chm' then 'เคมีบำบัด' when w.ward_id = '344' then 'อายุรกรรมโรคเลือด' when w.ward_id in ('441','443') then 'เด็ก 1,2' when w.ward_id = '421' then 'เด็ก 3' when w.ward_id = '431' then 'เด็ก 4' when w.ward_id = '332' then 'เด็ก 5' when w.ward_id = '621' then 'จักษุ' when w.ward_id = '342' then 'โสต ศอ นาสิก' when w.ward_id = '641' then 'พิเศษอายุรกรรม7ทิศใต้' when w.ward_id = '651' then 'พิเศษอายุรกรรม7ทิศเหนือ' when w.ward_id = '711' then 'พิเศษพระปทุมฯ1' when w.ward_id = '721' then 'พ.ปทุมฯ2,BMT' when w.ward_id = '731' then 'พิเศษพระปทุมฯ3' when w.ward_id = '741' then 'พิเศษพระปทุมฯ4' when w.ward_id = '751' then 'พิเศษพระปทุมฯ5' when w.ward_id = 'b2r' then 'พิเศษประกันสังคม' when w.ward_id = 'w61' then 'พิเศษศัลยกรรม' when w.ward_id = 's61' then 'พิเศษสงฆ์' when w.ward_id = 'w62' then 'พิเศษวิชิต' when w.ward_id = '112' then 'ห้องคลอด' end as ward_name `), db.raw(` (select count(i.hn) from Ipd_h i where ward_id = w.ward_id and i.discharge_status = '0') as ward_pt `), 'w.ward_tot_bed as ward_bed', 'w.ward_tot_bed as ward_standard' ]) .whereNot('ward_tot_bed', '') .whereNot('ward_tot_bed', '99') .whereNotIn('ward_id', ['b2r', '211', 'obs']) .whereNot('ward_id', 'w91') .whereNot('ward_id', '543') .whereNot('ward_id', 'gp') .whereNot('ward_id', 'p01') .whereNot('ward_id', 'w11') .whereNotIn('ward_id', [ '111','114','115','143','141','113','s41','w41','w45','w47','b2l', '411','222','311','333','323','343','224','s31','s32','322', '423','424','212','214','215' ]);
    return data;
  }
  async getBedsharing(db: Knex) {
    let data = await db.raw(`    
      SELECT ward_id as ward_code,
      case
      when w.ward_id = '121' then 'gen1'
      when w.ward_id = '125' then 'gen2'
      when w.ward_id = '123' then 'gen3'
      when w.ward_id = '124' then 'gen4'
      when w.ward_id = '131' then 'ศัลย์ทางเดินปัสสาวะ'
      when w.ward_id = 's11' then 'CVT'
      when w.ward_id = '122' then 'Trauma'
      when w.ward_id = '541' then 'Chest'
      when w.ward_id = 'w55' then 'Neuro หญิง'
      when w.ward_id = 'w43' then 'Neuro ชาย'
      when w.ward_id = 'w51' then 'ศัลย์เด็ก'
      when w.ward_id = 'w53' then 'ศัลย์ตกแต่ง'
      when w.ward_id = '211' then 'สูติกรรม 1'
      when w.ward_id = '221' then 'สูติกรรม 2'
      when w.ward_id = '132' then 'สูติกรรม 3'
      when w.ward_id = '231' then 'นรีเวชกรรม'
      when w.ward_id = '241' then 'Ortho ชาย 1'
      when w.ward_id = '142' then 'Ortho ชาย 2'
      when w.ward_id = '242' then 'Ortho หญิง'
      when w.ward_id = '144' then 'Spinal Unit'
      when w.ward_id = '331' then 'อช.ชั้น 4'
      when w.ward_id = '341' then 'อช.ชั้น 5'
      when w.ward_id = '862' then 'อช.ชั้น 6'
      when w.ward_id = '841' then 'อญชั้น 4'
      when w.ward_id = '232' then 'อญชั้น 5'
      when w.ward_id = '321' then 'อญชั้น 6'
      when w.ward_id = 'chm' then 'เคมีบำบัด'
      when w.ward_id = '344' then 'อายุรกรรมโรคเลือด'
      when w.ward_id in ('441','443') then 'เด็ก 1,2'
      when w.ward_id = '421' then 'เด็ก 3'
      when w.ward_id = '431' then 'เด็ก 4'
      when w.ward_id = '332' then 'เด็ก 5'
      when w.ward_id = '621' then 'จักษุ'
      when w.ward_id = '342' then 'โสต ศอ นาสิก'
      when w.ward_id = '641' then 'พิเศษอายุรกรรม7ทิศใต้'
      when w.ward_id = '651' then 'พิเศษอายุรกรรม7ทิศเหนือ'
      when w.ward_id = '711' then 'พิเศษพระปทุมฯ1'
      when w.ward_id = '721' then 'พ.ปทุมฯ2,BMT'
      when w.ward_id = '731' then 'พิเศษพระปทุมฯ3'
      when w.ward_id = '741' then 'พิเศษพระปทุมฯ4'
      when w.ward_id = '751' then 'พิเศษพระปทุมฯ5'
      when w.ward_id = 'b2r' then 'พิเศษประกันสังคม'
      when w.ward_id = 'w61' then 'พิเศษศัลยกรรม'
      when w.ward_id = 's61' then 'พิเศษสงฆ์'
      when w.ward_id = 'w62' then 'พิเศษวิชิต'
      when w.ward_id = '112' then 'ห้องคลอด'
      end as ward_name,
      (select count(i.hn) from Ipd_h i where ward_id=w.ward_id and i.discharge_status='0') as ward_pt,
      w.ward_tot_bed as ward_bed,
      w.ward_tot_bed as ward_standard
      from Ward w where (ward_tot_bed<>'' and ward_tot_bed<>'99') and (ward_id<>'b2r' and ward_id<>'211' and ward_id<>'obs')
      and w.ward_id <> 'w91'
      and w.ward_id <> '543'
      and w.ward_id <> 'gp'
      and w.ward_id <> 'p01'
      and w.ward_id <> 'w11'
      and w.ward_id not in ('111','114','115','143','141','113','s41','w41','w45','w47','b2l','411','222','311','333','323','343','224','s31','s32','322','423','424','212','214','215')
    `);
    return data;
  }
  /* OK */
  async getReferOut_knex(db: Knex, start_date: any, end_date: any) {
    let data = await db('Refer as r') .select( db.raw(`ltrim(rtrim(r.Hn)) + r.RegNo as seq`), 'r.Hn as hn', db.raw(`isnull(i.ladmit_n, '') as an`), db.raw(`rtrim(t.titleName) as title_name`), db.raw(`rtrim(p.firstName) as first_name`), db.raw(`rtrim(p.lastName) as last_name`), 'r.ReferRunno as referno', db.raw(`convert(date, convert(char, r.ReferDate - 5430000)) as referdate`), 'r.ReferHCODE as to_hcode', 'h.NAME as to_hcode_name', db.raw(`isnull(rs.REASON_DESC, '') as refer_cause`), db.raw(`FORMAT(getdate(), N'hh:mm') as refertime`), db.raw(`'' as doctor`), db.raw(`'' as doctor_name`) ) .leftJoin('PATIENT as p', 'r.Hn', 'p.hn') .leftJoin('PTITLE as t', 'p.titleCode', 't.titleCode') .leftJoin('HOSPCODE as h', 'r.ReferHCODE', 'h.OFF_ID') .leftJoin('REFERRS as rs', 'r.ReferReason', 'rs.REASON_CODE') .leftJoin('Ipd_h as i', function () { this.on('r.Hn', '=', 'i.hn').andOn('r.RegNo', '=', 'i.regist_flag'); }) .whereNot('r.ReferStatus', 'I') .whereRaw(`convert(char, r.ReferDate - 5430000) between replace(?, '-', '') and replace(?, '-', '')`, [start_date, end_date]) .andWhere('r.ReferHCODE', '<>', '10669');
    return data;
  }
  async getReferOut(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    select ltrim(rtrim(r.Hn)) + r.RegNo as seq
      , r.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , rtrim(t.titleName) as title_name
      , rtrim(p.firstName) as first_name
      , rtrim(p.lastName) as last_name
      , r.ReferRunno as referno
      , convert(date, convert(char, r.ReferDate - 5430000)) as referdate
      , r.ReferHCODE as to_hcode
      , h.NAME as to_hcode_name
      , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'hh:mm') as refertime
      , '' as doctor
      , '' as doctor_name
    from Refer r
    left join PATIENT p on r.Hn = p.hn
    LEFT JOIN PTITLE t ON p.titleCode = t.titleCode
    left join HOSPCODE h on r.ReferHCODE = h.OFF_ID
    left join REFERRS rs on r.ReferReason = rs.REASON_CODE
    left join Ipd_h i on r.Hn = i.hn and r.RegNo = i.regist_flag
    where r.ReferStatus <> 'I' and convert(char, r.ReferDate - 5430000)  between replace('${start_date}', '-', '') 
    and replace('${end_date}', '-', '') and r.ReferHCODE<>'10669'`);
    return data;
  }

  /* OK */
  async getReferBack_knex(db: Knex, start_date: any, end_date: any) {
    let data = await db .select([ db.raw("ltrim(rtrim(Refer.Hn)) + Refer.RegNo as seq"), 'Refer.Hn as hn', db.raw("isnull(i.ladmit_n, '') as an"), db.raw("RTRIM(PTITLE.titleName) as title_name"), db.raw("rtrim(PATIENT.firstName) as first_name"), db.raw("rtrim(PATIENT.lastName) as last_name"), 'Refer.ReferRunno as referno', db.raw("convert(date, convert(char, Refer.ReferDate - 5430000)) as referdate"), 'Refer.ReferHCODE as to_hcode', 'HOSPCODE.NAME as to_hcode_name', db.raw("isnull(rs.REASON_DESC, '') as refer_cause"), db.raw("FORMAT(getdate(), N'hh:mm') as refertime"), db.raw("'' AS doctor"), db.raw("'' as doctor_name") ]) .from('Refer') .innerJoin('PATIENT', 'Refer.Hn', 'PATIENT.hn') .innerJoin('PTITLE', 'PATIENT.titleCode', 'PTITLE.titleCode') .leftJoin('HOSPCODE', 'Refer.ReferHCODE', 'HOSPCODE.OFF_ID') .leftJoin('REFERRS as rs', 'Refer.ReferReason', 'rs.REASON_CODE') .leftJoin('Ipd_h as i', function() { this.on('Refer.Hn', '=', 'i.hn') .andOn('Refer.RegNo', '=', 'i.regist_flag'); }) .whereRaw("convert(char, Refer.ReferDate - 5430000) BETWEEN replace(?, '-', '') and replace(?, '-', '')", [start_date, end_date]) .andWhere('Refer.ReferReason', 'in', [6, 7, 8, 9, 10]);
    return data;
  }
  async getReferBack(db: Knex, start_date: any, end_date: any) {
    let data = await db.raw(`
    SELECT
    ltrim(rtrim(Refer.Hn)) + Refer.RegNo as seq
      , Refer.Hn as hn
      , isnull(i.ladmit_n, '') as an
      , RTRIM(PTITLE.titleName) as title_name
      , rtrim(PATIENT.firstName) as first_name
      , rtrim(PATIENT.lastName) as last_name
      , Refer.ReferRunno AS referno
        , convert(date, convert(char, Refer.ReferDate - 5430000)) as referdate
        , Refer.ReferHCODE AS to_hcode
          , HOSPCODE.NAME as to_hcode_name
          , isnull(rs.REASON_DESC, '') as refer_cause
      , FORMAT(getdate(), N'hh:mm') as refertime
          , '' AS doctor
            , '' as doctor_name
    FROM Refer with (nolock)
    INNER JOIN PATIENT with (nolock) ON Refer.Hn = PATIENT.hn
    INNER JOIN PTITLE with (nolock) ON PATIENT.titleCode = PTITLE.titleCode
    LEFT JOIN HOSPCODE with (nolock) ON Refer.ReferHCODE = HOSPCODE.OFF_ID
    left join REFERRS rs on Refer.ReferReason = rs.REASON_CODE
    left join Ipd_h i on Refer.Hn = i.hn and Refer.RegNo = i.regist_flag
    WHERE   convert(char, Refer.ReferDate - 5430000)  BETWEEN replace('${start_date}', '-', '') and replace('${end_date}', '-', '')
    AND Refer.ReferReason in (6,7,8,9,10) `);
    return data;
  }

  async getAppoint_knex(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db('Appoint as a') .select( 'a.hn as seq', db.raw('CONVERT(date, CONVERT(char, a.appoint_date - 5430000)) as receive_appoint_date'), 'a.appoint_time_from as receive_appoing_begintime', 'a.appoint_time_to as receive_appoing_endtime', db.raw('dbo.TitleFullName(d.docName, d.docLName) as receive_appoing_doctor'), db.raw('LTRIM(RTRIM(deptDesc)) as receive_appoing_chinic'), 'appoint_note as receive_text', db.raw("(RTRIM(pr.firstName) + ' ' + RTRIM(pr.lastName)) as receive_by"), 'a.remark as receive_special_note', db.raw("CONCAT(LTRIM(RTRIM(deptDesc)), '(', dep.deptCode, ')') as receive_checkpoint"), db.raw("CONCAT('045-319200', ' ต่อ ', teldept) as receive_teldept") ) .leftJoin('PATIENT as p', 'p.hn', 'a.hn') .leftJoin('PTITLE as pti', 'pti.titleCode', 'p.titleCode') .leftJoin('DOCC as d', 'd.docCode', 'a.doctor') .leftJoin('DEPT as dep', 'dep.deptCode', 'a.pre_dept_code') .leftJoin('profile as pr', 'pr.UserCode', 'a.maker') .where('a.app_type', 'A') .andWhere(db.raw('CONVERT(char, a.appoint_date - 5430000)'), '=', db.raw(`REPLACE(?, '-', '')`, [app_date])) .andWhere('a.hn', hn) .orderBy('a.keyin_time', 'desc');
    return data;
  }
  async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
    let data = await db.raw(`
    SELECT a.hn as seq
    ,convert(date,convert(char,a.appoint_date -5430000)) as receive_appoint_date
    ,a.appoint_time_from as receive_appoing_begintime
    ,a.appoint_time_to as receive_appoing_endtime
    ,dbo.TitleFullName(d.docName,d.docLName) as receive_appoing_doctor
    ,ltrim(rtrim(deptDesc)) as receive_appoing_chinic
    ,appoint_note as receive_text
    ,(RTRIM(pr.firstName)+' '+RTRIM(pr.lastName)) as receive_by 
    ,a.remark as receive_special_note
    ,concat(ltrim(rtrim(deptDesc)),'(',dep.deptCode,')') as receive_checkpoint
    ,concat('045-319200',' ต่อ ',teldept) as receive_teldept
    FROM Appoint a left join PATIENT p on p.hn=a.hn
    left join PTITLE pti on pti.titleCode=p.titleCode
    left join DOCC d on d.docCode=a.doctor
    left join DEPT dep on dep.deptCode=a.pre_dept_code
    left join profile pr on pr.UserCode=a.maker
    WHERE
    (a.app_type = 'A')
    and convert(char, a.appoint_date - 5430000)  = replace('${app_date}', '-', '')
    and a.hn='${hn}' order by a.keyin_time desc
        `);
    return data;
  }

  async getXray_knex(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db('XresHis as x') .select( db.raw("convert(date, convert(char, x.xreq_date - 5430000)) as xray_date"), 'c.xproc_des as xray_name' ) .leftJoin('OPD_H as o', function () { this.on('o.hn', '=', 'x.hn') .andOn('o.regNo', '=', 'x.regist_flag'); }) .leftJoin('Xreq_h as xh', function () { this.on('xh.hn', '=', 'x.hn') .andOn('xh.regist_flag', '=', 'x.regist_flag') .andOn('x.xrunno', '=', 'xh.xrunno'); }) .leftJoin('Xpart as p', 'x.xpart_code', 'p.xpart_code') .leftJoin('Xreq_d as d', function () { this.on('x.xprefix', '=', 'd.xprefix') .andOn('x.xrunno', '=', 'd.xrunno'); }) .leftJoin('Xcon as xc', 'x.xprefix', 'xc.xprefix') .leftJoin('Xproc as c', function () { this.on('d.xpart_code', '=', 'c.xproc_part') .andOn('d.xproc_code', '=', 'c.xproc_code'); }) .whereNot('xc.xchr_code', 'PAT') .andWhere('x.hn', db.raw(`dbo.padc('${hn}',' ',7)`)) .andWhere(db.raw(`x.regist_flag = right('${seq}', 2)`)) .orderBy('x.xreq_date', 'desc');
    return data;
  }
  async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
    let data = await db.raw(`
          select convert(date, convert(char, x.xreq_date - 5430000)) as xray_date,c.xproc_des as xray_name
          from XresHis x 
          left join OPD_H o (NOLOCK) on (o.hn = x.hn and o.regNo = x.regist_flag) 
          LEFT JOIN Xreq_h xh on(xh.hn = x.hn and xh.regist_flag = x.regist_flag and x.xrunno = xh.xrunno) 
          LEFT JOIN Xpart p ON(x.xpart_code = p.xpart_code) 
          LEFT JOIN Xreq_d d ON(x.xprefix = d.xprefix AND x.xrunno = d.xrunno) 
          LEFT JOIN Xcon xc on(x.xprefix = xc.xprefix) 
          LEFT JOIN Xproc c ON(d.xpart_code = c.xproc_part  AND d.xproc_code = c.xproc_code) 
          WHERE xc.xchr_code <> 'PAT' 
          AND x.hn = dbo.padc('${hn}',' ',7)
          AND x.regist_flag = right('${seq}', 2)
          order by  x.xreq_date DESC
         `);
    return data;
  }

  async getDepartment_knex(db: Knex) {
    let data = await db.select( db.raw('LTRIM(RTRIM(ward_id)) AS dep_code'), db.raw('LTRIM(RTRIM(ward_name)) AS dep_name') ) .from('Ward') .orderBy('ward_name', 'asc');
    return data
  }
  async getDepartment(db: Knex) {
    let data = await db.raw(`
      SELECT ltrim(rtrim(ward_id)) as dep_code,ltrim(rtrim(ward_name)) as dep_name from Ward order by ward_name asc
    `);
    return data
  }

  async getPtHN_knex(db: Knex, cid: any) {
    let data = await db('Ward') .select( db.raw('ltrim(rtrim(ward_id)) as dep_code'), db.raw('ltrim(rtrim(ward_name)) as dep_name') ) .orderBy('ward_name', 'asc');
    return data[0];
  }
  async getPtHN(db: Knex, cid: any) {
    let data = await db.raw(`
      select hn from PatSS with(nolock) where CardID = '${cid}'
    `);
    return data[0];
  }

  async getMedrecconcile(db: Knex, hn: any) {
    let data = await db.raw(`
        select '' as drug_hospcode ,'' as drug_hospname,'' as drug_name,'' as drug_use,'' as drug_receive_date
        `);
    return data;
  }

    /* OK */
    async getServicesCoc_knex(db: Knex, hn: any) {
      let data = db('OPD_H as o') .select([ 'o.hn', db.raw("LTRIM(RTRIM(o.hn)) + o.regNo AS seq"), db.raw("CONVERT(DATE, CONVERT(CHAR, o.registDate - 5430000)) AS date_serv"), db.raw("LEFT(o.timePt, 2) + ':' + RIGHT(o.timePt, 2) AS time_serv"), 'd.deptDesc AS department' ]) .leftJoin('DEPT as d', 'd.deptCode', 'o.dept') .leftJoin('Refer as r', 'o.hn', 'r.Hn') .where(db.raw("o.hn = dbo.padc(?, ' ', 7)", [hn])) .groupBy(['o.hn', 'o.regNo', 'o.registDate', 'o.timePt', 'd.deptDesc']) .orderBy('seq', 'desc') .limit(3);
      return data;
    }

    async getServicesCoc(db: Knex, hn: any) {
      let data = await db.raw(`
      SELECT top 3 o.hn, ltrim(rtrim(o.hn)) + o.regNo as seq, convert(date,convert(char,o.registDate -5430000)) as date_serv
      ,left(o.timePt,2)+':'+right (o.timePt,2) as time_serv, d.deptDesc as department 
      from OPD_H as o 
      left join DEPT d on(d.deptCode = o.dept) 
      left join Refer r on o.hn=r.Hn 
      where o.hn = dbo.padc('${hn}',' ',7)
      GROUP BY o.hn,o.regNo,o.registDate,o.timePt,d.deptDesc
      order by seq desc
      `);
      return data;
    }

    async getProfileCoc_knex(db: Knex, hn: any) {
      let data = await db('PATIENT as p') .select( db.raw("LTRIM(RTRIM(p.hn)) as hn"), db.raw("RTRIM(t.titleName) as title_name"), db.raw("RTRIM(p.firstName) as first_name"), db.raw("RTRIM(p.lastName) as last_name"), db.raw("RTRIM(ps.CardID) AS cid"), db.raw("p.moo as moopart"), db.raw("(RTRIM(LTRIM(p.addr1)) + ' ' + RTRIM(LTRIM(p.addr2))) as addrpart"), db.raw("p.tambonCode as tmbpart"), db.raw("SUBSTRING(p.regionCode, 3, 2) as amppart"), db.raw("p.areaCode as chwpart"), db.raw(` CASE WHEN RIGHT(p.birthDay, 4) = '0000' THEN RIGHT('000' + LTRIM(STR(year(getdate()) + 543 - LEFT(p.birthDay, 4))), 3) + '-00-00' ELSE RIGHT('000' + LTRIM(STR(year(getdate()) + 543 - year(p.birthDay))), 3) + '-' + RIGHT('00' + LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate())) % 12)), 2) + '-' + RIGHT('00' + LTRIM(STR(REPLACE(DATEDIFF(DAY, DAY(p.birthDay), DAY(GETDATE())), '-', ''))), 2) END as age`), db.raw("i.useDrg AS pttype_id"), db.raw("RTRIM(pt.pay_typedes) as pttype_name"), db.raw("RTRIM(LTRIM(ps.SocialID)) as pttype_no"), db.raw("i.HMAIN AS hospmain"), db.raw("h1.NAME as hospmain_name"), db.raw("i.HSUB AS hospsub"), db.raw("h2.NAME as hospsub_name"), db.raw("convert(date, convert(char, o.registDate - 5430000)) as registDate"), db.raw("convert(date, convert(char, p.birthDay - 5430000)) as brthdate"), db.raw("(select TOP 1 convert(date, convert(char, VisitDate - 5430000)) from PATDIAG where Hn = p.hn group by Hn, VisitDate order by VisitDate desc) as VisitDate"), db.raw("ps.father as father_name"), db.raw("p.mother as mother_name"), db.raw("'' as couple_name"), db.raw("ps.relatives as contact_name"), db.raw("ps.relationship as contact_relation"), db.raw("'' as contact_mobile"), db.raw("CASE WHEN p.sex = 'ช' THEN 'ชาย' ELSE 'หญิง' END as sex"), db.raw("(select occdes from Occup where p.occupation = occcode) as occupation") ) .leftOuterJoin('PTITLE as t', 'p.titleCode', 't.titleCode') .leftOuterJoin('PatSS as ps', 'p.hn', 'ps.hn') .leftOuterJoin('OPD_H as o', 'p.hn', 'o.hn') .leftOuterJoin('Bill_h as i', function() { this.on('o.hn', 'i.hn').andOn('o.regNo', 'i.regNo'); }) .leftOuterJoin('Paytype as pt', 'i.useDrg', 'pt.pay_typecode') .leftOuterJoin('DEPT as d', 'o.dept', 'd.deptCode') .leftOuterJoin('HOSPCODE as h1', 'i.HMAIN', 'h1.OFF_ID') .leftOuterJoin('HOSPCODE as h2', 'i.HSUB', 'h2.OFF_ID') .where('p.hn', db.raw(`dbo.padc('${hn}', ' ', 7)`)) .orderBy('o.registDate', 'desc') .first();
      return data[0];
    }
    async getProfileCoc(db: Knex, hn: any) {
      let data = await db.raw(`
      SELECT TOP 1 ltrim(rtrim(p.hn)),rtrim(t.titleName) as title_name, rtrim(p.firstName) as first_name, rtrim(p.lastName) as last_name, rtrim(ps.CardID) AS cid
      , p.moo as moopart, (RTRIM(LTRIM(p.addr1)) + '  ' + RTRIM(LTRIM(p.addr2))) as addrpart, p.tambonCode as tmbpart
      , SUBSTRING(p.regionCode, 3, 2) as amppart, p.areaCode as chwpart
      ,(case when right(p.birthDay,4) = '0000' then RIGHT('000'+LTRIM(STR(year(getdate())+543 - left(p.birthDay,4))),3)+'-00-00'
      else  RIGHT('000'+LTRIM(STR(year(getdate())+543 - year(p.birthDay))),3) + '-'+ 
      RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthDay, getdate()))%12)),2)+'-'+ 
      RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthDay),DAY(GETDATE())),'-',''))),2) end) as age 
      , i.useDrg AS pttype_id,rtrim(pt.pay_typedes) as  pttype_name, RTRIM(LTRIM(ps.SocialID)) as pttype_no
      , i.HMAIN AS hospmain, h1.NAME as hospmain_name, i.HSUB AS hospsub, h2.NAME as hospsub_name
      ,convert(date,convert(char,o.registDate -5430000))  as registDate
      ,convert(date,convert(char,p.birthDay -5430000)) as brthdate
      ,(select Top 1 convert(date,convert(char,VisitDate -5430000))  from PATDIAG with(nolock) where Hn=p.hn group by Hn,VisitDate order by VisitDate desc) as VisitDate 
       ,ps.father as father_name 
      ,p.mother as mother_name
      ,'' as couple_name
      ,ps.relatives as contact_name
      ,ps.relationship as contact_relation
      ,'' as contact_mobile
      ,(case when p.sex='ช' then 'ชาย' else 'หญิง' end) as sex
      ,(select occdes from Occup where p.occupation = occcode) as occupation
      FROM PATIENT p
      LEFT OUTER JOIN PTITLE t ON p.titleCode=t.titleCode
      LEFT OUTER JOIN PatSS ps ON p.hn = ps.hn
      LEFT OUTER JOIN OPD_H o ON p.hn = o.hn
      LEFT OUTER JOIN Bill_h i ON o.hn = i.hn AND o.regNo = i.regNo
      LEFT OUTER JOIN Paytype pt ON i.useDrg = pt.pay_typecode 
      LEFT OUTER JOIN DEPT d ON o.dept = d.deptCode
      LEFT OUTER JOIN HOSPCODE h1 ON i.HMAIN = h1.OFF_ID
      LEFT OUTER JOIN HOSPCODE h2 ON i.HSUB = h2.OFF_ID
      WHERE p.hn=dbo.padc('${hn}',' ',7)
      ORDER BY o.registDate DESC
      `);
      return data[0];
    }
}
