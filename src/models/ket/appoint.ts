import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_API_URL;

export class AppointModel {

    async list(token: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint/list`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async select_id(token: any, appoint_id: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint/select_id?id=${appoint_id}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async select_appointByhcode(token: any, hcode: any, clinic_code: any, appoint_date: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'GET',
                url: `${urlApi}/appoint/select_appointByhcode?hcode=${hcode}&clinic_code=${clinic_code}&appoint_date=${appoint_date}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async insert(token: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'POST',
                url: `${urlApi}/appoint/insert`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async update(token: any, appoint_id: any, rows: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'PUT',
                url: `${urlApi}/appoint/update?id=${appoint_id}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                body: { rows: rows },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }

    async delete(token: any, appoint_id: any) {
        return await new Promise((resolve: any, reject: any) => {
            var options = {
                method: 'DELETE',
                url: `${urlApi}/appoint/delete?id=${appoint_id}`,
                agentOptions: {
                    rejectUnauthorized: false
                },
                headers:
                {
                    'cache-control': 'no-cache',
                    // 'content-type': 'application/json',
                    'authorization': `Bearer ${token}`,
                },
                json: true
            };

            request(options, function (error: any, response: any, body: any) {
                if (error) {
                    reject(error);
                } else {
                    resolve(body);
                }
            });
        });
    }
}