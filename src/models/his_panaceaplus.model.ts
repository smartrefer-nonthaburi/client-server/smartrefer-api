import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisPanaceaplusModel {

    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT username , fullname, hcode
        from smartrefer_user WHERE username = '${username}' and password = '${password}'
        `);
        return data;
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT 
        seq, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE hn = '${hn}' and seq = '${seq}'`);
        return data;
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select hn, cid, title_name,first_name, last_name,
        moopart, addrpart, tmbpart, amppart, chwpart, brthdate
        , age, pttype_id, pttype_name, pttype_no, hospmain
        , hospmain_name, hospsub, hospsub_name, registdate, visitdate
        , father_name, mother_name, couple_name, contact_name,contact_relation, contact_mobile
        FROM smartrefer_person
        WHERE hn ='${hn}'`);
        return data;
    }

    async getHospital(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT provider_code, provider_name 
        FROM smartrefer_hospital`);
        return data;
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT drug_name, symptom ,begin_date,daterecord
        FROM smartrefer_allergy 
        WHERE hn ='${hn}'`);
        return data;
    }

    async getChronic(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT icd_code, start_date, icd_name
        FROM smartrefer_chronic 
        WHERE hn ='${hn}'`);
        return data;
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT seq, date_serv, time_serv
        , icd_code, cd_name
        , diag_type, DiagNote, diagtype_id
        FROM smartrefer_diagnosis  
        WHERE seq ='${seq}'`);
        return data;
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select seq, an, pid, hn, referno, referdate, to_hcode, 
        pttype_id, pttype_name, strength_id,strength_name,location_name,  station_name, loads_id, loads_name,
        to_hcode_name, refer_cause, refertime,
	    doctor, doctor_name,refer_remark
        from smartrefer_refer
        where seq ='${seq}' and referno = '${referno}'
        `);
        return data;
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select seq, date_serv, time_serv, drug_name,
        qty, unit, usage_line1, usage_line2, usage_line3
        FROM smartrefer_drugs
        WHERE seq = '${seq}'`);
        return data;
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT date_serv, time_serv, labgroup, lab_name, lab_result, unit, standard_result
        FROM smartrefer_lab where seq ='${seq}'`);
        return data;
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        select seq,date_serv,date,time,department,detail,time_serv 
	from smartrefer_appointmen
	where seq = '${seq}'
        `);
        return data;
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        select date_serv, time_serv, vaccine_code, vaccine_name
        from smartrefer_vaccines
        where hn='${hn}'`);
        return data;
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT pid, seq, date_serv,	time_serv, procedure_code,	
        procedure_name, start_date, start_time, end_date, end_time
        from smartrefer_procedure
        where seq = '${seq}'
        `);
        return data;
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select seq,date_serv,time_serv,bloodgrp,weight,height,bmi,temperature,pr,rr,sbp,dbp,symptom,depcode,department,movement_score,vocal_score,eye_score,oxygen_sat,gak_coma_sco,diag_text,pupil_right,pupil_left
        FROM smartrefer_nurtures
        WHERE seq = '${seq}'
        `);
        return data;
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT seq , pe FROM smartrefer_physical WHERE seq = '${seq}'
        `);
        return data;
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT seq , hpi FROM smartrefer_pillness WHERE seq = '${seq}'
        `);
        return data;
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT xray_date ,xray_name 
        FROM smartrefer_xray 
        WHERE seq = '${seq}'
        `);
        return data;
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        select ward_code, ward_name, ward_pt, ward_bed, ward_standard 
        from smartrefer_bedsharing 
        `);
        return data;
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select r.seq, r.hn, r.an,p.title_name, p.first_name, p.last_name, r.referno, r.referdate,r.location_name, r.to_hcode, r.pttype_id,
        r.pttype_name, r.strength_id, r.to_hcode_name, r.refer_cause, r.refertime, r.doctor, r.doctor_name
        from smartrefer_refer_out r
	    INNER JOIN smartrefer_person p on p.hn = r.hn
        where r.ReferDate between '${start_date}' and '${end_date}'`);
	return data;
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select r.seq, r.hn, r.an,p.title_name, p.first_name, p.last_name, r.referno, referdate,
        r.to_hcode, r.to_hcode_name, r.refer_cause, r.refertime, r.doctor, r.doctor_name
        from smartrefer_refer_back r
	    INNER JOIN smartrefer_person p on p.hn = r.hn
        where ReferDate between '${start_date}' and '${end_date}'`);
        return data;
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        SELECT seq, receive_apppoint_date, receive_apppoint_beginttime,
        receive_apppoint_endtime, receive_apppoint_doctor, receive_apppoint_chinic, receive_text,
        receive_by
        from smartrefer_appoint
        WHERE hn ='${hn}' and receive_apppoint_date = '${app_date}'
        `);
        return data;
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`
            SELECT dep_code, dep_name from smartrefer_department 
        `);
        return data;

    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        select hn from smartrefer_person where cid = '${cid}'
        `);
        return data;
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        select hn, drug_hospcode ,drug_hospname,drug_name,drug_use,drug_receive_date from smartrefer_medreconcile where hn = '${hn}'
        `);
        return data;
    }

    async getServicesCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT hn,
        seq, title_name, first_name, last_name, 
		date_serv, time_serv, department
		from smartrefer_visit
        WHERE hn = '${hn}'
        ORDER BY seq DESC limit 3
        `);
        return data;
    }

    async getProfileCoc(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select hn, cid, title_name,first_name, last_name,
        moopart, addrpart, tmbpart, amppart, chwpart, brthdate
        , age, pttype_id, pttype_name, pttype_no, hospmain
        , hospmain_name, hospsub, hospsub_name, registdate, visitdate
        , father_name, mother_name, couple_name, contact_name,contact_relation, contact_mobile
        FROM smartrefer_person
        WHERE hn ='${hn}'`);
        return data;
    }

}
