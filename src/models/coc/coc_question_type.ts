import * as Knex from 'knex';
const request = require("request");
const urlApi = process.env.SERV_COC_API_URL;

export class CocQuestionTypeModel {

  async select(token:any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'GET',
        url: `${urlApi}/coc_question_type/select`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async insert(token:any, rows: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'POST',
        url: `${urlApi}/coc_question_type/insert`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        body: { rows: rows },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async update(token:any, id: any, rows: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'PUT',
        url: `${urlApi}/coc_question_type/update?question_type_id=${id}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        body: { rows: rows },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }

  async delete(token:any, id: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'DELETE',
        url: `${urlApi}/coc_question_type/delete?question_type_id=${id}`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          // 'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        json: true
      };

      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }
  async selectOne(token:any, rows: any) {
    return await new Promise((resolve: any, reject: any) => {
      var options = {
        method: 'POST',
        url: `${urlApi}/coc_question_type/selectOne`,
        agentOptions: {
          rejectUnauthorized: false
        },
        headers:
        {
          'cache-control': 'no-cache',
          'content-type': 'application/json',
          'authorization': `Bearer ${token}`,
        },
        body: { rows: rows },
        json: true
      };
      request(options, function (error:any, response:any, body:any) {
        if (error) {
          reject(error);
        } else {
          resolve(body);
        }
      });
    });
  }


}