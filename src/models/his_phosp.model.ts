import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisPhospModel {

    async getLogin(db: Knex, username: any, password: any) {
        let data = await db.raw(`
        SELECT dct.dct as username , CONCAT(dct.fname, ' ', dct.lname) as fullname, (select hcode from setup limit 1) as hcode
        from dct WHERE dct.dct = '${username}' 
        and SUBSTRING(dct.cid, 10, 13) = '${password}'
        `);
        return data[0];
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT 
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        INNER JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.vn = '${seq}' and o.ovstost = '3'
		UNION
		SELECT 
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}' and ipt.vn = '${seq}' and ipt.dchtype = '4'`);
        return data[0];
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select p.hn as hn, p.pop_id as cid, p.pname as title_name,p.fname as first_name,p.lname as last_name,
        p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(day(p.brthdate)>day(now()),dayofmonth(now())-(day(p.brthdate)-day(now())),day(now())-day(p.brthdate)),2,'0')) as age
        ,p.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        FROM pt as p 
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'`);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT s.hcode as provider_code,h.namehosp as provider_name 
        FROM setup as s 
        INNER JOIN hospcode as h on h.off_id = s.hcode`);
        return data[0];
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT namedrug as drug_name, detail as symptom ,entrydate as begin_date,entrydate as daterecord
        FROM allergy 
        WHERE hn ='${hn}'`);
        return data[0];
    }

    async getChronic(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT c.chronic as icd_code, c.date_diag as start_date, IF(i.name_t!='', i.name_t, "-") as icd_name
        FROM chronic as c 
        INNER JOIN icd101 as i on i.icd10 = c.chronic
        WHERE c.pid ='${hn}'`);
        return data[0];
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, date_format(ovst.vstdttm,'%Y-%m-%d') as date_serv, time(ovst.vstdttm) as time_serv
        , o.icd10 as icd_code,IF(o.icd10name!='', o.icd10name, i.icd10name) as icd_name
        , o.cnt as diag_type, d.diagtext as DiagNote, 
        (case when cnt = 1 and o.icd10 not between 'V0000' and 'Y9999' then 1
        when cnt = 0 and o.icd10 not between 'V0000' and 'Y9999' then 4
        when o.icd10 between 'V0000' and 'Y9999' then 5 else 4 end)  as diagtype_id
        FROM ovstdx as o
        INNER JOIN ovst on ovst.vn = o.vn
        INNER JOIN icd101 as i on i.icd10 = o.icd10
        left JOIN visitdiagtext as d on o.vn = d.vn 
        WHERE o.vn ='${seq}'`);
        return data[0];
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select o.vn as seq,o.an as an,ovst.hn as pid, ovst.hn as hn, 
		o.rfrno as referno,
        o.vstdate as referdate, 
        o.rfrlct as to_hcode, 
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        t.triage as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else '' end 
            ) as strength_name,
		if(o.cln='' or o.cln is null,i.nameidpm,c.namecln) as location_name,
		if(o.cln='' or o.cln is null,'IPD',if(substr(o.cln,1,1)=2,'ER','OPD')) as station_name,
        h.namehosp as to_hcode_name, 
        f.namerfrcs as refer_cause,
	    time(o.vsttime*100) as refertime,
	    d.lcno as doctor,
	    CONCAT(d.fname,' ',d.lname) as doctor_name
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        left Join rfrcs as f on f.rfrcs = o.rfrcs
		left join cln as c on o.cln=c.cln 
		left join idpm as i on o.ward=i.idpm
        left Join dct as d on d.lcno=ovst.dct
        left join optriage as t on o.vn=t.vn
        where o.vn  ='${seq}' and o.rfrno = 'referno'
        `);
        return data[0];
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select p.vn as seq,
        DATE_FORMAT(date(p.prscdate),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(p.prsctime),'%h:%i:%s') as time_serv, 
        pd.nameprscdt as drug_name,
        sum(pd.qty) as qty, 
        med.pres_unt as unit ,
        if(pd.medusage = '',x.doseprn,IF(m.doseprn1!='', m.doseprn1, 'no list')) as usage_line1 ,
        IF(m.doseprn2!='', m.doseprn2, 'no list') as usage_line2,
        '' as usage_line3
        FROM prsc as p 
        Inner Join prscdt as pd ON pd.PRSCNO = p.PRSCNO 
        Left Join medusage as m ON m.dosecode = pd.medusage
        left join xdose as x ON pd.xdoseno=x.xdoseno
        Left Join meditem as med ON med.meditem = pd.meditem  
        WHERE p.vn = '${seq}' and med.type = 1 GROUP BY med.meditem`);
        return data[0];
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT
        date_serv,time_serv,labname as labgroup,lab_test as lab_name,
        hi.Get_Labresult(t.lab_table,t.labfield,t.lab_number) as lab_result,unit,
        reference as standard_result
        FROM
        (SELECT DISTINCT
        l.ln as lab_number,
        l.vn as seq,
        l.hn as hn,
        lb.labname,
        DATE_FORMAT(date(l.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(l.vstdttm),'%h:%i:%s') as time_serv,
        lb.fieldname as lab_code_local,
        lb.unit,
        replace(lb.fieldlabel,"'",'\`') as lab_test, lb.filename as lab_table,
        lb.fieldname as labfield,
        concat(lb.normal,' ',lb.unit) as reference,
        replace(lab.labname,"'",'\`') as lab_group_name,
        l.labcode as lab_group
        FROM
        hi.lbbk as l 
        inner join hi.lab on l.labcode=lab.labcode and l.finish=1 and l.vn='${seq}'
        inner join hi.lablabel as lb on l.labcode = lb.labcode
        group by l.ln,l.labcode,lb.filename,lb.fieldname
        ) as t `);
        return data[0];
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT o.vn as seq, o.vstdate as date_serv, o.fudate as date, o.futime as time, o.cln as department, o.dscrptn as detail, time(ovst.vstdttm) as time_serv
        FROM oapp as o 
        INNER JOIN ovst on ovst.vn = o.vn
        WHERE o.vn ='${seq}'`);
        return data[0];
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        h.vac as vaccine_code, 
        h.namehpt as vaccine_name
        from 
        hi.epi e 
        inner join 
        hi.ovst o on e.vn = o.vn 
        left join 
        hi.hpt as h on e.vac=h.codehpt
        where 
        o.hn='${hn}'
        
        UNION

        select 
        o.vstdttm as date_serv,
        DATE_FORMAT(time(o.drxtime),'%h:%i:%s') as time_serv, 
        vc.stdcode as vacine_code, 
        vc.\`name\` as vacine_name
        from 
        hi.ovst o 
        inner join 
        hi.prsc pc on o.vn = pc.vn  
        inner join 
        hi.prscdt pd on pc.prscno = pd.prscno  
        inner join 
        hi.meditem m on pd.meditem = m.meditem 
        inner join 
        hi.vaccine vc on vc.meditem = m.meditem  
        where 
        o.hn='${hn}'`);
        return data[0];
    }

    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,	
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        p.icd9cm as procedure_code,	
        p.icd9name as procedure_name,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(p.opdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(p.opdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
        from
        hi.ovst o 
        inner join 
        hi.ovstdx ox on o.vn = ox.vn 
        inner join
        hi.oprt p on o.vn = p.vn 
        left outer join
        hi.cln c on o.cln = c.cln
        LEFT OUTER JOIN 
        hi.dct on (
            CASE WHEN LENGTH(o.dct) = 5 THEN dct.lcno = o.dct 
                WHEN LENGTH(o.dct) = 4 THEN dct.dct = substr(o.dct,1,2)  
                WHEN LENGTH(o.dct) = 2 THEN dct.dct = o.dct END )
        where 
        o.vn = '${seq}' and p.an = 0 
        group by 
        p.vn,procedure_code
        UNION 
        SELECT 
        o.hn as pid,
        o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y%m%d') as date_serv,
        DATE_FORMAT(time(o.nrxtime),'%h:%i:%s') as time_serv, 
        i.ICD10TM as procedure_code,
        i.name_Tx as procedure_name,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as start_date,	
        DATE_FORMAT(time(dt.vstdttm),'%h:%i:%s') as start_time,
        DATE_FORMAT(date(dt.vstdttm),'%Y%m%d') as end_date,
        '00:00:00' as end_time
    
        FROM
        hi.dtdx 
        INNER JOIN 
        hi.icd9dent as i on dtdx.dttx=i.code_tx
        INNER JOIN 
        hi.dt on dtdx.dn=dt.dn
        INNER JOIN
        hi.ovst as o on dt.vn=o.vn and o.cln='40100'
        left outer join 
        hi.cln c on o.cln = c.cln  
        left join dentist as d on dt.dnt=d.codedtt
        where 
        o.vn = '${seq}'
        group by 
        dtdx.dn,procedure_code
        `);
        return data[0];
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select o.vn as seq,
        DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        p.bloodgrp as bloodgrp,
        o.bw as weight,
        o.height as height,
        o.bmi as bmi,
        o.tt as temperature,
        o.pr as pr,
        o.rr as rr,
        o.sbp as sbp,
        o.dbp as dbp,
        cc.symptom as symptom,
        c.cln as depcode,
        c.namecln as department
        FROM ovst as o 
        LEFT JOIN symptm as cc on o.vn = cc.vn
        INNER JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.vn = '${seq}'
        `);
        return data[0];
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT vn as seq , group_concat(sign order by id separator ' ') as pe FROM sign WHERE vn = '${seq}' group by vn
        `);
        return data[0];
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT p.vn as seq , group_concat(p.pillness order by id separator '') as hpi FROM pillness as p WHERE p.vn = '${seq}' group by p.vn
        `);
        return data[0];
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        select 
        idpm.idpm as ward_code,
        idpm.nameidpm as ward_name,
        count(an) as ward_pt,
        idpm.bed as ward_bed,
        idpm.bed_std as ward_standard 
        from 
        idpm 
        left join ipt on idpm.idpm=ipt.ward
        and dchdate = '0000-00-00'
        group by idpm
        `);
        return data[0];
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn,
        pt.pname as title_name, pt.fname as first_name, pt.lname as last_name,
        o.rfrno as referno,
        date(o.vstdate) as referdate,
        o.rfrlct as to_hcode,
        if(t.trauma='T',1,2) as pttype_id,
        if(t.trauma='T','Trauma','Non Trauma') as pttype_name,
        t.triage as strength_id,
        (case t.triage 
            when 1 then 'Resucitate'
            when 2 then 'Emergency'
            when 3 then 'Urgency'
            when 4 then 'Semi Urgency'
            when 5 then 'Non Urgency'
            else '' end 
            ) as strength_name,
        h.namehosp as to_hcode_name,
        f.namerfrcs as refer_cause,
        time(o.vsttime*100) as refertime,
        d.lcno as doctor,
        CONCAT(d.fname, ' ', d.lname) as doctor_name
        from orfro as o
        left Join hospcode as h on h.off_id = o.rfrlct
        inner join ovst on ovst.vn = o.vn
        inner Join pt on pt.hn = ovst.hn
        left Join rfrcs as f on f.rfrcs = o.rfrcs
        left Join dct as d on d.lcno = ovst.dct
        left join optriage as t on o.vn=t.vn
        where date(o.vstdate) between '${start_date}' and '${end_date}'`);
        return data[0];
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        select o.vn as seq, pt.hn as hn,
            pt.pname as title_name,
			pt.fname as first_name, 
			pt.lname as last_name,
            o.rfrno as referno,
            date(o.dchdate) as referdate,
            o.hcode as to_hcode,
            h.namehosp as to_hcode_name,
            '' as refer_cause,
            time(o.dchtime*100) as refertime,
            d.lcno as doctor,
            CONCAT(d.fname, ' ', d.lname) as doctor_name
        from referresult as o
        left Join hospcode as h on h.off_id = o.hcode
        left join ipt on ipt.an = o.an
		left join iptdx on ipt.an=iptdx.an and itemno=1 
        left Join pt on pt.hn = ipt.hn
        left Join dct as d on d.lcno = iptdx.dct
        where o.dchdate between '${start_date}' and '${end_date}'`);
        return data[0];
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        SELECT 
        a.vn as seq,
        a.fudate as receive_apppoint_date,
        '' as receive_apppoint_beginttime,
        '' as receive_apppoint_endtime,
        CONCAT(d.dspname,' ',l.prename,d.fname,' ',d.lname) as receive_apppoint_doctor,
        group_concat(distinct c.namecln) as receive_apppoint_chinic, 
        group_concat(distinct a.dscrptn) as receive_text,
        '' as receive_by
        from oapp as a 
        inner join pt as p on a.hn=p.hn
        left join dct as d on (case length(a.dct) when 5 then a.dct=d.lcno when 4 then substr(a.dct,3,2)=d.dct end)
        left join l_prename as l on d.pname=l.prename_code
        left join lab on substr(a.cln,2,3)=lab.labcode
        left join cln as c on if(substr(a.cln,1,1) !=7,a.cln=c.cln,c.cln='70100')
        WHERE a.hn ='${hn}' and a.vstdate = '${app_date}'
        `);
        return data[0];
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`

        `);
        return data[0];

    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`

        `);
        return data[0];
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`

        `);
        return data[0];
    }

    async getServicesCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT 
        o.hn,
        o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        c.namecln as department
        FROM ovst as o 
        INNER JOIN cln as c ON c.cln = o.cln 
        INNER JOIN pt as p	ON p.hn = o.hn
        WHERE o.hn ='${hn}' and o.ovstost = '3'
		UNION
		SELECT 
        o.hn,
		o.vn as seq,p.pname as title_name,p.fname as first_name,p.lname as last_name, 
		DATE_FORMAT(date(o.vstdttm),'%Y-%m-%d') as date_serv, 
        DATE_FORMAT(time(o.vstdttm),'%h:%i:%s') as time_serv, 
        idpm.nameidpm as department
		from ipt 
		LEFT JOIN idpm  on idpm.idpm = ipt.ward
		INNER JOIN ovst as o on o.vn = ipt.vn
	    INNER JOIN pt as p	ON p.hn = o.hn
        WHERE ipt.hn ='${hn}' and ipt.dchtype = '4'
        ORDER BY seq DESC limit 3
        `);
        return data[0];
    }

    async getProfileCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        select p.hn as hn, p.pop_id as cid, p.pname as title_name,p.fname as first_name,p.lname as last_name,
        p.moopart,p.addrpart,p.tmbpart,amppart,chwpart,p.brthdate
        ,concat(lpad(timestampdiff(year,p.brthdate,now()),3,'0'),'-'
        ,lpad(mod(timestampdiff(month,p.brthdate,now()),12),2,'0'),'-'
        ,lpad(if(day(p.brthdate)>day(now()),dayofmonth(now())-(day(p.brthdate)-day(now())),day(now())-day(p.brthdate)),2,'0')) as age
        ,p.pttype as pttype_id,t.namepttype as pttype_name,s.card_id as pttype_no,s.hospmain
        ,c.hosname as hospmain_name,s.hospsub,h.hosname as hospsub_name,p.fdate as registdate,p.ldate as visitdate
        FROM pt as p 
        INNER JOIN pttype as t on p.pttype=t.pttype
        left join insure as s on p.hn=s.hn and p.pttype=s.pttype
        left join chospital as c on s.hospmain=c.hoscode
        left join chospital as h on s.hospmain=h.hoscode
        WHERE p.hn ='${hn}'`);
        return data[0];
    }

}