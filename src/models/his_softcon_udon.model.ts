import Knex = require('knex');
const hospcode = process.env.HIS_CODE;

export class HisSoftConUdonModel {

    async getLogin(db: Knex, username: any, password: any) {
        // console.log(db);
        
        let data = await db.raw(`
        select ltrim(rtrim(name)) as username
        ,ltrim(rtrim(displayname)) as fullname
        ,'11501' as hcode
        from SoftCon.[User] u 
        where ltrim(rtrim(u.name)) = '${username}'
        and u.password = dbo.[ComputeHash]('${password}')
        `);
        return data[0];
        // let data = await db(`SoftCon.[User] u`)
        // .select(db.raw(`ltrim(rtrim(name)) as username`) , db.raw(`ltrim(rtrim(displayname)) as fullname`),db.raw(`'11501' as hcode `))
        // .andWhereRaw(`ltrim(rtrim(u.name)) = ? and u.password = dbo.[ComputeHash](?)`,[username,password]);
        // return data;
    }

    async getServices(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,null as department
        ,'OPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        where r.ClinicVisitKey is not null
        and r.VisitKey = '${seq}'
        union all
        select top 1 r.VisitKey as seq
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
       , null as department
        ,'IPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey        
      where r.AdmitKey is not null        and r.VisitKey = '${seq}'
        `);
        return data[0];
    }

    async getProfile(db: Knex, hn: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1
        pt.Code as hn
        ,r.patientkey 
        ,(case when p.cid = '' or p.cid is null then '9999999999999' else p.cid end) as cid
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,case when p.moo is null  then '00' when LEN(p.moo)= 1  then '0'+ltrim(rtrim(p.moo))
         else ltrim(rtrim(p.moo))
        end as moopart
        ,p.addressno as addrpart
        ,right(p.thsubdistrictkey,2)  as tmbpart
        ,right(left(p.THSubdistrictKey,4),2) as amppart
        ,case when p.thprovincekey = '' or p.thprovincekey is null then '34' else p.thprovincekey end as chwpart
        ,convert(varchar(10),p.birthdt,121) as brthdate
        ,RIGHT('000'+LTRIM(STR(year(getdate()) - year(p.birthdt))),2) + ''+ 
         RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthdt, getdate()))%12)),0)+''+ 
         RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthdt),DAY(GETDATE())),'-',''))),0)  as age 
        ,replace(p.genderkey,'-','') as sex
        ,(case when p.genderkey = '-1' then 'ชาย' 
         when p.genderkey = '-2' then 'หญิง'
          else 'ไม่ระบุเพศ' end) as sexname
        ,o.Name as occupation
        ,null as pttype_id  --รหัสสิทธิ์การรักษา
        ,null as pttype_name --สิทธิ์การรักษา
        ,null as pttype_no --เลขที่สิทธิ์การรักษา
        ,11501 as hospmain --รหัส รพ.สิทธิ์การรักษาหลัก
        ,null as hospmain_name --รพ.สิทธิ์การรักษา
        ,null as hospsub -- รหัส รพ.สิทธิ์การรักษารอง
        ,null as hospsub_name --รพ.สิทธิ์การรักษารอง
        ,convert(varchar(10),pt.docdt,121) as registdate 
        ,convert(varchar(10),r.docdt,121) as visitdate
        ,concat(p.fatherfirstname,' ',p.fatherlastname) as father_name
        ,concat(p.motherfirstname,' ',p.motherlastname) as mother_name
        ,concat(p.spousefirstname,' ',p.spouselastname) as couple_name
        ,p.contactpersonname as contact_name
        ,p.contactpersonrelation as contact_relation
        ,p.contactpersontelephone as contact_mobile
        from person p
        join referout r on p.personkey = r.patientkey 
        left join Title t on p.titlekey = t.titlekey
        left join Occupation o on p.occupationkey = o.occupationkey
        left join patient pp on p.personkey = pp.patientkey
		LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
		LEFT JOIN dbo.Admit a ON a.AdmitKey = r.AdmitKey
		LEFT JOIN dbo.ClinicVisit cv ON cv.ClinicVisitKey = r.ClinicVisitKey        where pt.Code = '${hn}' and r.VisitKey = '${seq}'
        `);
        return data[0];
    }

    async getHospital(db: Knex, hn: any) {
        let data = await db.raw(`
        SELECT '11501' as provider_code,'โรงพยาบาลค่ายประจักษ์ศิลปาคม' as provider_name
        `);
        return data;
    }

    async getAllergyDetail(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        SELECT drg.name as drug_name
        ,adr.note as symptom 
        ,convert(varchar(10),adr.adrcardissuedate,121) as begin_date
        ,convert(varchar(10),adr.recorddt,121) as daterecord
        FROM PatientADR adr
        inner join DrugGenericName drg on adr.druggenericnamekey = drg.druggenericnamekey
		JOIN dbo.Patient p ON p.PatientKey = adr.PatientKey
        where p.Code = '${hn}'
        `);
        return data;
    }

    async getChronic(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getDiagnosis(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        `);
        return data;
    }

    async getRefer(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq
        ,'' as an
        ,r.patientkey as pid
        ,pt.Code as hn
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,h.Code AS to_hcode
        ,'nontrauma' as pttype_id -- รหัสของ Trauma / Non Trauma
        ,'Non Trauma' as pttype_name -- ข้อมูลเป็น Trauma / Non Trauma
		, (case when e.Code = '' or e.Code is null 
         then '1' else e.Code end) as strength_id
        ,(case e.Code 
                    when 5 then 'Resucitate'
                    when 4 then 'Emergency'
                    when 3 then 'Urgency'
                    when 2 then 'Semi Urgency'
                    when 1 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,null as location_name --จุดส่งต่อ เช่น อายุรกรรม / เวชปฏิบัติทั่วไป / อุบัติเหตุ-ฉุกเฉิน / ตึกผู้ป่วยใน  --แผน
        ,null as station_name -- แผนกที่นำส่ง เช่น OPD , IPD , ER 
        ,r.code as loads_id   -- รหัสการนำส่ง       
        ,null as loads_name  -- การนำส่ง เช่น รถ Ambulance , รถ Ambulance พร้อมพยาบาล , ไปเอง , เจ้าหน้าที่นำส่ง , EMS
        ,le.Name AS to_hcode_name --หาชื่อ hcode
        ,refercause.name as refer_cause
        ,convert(varchar,r.DocDT,108) as refertime
        ,em.ProfessionPermitID as doctor --เอาเลข ว.แพทย์ ฟิวด์นี้ไม่รู้ถูกไหม
        ,doctor.fullname as doctor_name
        , REPLACE(REPLACE(r.currentsicknote, CHAR(13), ''), CHAR(10), '') AS refer_remark --doctor note
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        left join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join person doctor on r.doctorkey = doctor.personkey
        left join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		left JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
		left JOIN dbo.Employee em ON em.employeeKey = r.doctorKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
     LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
        where r.ClinicVisitKey is not null
        and r.VisitKey = '${seq}' and replace(r.code,'/','-') = '${referno}'
        
        union all 
        
        select top 1 r.VisitKey as seq
        ,replace(a.an,'/','-') as an
        ,r.patientkey as pid
        ,pt.Code as hn
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,h.Code AS to_hcode
        ,'nontrauma' as pttype_id -- รหัสของ Trauma / Non Trauma
        ,'Non Trauma' as pttype_name -- ข้อมูลเป็น Trauma / Non Trauma
		, (case when e.Code = '' or e.Code is null 
         then '1' else e.Code end) as strength_id
        ,(case e.Code 
                    when 5 then 'Resucitate'
                    when 4 then 'Emergency'
                    when 3 then 'Urgency'
                    when 2 then 'Semi Urgency'
                    when 1 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        , NULL as location_name --จุดส่งต่อ เช่น อายุรกรรม / เวชปฏิบัติทั่วไป / อุบัติเหตุ-ฉุกเฉิน / ตึกผู้ป่วยใน
        , NULL as station_name -- แผนกที่นำส่ง เช่น OPD , IPD , ER 
        , NULL as loads_id   -- รหัสการนำส่ง       
        , NULL as loads_name  -- การนำส่ง เช่น รถ Ambulance , รถ Ambulance พร้อมพยาบาล , ไปเอง , เจ้าหน้าที่นำส่ง , EMS
        ,le.Name as to_hcode_name --หาชื่อ hcode
        ,refercause.name as refer_cause
        ,convert(varchar,r.DocDT,108) as refertime
        ,em.ProfessionPermitID as doctor --เอาเลข ว.แพทย์ ฟิวด์นี้ไม่รู้ถูกไหม
        ,doctor.fullname as doctor_name
        , REPLACE(REPLACE(r.currentsicknote, CHAR(13), ''), CHAR(10), '') AS refer_remark --doctor note
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        left join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        left join admit a on r.admitkey = a.admitkey
        left join person doctor on r.doctorkey = doctor.personkey
        left join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		left JOIN dbo.Employee em ON em.employeeKey = r.doctorKey
		left JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
    LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
        where r.AdmitKey is not null
        and r.VisitKey = '${seq}' and replace(r.code,'/','-') = '${referno}'        `);
        return data;
    }

    async getDrugs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select  
        mh.VisitKey as seq
        ,convert(varchar(10),mh.docdt,121) as date_serv
        ,convert(varchar,mh.docdt,108) as time_serv
        ,mi.itemname as drug_name
        ,mi.IssueQty as qty
        ,mi.itemunitname as unit 
        ,REPLACE(REPLACE(mi.usagenote, CHAR(13), ''), CHAR(10), '')  as usage_line1
        ,'' as usage_line2
        ,'' as usage_line3
        from medrequest mi
        inner join medrequestheader mh on mi.medrequestheaderkey = mh.medrequestheaderkey
        where mh.VisitKey ='${seq}'
        order by docdt DESC
        `);
        return data;
    }

    async getLabs(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT
        l.VisitKey as seq,
		convert(varchar(10),l.DocDT,121) as date_serv,
		convert(varchar,l.DocDT,108) as time_serv ,
		lic.Name
		as labgroup,
		lr.ItemName as lab_name,        
		ISNULL( lod.Name , ls.ResultValue) +
		CASE WHEN lod.Name IS NOT NULL AND ls.ResultValue IS NOT NULL THEN ls.ResultValue ELSE '' END as lab_result,        
		lr.ItemUnitName as unit, 
		CASE when (CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),null)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), null) END ) is null then '.' else 
		CASE WHEN (lo.HasGenderVar = 1) THEN ISNULL(CAST(CAST(lor.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(lor.MaxRefValue AS decimal(19,3)) AS nvarchar(10)),null)  ELSE ISNULL(CAST(CAST(ls.MinRefValue AS decimal(19,3)) AS nvarchar(10)) + ' - ' + CAST(CAST(ls.MaxRefValue AS decimal(19,3)) AS nvarchar(10)), null) END
		end as standard_result
        from LabRequestHeader l
		JOIN dbo.Person ps ON ps.PersonKey = l.PatientKey
		JOIN LabRequest lr on l.LabRequestHeaderKey = lr.LabRequestHeaderKey
		LEFT JOIN LabResult ls on ls.LabRequestKey = lr.LabRequestKey
		LEFT JOIN LabOutput lo on lo.LabOutputKey = ls.LabOutputKey
		LEFT JOIN dbo.LabOutputDomain lod ON lod.LabOutputDomainKey = ls.LabOutputDomainKey
		JOIN dbo.LabItem li ON li.ItemKey = lr.ItemKey
		JOIN dbo.LabItemCategory lic ON lic.LabItemCategoryKey = li.LabItemCategoryKey
		LEFT JOIN dbo.LabOutputRef lor ON lor.LabOutputKey = lo.LabOutputKey AND lor.GenderKey = ps.GenderKey        
		WHERE l.VisitKey = '${seq}' and isnull(ls.ResultValue,'') <> ''
		AND l.IsCanceled = 0        `);
        return data;
    }

    async getAppointment(db: Knex, hn: any, dateServ: any, seq: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getVaccine(db: Knex, hn: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }
    async getProcedure(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
     SELECT 
        v.PatientKey as pid,
        v.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        i.Code as procedure_code,	
        r.ItemName as procedure_name,
        convert(varchar(10),h.DocDT,121) as start_date,	
        convert(varchar,h.DocDT,108) as start_time,
        convert(varchar(10),h.ExpectIssueDate,121) as end_date,
        convert(varchar,h.ExpectIssueTime,108) as end_time
		from ORRequestHeader h 
		INNER JOIN Visit v on  v.VisitKey = h.VisitKey
		INNER JOIN ORRequest r on r.ORRequestHeaderKey = h.ORRequestHeaderKey
		JOIN dbo.Item i ON i.ItemKey = r.ItemKey
		WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }

    async getNurture(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        select top 1 r.VisitKey as seq,
        convert(varchar(10),v.DocDT,121) as date_serv, 
        convert(varchar,v.DocDT,108) as time_serv, 
        '' as bloodgrp,
        c.Weight as weight,
        c.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        null as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        null as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        LEFT JOIN Visit v on r.VisitKey = v.VisitKey
        INNER JOIN ClinicVisit c ON c.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ClinicVisitVS vs on vs.ClinicVisitKey = r.ClinicVisitKey
        LEFT JOIN ServiceUnit s on s.serviceunitkey = c.serviceunitkey
        WHERE r.ClinicVisitKey is not null
        and v.VisitKey = '${seq}'

        UNION all 

        select 	top 1 r.VisitKey as seq,
        convert(varchar(10),a.DocDT,121) as date_serv, 
        convert(varchar,a.DocDT,108) as time_serv, 
        '' as bloodgrp,
        a.Weight as weight,
        a.Height as height,
        '' as bmi,
        vs.Temp as temperature,
        vs.PR as pr,
        vs.RR as rr,
        vs.SystolicBP as sbp,
        vs.DiastolicBP as dbp,
        a.PENurseNote as symptom,
        s.Code as depcode,
        s.Name as department,
        '' as movement_score,
        '' as vocal_score,
        '' as eye_score,
        vs.O2Sat as oxygen_sat,
        null as gak_coma_sco,
        '' as diag_text,
        '' as pupil_right,
        '' as pupil_left
        FROM ReferOut r
        INNER JOIN Admit a ON a.AdmitKey = r.AdmitKey
        INNER JOIN AdmitVS vs on vs.AdmitKey = a.AdmitKey
        LEFT JOIN ClinicVisit c ON c.ClinicVisitKey = a.ClinicVisitKey
        LEFT JOIN ServiceUnit s on c.serviceunitkey = s.serviceunitkey
        WHERE r.AdmitKey is not null 
        and a.VisitKey = '${seq}'        `);
        return data;
    }

    async getPhysical(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PENote, CHAR(13), ''), CHAR(10), '') as pe FROM ClinicVisit c WHERE  c.VisitKey = ${seq}
        `);
        return data;
    }

    async getPillness(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT c.VisitKey as seq , REPLACE(REPLACE(c.PINote, CHAR(13), ''), CHAR(10), '') as hpi FROM ClinicVisit c 
WHERE c.VisitKey = ${seq}
        `);
        return data;
    }

    async getBedsharing(db: Knex) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getReferOut(db: Knex, start_date: any, end_date: any) {
        // console.log('start_date',start_date);
        // console.log('end_date',end_date);
        
        let data = await db.raw(`
        SELECT r.VisitKey as seq 
        ,pt.Code as hn
        ,null as AN
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,convert(varchar,r.DocDT,108) as refertime
        ,null as location_name
        ,h.Code as to_hcode
        ,mc.code as pttype_id
        ,mc.name as pttype_name
        		, (case when e.Code = '' or e.Code is null 
         then '1' else e.Code end) as strength_id
        ,(case e.Code 
                    when 5 then 'Resucitate'
                    when 4 then 'Emergency'
                    when 3 then 'Urgency'
                    when 2 then 'Semi Urgency'
                    when 1 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,le.Name as to_hcode_name
        ,refercause.name as refer_cause
        ,r.doctorkey as doctor
        ,doctor.fullname as doctor_name
        ,r.CurrentSickNote as refer_remark
        FROM ReferOut  r 
        join person p on r.patientkey = p.personkey
        JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        join Title t on p.titlekey = t.titlekey
        join ClinicVisit c on r.clinicvisitkey = c.clinicvisitkey
        left join Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
        join person doctor on r.doctorkey = doctor.personkey
        join ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
		LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
		JOIN dbo.VisitMC vmc ON vmc.VisitMCKey = c.MainVisitMCKey
		JOIN dbo.PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
		JOIN dbo.MC ON mc.MCKey = pmc.MCKey
        where convert(varchar(10),r.DocDT,121) between '${start_date}' and '${end_date}'   and  c.ClinicVisitSKey <> 32
        UNION  
        SELECT 
        r.VisitKey as seq  
        ,pt.Code as hn 
        , replace(a.an,'/','-') as AN
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,replace(r.code,'/','-') as referno
        ,convert(varchar(10),r.DocDT,121) as referdate
        ,convert(varchar,r.DocDT,108) as refertime
        ,null as location_name
        ,h.Code as to_hcode
        ,mc.code as pttype_id
        ,mc.name as pttype_name
		, (case when e.Code = '' or e.Code is null 
         then '1' else e.Code end) as strength_id
        ,(case e.Code 
                    when 5 then 'Resucitate'
                    when 4 then 'Emergency'
                    when 3 then 'Urgency'
                    when 2 then 'Semi Urgency'
                    when 1 then 'Non Urgency'
                    else 'Non Urgency' end 
                    ) as strength_name
        ,le.Name as to_hcode_name  
        ,refercause.name as refer_cause
        ,r.doctorkey as doctor
        ,doctor.fullname as doctor_name
        ,r.CurrentSickNote as refer_remark
        FROM ReferOut  r 
        JOIN person p on r.patientkey = p.personkey
        JOIN Title t on p.titlekey = t.titlekey
        JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        LEFT JOIN admit a on r.admitkey = a.admitkey
        LEFT JOIN dbo.Hospital h ON r.referhospitalkey = h.hospitalkey
        LEFT JOIN ClinicVisit c ON c.ClinicVisitKey = a.ClinicVisitKey
        INNER JOIN AdmitVS vs on vs.AdmitKey = a.AdmitKey
        JOIN dbo.VisitMC vmc ON vmc.VisitMCKey = c.MainVisitMCKey
		JOIN dbo.PatientMC pmc ON pmc.PatientMCKey = vmc.PatientMCKey
		JOIN dbo.MC ON mc.MCKey = pmc.MCKey  
		LEFT JOIN Emergencylevel e on e.EmergencyLevelKey = c.EmergencyLevelKey
		LEFT JOIN dbo.LegalEntity le ON le.LegalEntityKey = h.HospitalKey
		JOIN ReferObjective refercause on r.ReferObjectiveKey = refercause.ReferObjectiveKey
		join person doctor on r.doctorkey = doctor.personkey
        where convert(varchar(10),r.DocDT,121) between '${start_date}' and '${end_date}' 
        and r.AdmitKey is not null   and c.ClinicVisitSKey  <> 32
		         `);
        // console.log(data);
        
        return data;
    }

    async getReferBack(db: Knex, start_date: any, end_date: any) {
        let data = await db.raw(`
        `);
        return data;
    }

    async getAppoint(db: Knex, hn: any, app_date: any, referno: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getXray(db: Knex, hn: any, dateServe: any, seq: any, referno: any) {
        let data = await db.raw(`
        SELECT h.DocDT, x.ItemName  
        FROM RadRequest as x  
        INNER JOIN RadRequestHeader h on x.RadRequestHeaderKey = h.RadRequestHeaderKey
        WHERE h.VisitKey = '${seq}'
        `);
        return data;
    }

    async getDepartment(db: Knex) {
        let data = await db.raw(`
        SELECT 
          s.Code as dep_code , 
          s.Name as dep_name 
        from ServiceUnit s
       WHERE s.IsDisabled = 0 
        `);
        return data;
    }

    async getPtHN(db: Knex, cid: any) {
        let data = await db.raw(`
        SELECT pt.Code as hn 
        from Person p 
        INNER JOIN patient pt on pt.PatientKey = p.PersonKey
        WHERE p.Cid = '${cid}'
        `);
        return data[0];
    }

    async getMedrecconcile(db: Knex, hn: any) {
        let data = await db.raw(`
        
        `);
        return data;
    }

    async getServicesCoc(db: Knex, hn: any, seq: any) {
        let data = await db.raw(`
        select top 3 r.VisitKey as seq
        pt.Code as hn,
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
        ,null as department
        ,'OPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey
        LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
        where r.ClinicVisitKey is not null
        and pt.Code = '${hn}'
        union all
        select top 3 r.VisitKey as seq
        pt.Code as hn,
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,convert(varchar(10),r.DocDT,121) as date_serv
        ,convert(varchar,r.DocDT,108) as time_serv
       , null as department
        ,'IPD' as dep
        from referout r
        join person p on r.patientkey = p.personkey
        join Title t on p.titlekey = t.titlekey  
        LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey      
      where r.AdmitKey is not null  and pt.Code = '${hn}'
      ORDER BY seq DESC

        `);
        return data;
    }    

    async getProfileCoc(db: Knex, hn: any) {
        let data = await db.raw(`
        select top 1
        pt.Code as hn
        ,r.patientkey 
        ,(case when p.cid = '' or p.cid is null then '9999999999999' else p.cid end) as cid
        ,t.name as title_name
        ,p.firstname as first_name
        ,p.lastname as last_name
        ,case when p.moo is null  then '00' when LEN(p.moo)= 1  then '0'+ltrim(rtrim(p.moo))
         else ltrim(rtrim(p.moo))
        end as moopart
        ,p.addressno as addrpart
        ,right(p.thsubdistrictkey,2)  as tmbpart
        ,right(left(p.THSubdistrictKey,4),2) as amppart
        ,case when p.thprovincekey = '' or p.thprovincekey is null then '34' else p.thprovincekey end as chwpart
        ,convert(varchar(10),p.birthdt,121) as brthdate
        ,RIGHT('000'+LTRIM(STR(year(getdate()) - year(p.birthdt))),2) + ''+ 
         RIGHT('00'+LTRIM(STR(ABS(DATEDIFF(month, p.birthdt, getdate()))%12)),0)+''+ 
         RIGHT('00'+LTRIM(STR(replace(DATEDIFF(DAY,DAY(p.birthdt),DAY(GETDATE())),'-',''))),0)  as age 
        ,replace(p.genderkey,'-','') as sex
        ,(case when p.genderkey = '-1' then 'ชาย' 
         when p.genderkey = '-2' then 'หญิง'
          else 'ไม่ระบุเพศ' end) as sexname
        ,o.Name as occupation
        ,null as pttype_id  --รหัสสิทธิ์การรักษา
        ,null as pttype_name --สิทธิ์การรักษา
        ,null as pttype_no --เลขที่สิทธิ์การรักษา
        ,11501 as hospmain --รหัส รพ.สิทธิ์การรักษาหลัก
        ,null as hospmain_name --รพ.สิทธิ์การรักษา
        ,null as hospsub -- รหัส รพ.สิทธิ์การรักษารอง
        ,null as hospsub_name --รพ.สิทธิ์การรักษารอง
        ,convert(varchar(10),pt.docdt,121) as registdate 
        ,convert(varchar(10),r.docdt,121) as visitdate
        ,concat(p.fatherfirstname,' ',p.fatherlastname) as father_name
        ,concat(p.motherfirstname,' ',p.motherlastname) as mother_name
        ,concat(p.spousefirstname,' ',p.spouselastname) as couple_name
        ,p.contactpersonname as contact_name
        ,p.contactpersonrelation as contact_relation
        ,p.contactpersontelephone as contact_mobile
        from person p
        join referout r on p.personkey = r.patientkey 
        left join Title t on p.titlekey = t.titlekey
        left join Occupation o on p.occupationkey = o.occupationkey
        left join patient pp on p.personkey = pp.patientkey
		LEFT JOIN dbo.Patient pt ON pt.PatientKey = r.PatientKey
		LEFT JOIN dbo.Admit a ON a.AdmitKey = r.AdmitKey
		LEFT JOIN dbo.ClinicVisit cv ON cv.ClinicVisitKey = r.ClinicVisitKey where pt.Code = '${hn}'
        `);
        return data[0];
    }
}

