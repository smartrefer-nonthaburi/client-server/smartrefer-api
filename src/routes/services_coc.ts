import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import moment from 'moment-timezone';

// model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'
import { HisEphisHiModel } from '../models/his_ephis.model';
import { HisHosxpPcuXEModel } from '../models/his_hosxppcuxe.model';
import { HisHosxpv4PCUModel } from '../models/his_hosxpv4_pcu.model';
import { HisHosxpPCUModel } from '../models/his_hosxp_pcu.model';
import { HisUbonrakThonburiModel } from '../models/his_ubonrakthonburi.model';
import { HisHosxpv3loeiModel } from '../models/his_hosxpv3loei.model';

import { RerferOutModel } from '../models/ket/referout'
import { RerferBackModel } from '../models/ket/referback'


const rerferOutModel = new RerferOutModel();
const rerferBackModel = new RerferBackModel();

export default async function servicesCoc(fastify: FastifyInstance) {

    // ห้ามแก้ไข // 
    //-----------------BEGIN START-------------------//
    const provider = process.env.HIS_PROVIDER;
    const hoscode = process.env.HIS_CODE;
    const connection = process.env.DB_CLIENT;
    let db: any;
    let hisModel: any;

    switch (connection) {
        case 'mysql2':
            db = fastify.mysql2;
            break;
        case 'mysql':
            db = fastify.mysql;
            break;
        case 'mssql':
            db = fastify.mssql;
            break;
        case 'pg':
            db = fastify.pg;
            break;
        case 'oracledb':
            db = fastify.oracledb;
            break;
                  
        default:
            db = fastify.mysql;
    }

    switch (provider) {
        case 'ubase':
            hisModel = new HisUbaseModel();
            break;
        case 'himpro':
            hisModel = new HisHimproHiModel();
            break;
        case 'jhcis':
            hisModel = new HisJhcisHiModel();
            break;
        case 'hosxpv3':
            hisModel = new HisHosxpv3Model();
            break;
        case 'hosxpv3arjaro':
            hisModel = new HisHosxpv3ArjaroModel();
            break;
        case 'hosxpv4':
            hisModel = new HisHosxpv4Model();
            break;
        case 'hosxpv4pg':
            hisModel = new HisHosxpv4PgModel();
            break;
        case 'hi':
            hisModel = new HisHiModel();
            break;
        case 'homc':
            hisModel = new HisHomcModel();
            break;
        case 'mbase':
            hisModel = new HisMbaseModel();
            break;
        case 'phosp':
            hisModel = new HisPhospModel();
            break;
        case 'universal':
            hisModel = new HisUniversalHiModel();
            break;
        case 'sakon':
            hisModel = new HisSakonModel();
            break;
        case 'homcudon':
            hisModel = new HisHomcUbonModel();
            break;
        case 'softcon':
            hisModel = new HisSoftConModel();
            break;
        case 'softconudon':
            hisModel = new HisSoftConUdonModel();
            break;
        case 'ssb':
            hisModel = new HisSsbModel();
            break;
        case 'panaceaplus':
            hisModel = new HisPanaceaplusModel();
            break;
        case 'hosxppcuv4':
            hisModel = new HisHosxpv4PCUModel();
            break;
        case 'ephis':
            hisModel = new HisEphisHiModel();
            break;
        case 'hosxppcuxe':
            hisModel = new HisHosxpPcuXEModel();
            break;
        case 'hosxppcu':
            hisModel = new HisHosxpPCUModel();
            break;
        case 'ubonrakthonburi':
            hisModel = new HisUbonrakThonburiModel();
            break;
        case 'hosxploei':
            hisModel = new HisHosxpv3loeiModel();
            break;
        default:
        // hisModel = new HisModel();
    }
    //------------------BEGIN END------------------//

    // getProfileCoc
    fastify.get('/patient/:hn', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let hn = req.params.hn;

        try {
            let res_patient = await hisModel.getProfileCoc(db, hn);
            reply.send(res_patient);

        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // view/00000/00000000/64000000
    fastify.get('/view/:hn', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hn = req.params.hn;
        let objService: any = {};
        let providerCode;
        let providerName;
        let profile = [];
        let service: any = [];

        let rs_hospital = [];
        let rs_profile = [];
        let rs_allergy = [];
        let rs_services = [];
        let rs_diagnosis = [];
        let rs_drugs = [];
        let rs_procedure = [];
        let rs_lab = [];
        let rs_xray = [];
        let rs_refers = [];
        let rs_nurtures = [];
        let rs_physicals = [];
        let rs_pillness = [];
        let rs_medrecconcile = [];

        // let providerCodeToken = req.decoded.provider_code;
        if (hn) {
            try {
                // console.log("provider:", provider);
                // console.log("process.env.HIS_PROVIDER:", process.env.HIS_PROVIDER);
                rs_hospital.push(await hisModel.getHospital(db, hn));
                console.log("rs_hospital:", rs_hospital[0]);
                if (rs_hospital[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        let rs_hosp = [];
                        rs_hosp = rs_hospital[0];
                        // console.log(rs_hosp[0].provider_code);

                        providerCode = rs_hosp[0].provider_code;
                        providerName = rs_hosp[0].provider_name;
                    } else {
                        providerCode = rs_hospital[0][0].provider_code;
                        providerName = rs_hospital[0][0].provider_name;
                    }
                }
                rs_profile.push(await hisModel.getProfileCoc(db, hn));
                console.log("rs_profile:", rs_profile);
                if (rs_profile) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_profile = rs_profile
                    } else {
                        rs_profile = rs_profile[0]
                    }
                    for (const rpro of rs_profile) {
                        const objPro = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "age": rpro.age,
                            "sex": rpro.sex,
                            "occupation": rpro.occupation,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                }

                rs_allergy.push(await hisModel.getAllergyDetail(db, hn));
                console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_allergy = rs_allergy[0]
                    } else {
                        rs_allergy = rs_allergy[0]
                    }
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        const objAllergy = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                rs_medrecconcile.push(await hisModel.getMedrecconcile(db, hn));
                console.log("rs_medrecconcile:", rs_medrecconcile[0]);
                if (rs_medrecconcile[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_medrecconcile = rs_medrecconcile[0]
                    } else {
                        rs_medrecconcile = rs_medrecconcile[0]
                    }
                    let medrecconcile: any = [];
                    for (const ra of rs_medrecconcile) {
                        const objMedrecconcile = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_hospcode": ra.drug_hospcode,
                            "drug_hospname": ra.drug_hospname,
                            "drug_name": ra.drug_name,
                            "drug_use": ra.drug_use,
                            "drug_receive_date": moment(ra.drug_receive_date).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        medrecconcile.push(objMedrecconcile);
                    }
                    objService.medrecconcile = medrecconcile;
                }

                rs_services = await hisModel.getServicesCoc(db, hn);
                console.log("rs_services:", rs_services);
                if (rs_services[0]) {

                    let objServiceCoc: any = {};

                    let pillness = [];
                    let physicals = [];
                    let nurtures = [];
                    let diagnosis = [];
                    let drugs = [];
                    let lab = [];
                    let procedure = [];
                    let refer = [];
                    let xray = [];

                    for (const v of rs_services) {
                        // console.log(v.seq);


                        rs_pillness = await hisModel.getPillness(db, hn, v.date_serv, v.seq);
                        console.log("rs_pillness:", rs_pillness);
                        if (rs_pillness[0]) {
                            for (const ri of rs_pillness) {
                                console.log(ri);

                                const objPillness = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": ri.seq,
                                    "hpi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                        }

                        rs_physicals = await hisModel.getPhysical(db, hn, v.date_serv, v.seq);
                        console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            for (const rp of rs_physicals) {
                                const objPhysicals = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                        }

                        rs_nurtures = await hisModel.getNurture(db, hn, v.date_serv, v.seq);
                        console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            for (const rn of rs_nurtures) {
                                const objNurtures = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight,
                                    "height": rn.height,
                                    "bmi": rn.bmi,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "cc": rn.symptom,
                                    "pmh": rn.pmh,
                                    "depcode": rn.depcode,
                                    "department": rn.department,
                                    "movement_score": rn.movement_score,
                                    "vocal_score": rn.vocal_score,
                                    "eye_score": rn.eye_score,
                                    "oxygen_sat": rn.oxygen_sat,
                                    "gak_coma_sco": rn.gak_coma_sco,
                                    "pupil_right": rn.pupil_right,
                                    "pupil_left": rn.pupil_left,
                                    "diag_text": rn.diag_text || ''
                                }
                                nurtures.push(objNurtures);
                            }
                        }

                        rs_diagnosis = await hisModel.getDiagnosis(db, hn, v.date_serv, v.seq);
                        console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            for (const rg of rs_diagnosis) {
                                const objDiagnosis = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "DiagNote": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                        }

                        rs_procedure = await hisModel.getProcedure(db, hn, v.date_serv, v.seq);
                        console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            for (const rp of rs_procedure) {
                                const objProcedure = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                        }

                        rs_drugs = await hisModel.getDrugs(db, hn, v.date_serv, v.seq);
                        console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            for (const rd of rs_drugs) {
                                const objDrug = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2,
                                    "usage_line3": rd.usage_line3
                                }
                                drugs.push(objDrug);
                            }
                        }

                        rs_lab = await hisModel.getLabs(db, hn, v.date_serv, v.seq);
                        console.log("rs_lab:", rs_lab);
                        if (rs_lab[0]) {
                            console.log('111', rs_lab);
                            for (const rl of rs_lab) {
                                let standard_result: any;
                                if (rl.standard_result) {
                                    standard_result = rl.standard_result.toString();
                                } else {
                                    standard_result = rl.standard_result;
                                }
                                const objLab = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "date_serv": moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit,
                                    "standard_result": standard_result
                                }
                                lab.push(objLab);
                            }
                        } else {
                            console.log('222', rs_lab);
                        }

                        rs_xray = await hisModel.getXray(db, hn, v.date_serv, v.seq);
                        console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            for (const v of rs_xray) {
                                const objXray = {
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                        }

                        rs_refers = await hisModel.getRefer(db, hn, v.date_serv, v.seq ,v.referno);
                        console.log("rs_refers:", rs_refers[0]);
                        if (rs_refers[0]) {

                            for (const rs_refer of rs_refers) {
                                const objRefer = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rs_refer.seq,
                                    "an": rs_refer.an,
                                    "pid": rs_refer.pid,
                                    "hn": rs_refer.hn,
                                    "referno": rs_refer.referno,
                                    "location_name": rs_refer.location_name,
                                    "station_name": rs_refer.station_name,
                                    "pttype_id": rs_refer.pttype_id,
                                    "pttype_name": rs_refer.pttype_name,
                                    "strength_id": rs_refer.strength_id,
                                    "strength_name": rs_refer.strength_name,
                                    "load_id": rs_refer.loads_id,
                                    "loads_name": rs_refer.loads_name,
                                    "ReferDate": moment(rs_refer.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "ReferTime": rs_refer.refertime,
                                    "to_hcode": rs_refer.to_hcode,
                                    "to_hcode_name": rs_refer.to_hcode_name,
                                    "doctor": rs_refer.doctor,
                                    "doctor_name": rs_refer.doctor_name,
                                    "refer_cause": rs_refer.refer_cause,
                                    "refer_remark": rs_refer.refer_remark,
                                }
                                refer.push(objRefer);
                            }
                        }

                    }
                    objServiceCoc.pillness = pillness;
                    objServiceCoc.physicals = physicals;
                    objServiceCoc.nurtures = nurtures;
                    objServiceCoc.diagnosis = diagnosis;
                    objServiceCoc.procedure = procedure;
                    objServiceCoc.drugs = drugs;
                    objServiceCoc.lab = lab;
                    objServiceCoc.xray = xray;
                    objServiceCoc.refer = refer;

                    service.push(objServiceCoc);

                }
                objService.service = service;

                if (objService) {
                    reply.send(objService);
                } else {
                    reply.send({ ok: false });
                }
            } catch (error: any) {
                console.log(error);
                reply.send({ ok: false, error: error.message });
            }
        } else {
            reply.send({ ok: false, error: 'Incorrect data!' });
        }

    })


}
