import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import moment from 'moment-timezone';
var cron = require('node-cron');
const request = require("request");

// model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'
import { HisEphisHiModel } from '../models/his_ephis.model';
import { HisHosxpPcuXEModel } from '../models/his_hosxppcuxe.model';
import { HisHosxpv4PCUModel } from '../models/his_hosxpv4_pcu.model';
import { HisHosxpPCUModel } from '../models/his_hosxp_pcu.model';
import { BedsharingModel } from '../models/ket/bedsharing';
import { HisUbonrakThonburiModel } from '../models/his_ubonrakthonburi.model';
import { HisHosxpv3loeiModel } from '../models/his_hosxpv3loei.model';

import { RerferOutModel } from '../models/ket/referout'
import { RerferBackModel } from '../models/ket/referback'

const rerferOutModel = new RerferOutModel();
const rerferBackModel = new RerferBackModel();
const bedsharingModel = new BedsharingModel();

export default async function bedsharing(fastify: FastifyInstance) {

  // ห้ามแก้ไข // 
  //-----------------BEGIN START-------------------//
  const provider = process.env.HIS_PROVIDER;
  const hoscode = process.env.HIS_CODE;
  const connection = process.env.DB_CLIENT;
  const url = process.env.HIS_API_URL;

  const number = Math.floor(Math.random() * 60);
  // console.log(number);
  // console.log(url);


  let db: any;
  let hisModel: any;

  switch (connection) {
    case 'mysql2':
      db = fastify.mysql2;
      break;
    case 'mysql':
      db = fastify.mysql;
      break;
    case 'mssql':
      db = fastify.mssql;
      break;
    case 'pg':
      db = fastify.pg;
      break;
    case 'oracledb':
      db = fastify.oracledb;
      break;
    default:
      db = fastify.mysql;
  }

  switch (provider) {
    case 'ubase':
      hisModel = new HisUbaseModel();
      break;
    case 'himpro':
      hisModel = new HisHimproHiModel();
      break;
    case 'jhcis':
      hisModel = new HisJhcisHiModel();
      break;
    case 'hosxpv3':
      hisModel = new HisHosxpv3Model();
      break;
    case 'hosxpv3arjaro':
      hisModel = new HisHosxpv3ArjaroModel();
      break;
    case 'hosxpv4':
      hisModel = new HisHosxpv4Model();
      break;
    case 'hosxpv4pg':
      hisModel = new HisHosxpv4PgModel();
      break;
    case 'hi':
      hisModel = new HisHiModel();
      break;
    case 'homc':
      hisModel = new HisHomcModel();
      break;
    case 'mbase':
      hisModel = new HisMbaseModel();
      break;
    case 'phosp':
      hisModel = new HisPhospModel();
      break;
    case 'universal':
      hisModel = new HisUniversalHiModel();
      break;
    case 'sakon':
      hisModel = new HisSakonModel();
      break;
    case 'homcudon':
      hisModel = new HisHomcUbonModel();
      break;
    case 'softcon':
      hisModel = new HisSoftConModel();
      break;
    case 'softconudon':
      hisModel = new HisSoftConUdonModel();
      break;
    case 'ssb':
      hisModel = new HisSsbModel();
      break;
    case 'panaceaplus':
      hisModel = new HisPanaceaplusModel();
      break;
    case 'hosxppcuv4':
      hisModel = new HisHosxpv4PCUModel();
      break;
    case 'ephis':
      hisModel = new HisEphisHiModel();
      break;
    case 'hosxppcuxe':
      hisModel = new HisHosxpPcuXEModel();
      break;
    case 'hosxppcu':
      hisModel = new HisHosxpPCUModel();
      break;
    case 'ubonrakthonburi':
      hisModel = new HisUbonrakThonburiModel();
      break;
      case 'hosxploei':
        hisModel = new HisHosxpv3loeiModel();
        break;

    default:
    // hisModel = new HisModel();
  }
  //------------------BEGIN END------------------//


  // 
  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request
    // const token = req.headers.authorization.split(' ')[1];
    let hn: any;
    let _hoscode: any;
    let rs_hospital = [];
    let dateToday = new Date();
    let objBedsharing: any = {};
    let bedsharing: any = [];
    try {

      rs_hospital.push(await hisModel.getHospital(db, hn));
      // console.log("rs_hospital:", rs_hospital[0]);
      if (rs_hospital[0]) {
        if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
          let rs_hosp = [];
          rs_hosp = rs_hospital[0];
          // console.log(rs_hosp[0].provider_code);

          _hoscode = rs_hosp[0].provider_code;
          // providerName = rs_hosp[0].provider_name;
        } else {
          _hoscode = rs_hospital[0][0].provider_code;
          // providerName = rs_hospital[0][0].provider_name;
        }
      }

      let res_bedsharing = await hisModel.getBedsharing(db);
      // console.log(res_bedsharing);
      if (res_bedsharing) {
        res_bedsharing.forEach(async (v: any) => {
          let info = {
            "regdate": moment(dateToday).tz('Asia/Bangkok').format('YYYY-MM-DD'),
            "time": moment(dateToday).tz('Asia/Bangkok').format('HH:mm:ss'),
            "hcode": _hoscode,
            "ward_code": v.ward_code,
            "ward_name": v.ward_name,
            "ward_bed": v.ward_bed,
            "ward_standard": v.ward_standard,
            "ward_pt": v.ward_pt,
            "bor": '0',
            "province": v.province
          }
          let info_ = {
            "regdate": moment(dateToday).tz('Asia/Bangkok').format('YYYY-MM-DD'),
            "time": moment(dateToday).tz('Asia/Bangkok').format('HH:mm:ss'),
            "ward_name": v.ward_name,
            "ward_bed": v.ward_bed,
            "ward_standard": v.ward_standard,
            "ward_pt": v.ward_pt,
            "bor": '0',
            "province": v.province
          }
          bedsharing.push(info);
          let res_bedsharing_web: any = await bedsharingModel.getBedsharing(hoscode, info.ward_code)
          // console.log('res_bedsharing_web :', res_bedsharing_web);

          if (res_bedsharing_web[0]) {
            // console.log('update');
            // console.log(info_);
            let res_inser_web: any = await bedsharingModel.update(hoscode, info.ward_code, info_)
          } else {
            // console.log('insert');
            // console.log(info);
            let res_updete_web: any = await bedsharingModel.insert(info)
          }
        });
        objBedsharing.bedsharing = bedsharing;
      }
      reply.send(objBedsharing);
    } catch (error) {
      reply.send({ ok: false, error: error });
    }
  })


  cron.schedule(`*/${number} * * * *`, async (req: FastifyRequest, reply: FastifyReply) => {
    // console.log('running a task every minute');
    // request({
    // method: 'GET',
    // uri: `${url}/bedsharing`,
    // header: {
    //     'Content-Type': 'application/x-www-form-urlencoded',
    // },
    // }, (err:any, httpResponse:any, body:any) => {
    // if (err) {
    //     console.log(err)
    // } else {
    //     console.log(body)
    // }
    // })

    // const req: any = request
    // const token = req.headers.authorization.split(' ')[1];
    let hn: any;
    let _hoscode: any;
    let rs_hospital = [];

    let dateToday = new Date();
    let objBedsharing: any = {};
    let bedsharing: any = [];
    try {

      rs_hospital.push(await hisModel.getHospital(db, hn));
      // console.log("rs_hospital:", rs_hospital[0]);
      if (rs_hospital[0]) {
        if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
          let rs_hosp = [];
          rs_hosp = rs_hospital[0];
          // console.log(rs_hosp[0].provider_code);

          _hoscode = rs_hosp[0].provider_code;
          // providerName = rs_hosp[0].provider_name;
        } else {
          _hoscode = rs_hospital[0][0].provider_code;
          // providerName = rs_hospital[0][0].provider_name;
        }
      }

      let res_bedsharing = await hisModel.getBedsharing(db);
      // console.log(res_bedsharing);
      if (res_bedsharing) {
        res_bedsharing.forEach(async (v: any) => {
          let info = {
            "regdate": moment(dateToday).tz('Asia/Bangkok').format('YYYY-MM-DD'),
            "time": moment(dateToday).tz('Asia/Bangkok').format('HH:mm:ss'),
            "hcode": _hoscode,
            "ward_code": v.ward_code,
            "ward_name": v.ward_name,
            "ward_bed": v.ward_bed,
            "ward_standard": v.ward_standard,
            "ward_pt": v.ward_pt,
            "bor": '0',
            "province": v.province
          }
          let info_ = {
            "regdate": moment(dateToday).tz('Asia/Bangkok').format('YYYY-MM-DD'),
            "time": moment(dateToday).tz('Asia/Bangkok').format('HH:mm:ss'),
            "ward_name": v.ward_name,
            "ward_bed": v.ward_bed,
            "ward_standard": v.ward_standard,
            "ward_pt": v.ward_pt,
            "bor": '0',
            "province": v.province
          }
          bedsharing.push(info);
          let res_bedsharing_web: any = await bedsharingModel.getBedsharing(hoscode, info.ward_code)
          // console.log('res_bedsharing_web :', res_bedsharing_web);

          if (res_bedsharing_web[0]) {
            // console.log('update');
            // console.log(info_);
            let res_inser_web: any = await bedsharingModel.update(hoscode, info.ward_code, info_)
          } else {
            // console.log('insert');
            // console.log(info);
            let res_updete_web: any = await bedsharingModel.insert(info)
          }
        });
        objBedsharing.bedsharing = bedsharing;
      }
      // reply.send(objBedsharing);
      // console.log(objBedsharing);
    } catch (error) {
      console.log(error);
      // reply.send({ ok: false, error: error });
    }
  });


}