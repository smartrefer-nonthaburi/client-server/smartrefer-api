import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { GatewayModel } from '../../models/anywhere/anywhere_gateway'

const gatewayModel = new GatewayModel();
export default async function barthelRate(fastify: FastifyInstance) {
    // console.log(process.env);

    // select_list
    fastify.post('/select_list',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];        
        const body:any = {
            "token":{
                "username":`${process.env.USER_ANYWHERE}`,
                "password":`${process.env.PASS_ANYWHERE}`
            },
            "rows":req.body.rows
        }
        try {
            let res_: any = await gatewayModel.select_list(token,body);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })

    // select_pid
    fastify.post('/select_pid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        const body:any = {
            "token":{
                "username":`${process.env.USER_ANYWHERE}`,
                "password":`${process.env.PASS_ANYWHERE}`
            },
            "rows":req.body.rows
        }
        try {
            let res_: any = await gatewayModel.select_pid(token,body);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })


            // patient
    fastify.post('/patient',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        const body:any = {
            "token":{
                "username":`${process.env.USER_ANYWHERE}`,
                "password":`${process.env.PASS_ANYWHERE}`
            },
            "rows":req.body.rows
        }
        // console.log(body);
        try {
            let res_: any = await gatewayModel.patient(token,body);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })

           // cancer
           fastify.post('/cancer',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
            const req: any = request
            const token = req.headers.authorization.split(' ')[1];
            const body:any = {
                "token":{
                    "username":process.env.USER_ANYWHERE,
                    "password":process.env.PASS_ANYWHERE
                },
                "rows":req.body.rows
            }
            try {
                let res_: any = await gatewayModel.cancer(token,body);
                reply.send(res_);
              } catch (error) {
                reply.send({ ok: false, error: error });
              }
            })

            // treatment
            fastify.post('/treatment',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
                const req: any = request
                const token = req.headers.authorization.split(' ')[1];
                const body:any = {
                    "token":{
                        "username":process.env.USER_ANYWHERE,
                        "password":process.env.PASS_ANYWHERE
                    },
                    "rows":req.body.rows
                }
                try {
                    let res_: any = await gatewayModel.treatment(token,body);
                    reply.send(res_);
                  } catch (error) {
                    reply.send({ ok: false, error: error });
                  }
                })
       
    }