import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { StationModel } from '../../models/ket/station'

const stationModel = new StationModel();
export default async function station(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await stationModel.station(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

    // select
    fastify.get('/select_hospcode/:hospcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
      const req: any = request
      const token = req.headers.authorization.split(' ')[1];
      let hospcode = req.params.hospcode;

      try {
          let res_: any = await stationModel.station_hospcode(token,hospcode);
          reply.send(res_);
        } catch (error) {
          reply.send({ ok: false, error: error });
        }
  })    
}