import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { ServicePlanModel } from '../../models/ket/serviceplan'

const servicePlanModel = new ServicePlanModel();
export default async function serviceplan(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await servicePlanModel.servicePlan(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
    })

}