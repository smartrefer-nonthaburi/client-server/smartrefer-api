import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { RerferOutModel } from '../../models/ket/referout'
import { EvaluateModel } from '../../models/ket/evaluate'
import { GatewayModel } from '../../models/anywhere/anywhere_gateway'
import moment from 'moment-timezone';

const rerferOutModel = new RerferOutModel();
const evaluateModel = new EvaluateModel();
const gatewayModel = new GatewayModel();

export default async function referout(fastify: FastifyInstance) {

    // select
    fastify.get('/selectOne/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferOutModel.rfout_selectOne(token, refer_no);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/selectCid/:cid/:hcode/:refer',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        // let cid = req.params.cid;
        // let hcode = req.params.hcode;
        // let refer = req.params.refer;
        const token = req.headers.authorization.split(' ')[1];

        let cid = req.params.cid;
        let hcode = req.params.hcode;
        let refer = req.params.refer;

        try {
            let res_: any = await rerferOutModel.rfout_select_cid(token, cid,hcode,refer);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
        try {
            let res_: any = await rerferOutModel.rfout_select(token, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/selectLimit/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        try {
            let res_: any = await rerferOutModel.rfout_selectLimit(token, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info_ =  JSON.stringify(req.body.rows);
        let info_cancer:any

        if(req.body.rows.referout.refer_hospcode == '14201' || req.body.rows.referout.refer_hospcode == '12276' || req.body.rows.referout.refer_sentto_CancerAnywhere){   //14201
            let data:any = req.body.rows.referout;            
            let pname:any;
            if(data.pname == 'นาย'){ pname = '1'}
            else if(data.pname == 'นาง'){ pname = '2'}
            else if(data.pname == 'นางสาว'){ pname = '3'}
            else if(data.pname == 'เด็กชาย'){ pname = '4'}
            else if(data.pname == 'เด็กหญิง'){ pname = '5'}
            else { pname = '99'}
            let sex:any;
            if(data.sex == 'ชาย'){ sex = '1'}
            else if(data.sex == 'หญิง'){ sex = '2'}
            else { sex = '9'}
            let c = data.cid.split('');
            info_cancer = {
                "hn": data.hn,
                "hosCode": data.hcode,
                "title_code": pname,
                "name": data.fname,
                "last_name": data.lname,
                "birth_date": moment(data.dob).tz('Asia/Bangkok').format('YYYYMMDD') ,
                "cid": `${c[0]}-${c[1]}${c[2]}${c[3]}${c[4]}-${c[5]}${c[6]}${c[7]}${c[8]}${c[9]}-${c[10]}${c[11]}-${c[12]}`,
                "sex_code": sex,
                "nationality_code":"9",
                "address_no": data.addrpart,
                "address_moo": data.moopart, 
                "area_code": `${data.chwpart}${data.amppart}${data.tmbpart}`, 
                "permanent_address_no": data.addrpart,
                "permanent_address_moo": data.moopart, 
                "permanent_area_code": `${data.chwpart}${data.amppart}${data.tmbpart}`, 
                "refer_no": data.refer_no
            }
            let body:any = {
                "token":{
                    "username":`${process.env.USER_ANYWHERE}`,
                    "password":`${process.env.PASS_ANYWHERE}`
                },
                "rows":info_cancer
            }
            let res_: any = await gatewayModel.patient(token, body);
            // console.log(res_);
        }

        if (info_) {
            let res_: any = await rerferOutModel.rfInsert(token, info_);
            reply.send({ ok: true, res_ });
        } else {
            reply.send({ ok: false });
        }
    })

    fastify.put('/update/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let _info_ = JSON.stringify(req.body.rows);
        // console.log(_info_);
        try {
            let res_: any = await rerferOutModel.rfUpdate(token, refer_no, _info_);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/receive/delete/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let _info_ = {
            "referout": {
                "receive_no": "0",
                "receive_date": null,
                "receive_time": null,
                "receive_station_id": "",
                "receive_station_name": "",
                "receive_spclty_id": "",
                "receive_spclty_name": "",
                "receive_refer_result_id": "",
                "receive_refer_result_name": "",
                "receive_serviceplan_id": "",
                "receive_serviceplan_name": "",
                "reject_refer_reason": "",
                "receive_ward_id": "",
                "receive_ward_name": ""
            }
        }
        try {
            let res_: any = await rerferOutModel.rfUpdate(token, refer_no, JSON.stringify(_info_));
            if (res_) {
                let del: any = await evaluateModel.delEva_detail(token, refer_no);
            }
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/receive/delete_refer/:refer_no/:providerUser',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        // console.log("delete_refer");
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let providerUser = req.params.providerUser;
        let _info_ = {
            "referout": {
                "receive_no": "0",
                "receive_date": null,
                "receive_time": null,
                "receive_station_id": "",
                "receive_station_name": "",
                "receive_spclty_id": "",
                "receive_spclty_name": "",
                "receive_refer_result_id": "",
                "receive_refer_result_name": "",
                "receive_serviceplan_id": "",
                "receive_serviceplan_name": "",
                "reject_refer_reason": "",
                "receive_ward_id": "",
                "receive_ward_name": "",
                "providerUser": providerUser
            }
        }
        try {
            let res_: any = await rerferOutModel.rfUpdate(token, refer_no, JSON.stringify(_info_));
            if (res_) {
                let del: any = await evaluateModel.delEva_detail(token, refer_no);
            }
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/receive/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferOutModel.rfReceive(token, refer_no);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_referout/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
            let res_: any = await rerferOutModel.count_referout(token, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_referout_reply/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        try {
            let res_: any = await rerferOutModel.count_referout_reply(token, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_reply/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
    
        try {
            let res_: any = await rerferOutModel.rfout_select_reply(token, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_report/:stdate/:endate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        let stdate = req.params.stdate;
        let endate = req.params.endate;
        try {
            let res_: any = await rerferOutModel.count_report(token, stdate, endate, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/count_report_reply/:stdate/:endate/:hcode',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hcode = req.params.hcode;
        let stdate = req.params.stdate;
        let endate = req.params.endate;
        try {
            let res_: any = await rerferOutModel.count_report_reply(token, stdate, endate, hcode);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/delete/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        try {
            let res_: any = await rerferOutModel.del_referout(token, refer_no);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/delete_refer/:refer_no/:providerUser',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        let providerUser = req.params.providerUser;
        try {
            let res_: any = await rerferOutModel.del_user_referout(token, refer_no, providerUser);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })


    fastify.get('/select_telemed/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
    
        try {
            let res_: any = await rerferOutModel.rfout_select_telemed(token, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.put('/updateExpire/:refer_no',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let refer_no = req.params.refer_no;
        // let _info_ = JSON.stringify(req.body.rows);
        let _info_ = req.body.rows;
        console.log(_info_);
        try {
            let res_: any = await rerferOutModel.rfUpdateExpire(token, refer_no, _info_);
            reply.send({ ok: true, res_ });
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    fastify.get('/select_telemed_cid/:cid/:hcode/:sdate/:edate/:limit',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let limit = req.params.limit;
        let cid = req.params.cid;
        let hcode = req.params.hcode;
        let sdate = req.params.sdate;
        let edate = req.params.edate;
    
        try {
            let res_: any = await rerferOutModel.rfout_select_telemed_cid(token,cid, hcode, sdate, edate, limit);
            reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })    
}