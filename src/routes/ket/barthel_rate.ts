import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import { BarthelRateModel } from '../../models/ket/barthel_rate'

const barthelRateModel = new BarthelRateModel();
export default async function barthelRate(fastify: FastifyInstance) {

    // select
    fastify.get('/select',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_: any = await barthelRateModel.select(token);
            reply.send(res_);
          } catch (error) {
            reply.send({ ok: false, error: error });
          }
        })

    //select_brt_rate?brt_rate_id=xxx
    fastify.get('/select_brt_rate',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let brt_rate_id = req.query.brt_rate_id
        try {
          let res_: any = await barthelRateModel.select_brt_rate(token, brt_rate_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //select_cid?cid=xxx&refer_no=xxx
    fastify.get('/select_cid',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let cid = req.query.cid
        let refer_no = req.query.refer_no
        try {
          let res_: any = await barthelRateModel.select_cid(token, cid, refer_no);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //select_imc?brt_rate_id=xxx
    fastify.get('/select_imc',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let imc_id = req.query.imc_id
        try {
          let res_: any = await barthelRateModel.select_imc(token, imc_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // insert
    fastify.post('/insert',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let info = req.body.rows;
        // console.log(info);
        try {
          let res_: any = await barthelRateModel.insert(token, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    //update?imc_id=xxx
    fastify.put('/update',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let imc_id = req.query.imc_id
        let info = req.body
        try {
          let res_: any = await barthelRateModel.update(token, imc_id, info);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
      
    })

    //delete?imc_id=xxx
    fastify.get('/delete',{ preValidation: [fastify.authenticate] },  async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let imc_id = req.query.imc_id
        try {
          let res_: any = await barthelRateModel.delete(token, imc_id);
          reply.send(res_);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })


    
}