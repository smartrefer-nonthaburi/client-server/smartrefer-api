import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify'
import moment from 'moment-timezone';

// model
import { HisUniversalHiModel } from '../models/his_universal.model';
import { HisHiModel } from '../models/his_hi.model';
import { HisHomcModel } from '../models/his_homc.model';
import { HisHosxpv3Model } from '../models/his_hosxpv3.model';
import { HisHosxpv3ArjaroModel } from '../models/his_hosxpv3arjaro.model';
import { HisHosxpv4Model } from '../models/his_hosxpv4.model';
import { HisHosxpv4PgModel } from '../models/his_hosxpv4pg.model';
import { HisHosxpv4PCUModel } from '../models/his_hosxpv4_pcu.model';
import { HisMbaseModel } from '../models/his_mbase.model';
import { HisPhospModel } from '../models/his_phosp.model';
import { HisHimproHiModel } from '../models/his_himpro.model';
import { HisJhcisHiModel } from '../models/his_jhcis.model';
import { HisUbaseModel } from '../models/his_ubase.model';
import { HisSakonModel } from '../models/his_sakon.model';
import { HisHomcUbonModel } from '../models/his_homc_udon.model';
import { HisSoftConModel } from '../models/his_softcon.model'
import { HisSoftConUdonModel } from '../models/his_softcon_udon.model'
import { HisSsbModel } from '../models/his_ssb.model'
import { HisPanaceaplusModel } from '../models/his_panaceaplus.model'
import { HisEphisHiModel } from '../models/his_ephis.model';
import { HisHosxpPcuXEModel } from '../models/his_hosxppcuxe.model';
import { HisHosxpPCUModel } from '../models/his_hosxp_pcu.model';
import { HisUbonrakThonburiModel } from '../models/his_ubonrakthonburi.model';
import { HisHosxpv3loeiModel } from '../models/his_hosxpv3loei.model';

import { RerferOutModel } from '../models/ket/referout'
import { RerferBackModel } from '../models/ket/referback'


const rerferOutModel = new RerferOutModel();
const rerferBackModel = new RerferBackModel();

export default async function services(fastify: FastifyInstance) {

    // ห้ามแก้ไข // 
    //-----------------BEGIN START-------------------//
    const provider = process.env.HIS_PROVIDER;
    const hoscode = process.env.HIS_CODE;
    const connection = process.env.DB_CLIENT;
    let db: any;
    let hisModel: any;

    switch (connection) {
        case 'mysql2':
            db = fastify.mysql2;
            break;
        case 'mysql':
            db = fastify.mysql;
            break;
        case 'mssql':
            db = fastify.mssql;
            break;
        case 'pg':
            db = fastify.pg;
            break;
        case 'oracledb':
            db = fastify.oracledb;
            break;
        default:
            db = fastify.mysql;
    }

    switch (provider) {
        case 'ubase':
            hisModel = new HisUbaseModel();
            break;
        case 'himpro':
            hisModel = new HisHimproHiModel();
            break;
        case 'jhcis':
            hisModel = new HisJhcisHiModel();
            break;
        case 'hosxpv3':
            hisModel = new HisHosxpv3Model();
            break;
        case 'hosxpv3arjaro':
            hisModel = new HisHosxpv3ArjaroModel();
            break;
        case 'hosxpv4':
            hisModel = new HisHosxpv4Model();
            break;
        case 'hosxpv4pg':
            hisModel = new HisHosxpv4PgModel();
            break;
        case 'hi':
            hisModel = new HisHiModel();
            break;
        case 'homc':
            hisModel = new HisHomcModel();
            break;
        case 'mbase':
            hisModel = new HisMbaseModel();
            break;
        case 'phosp':
            hisModel = new HisPhospModel();
            break;
        case 'universal':
            hisModel = new HisUniversalHiModel();
            break;
        case 'sakon':
            hisModel = new HisSakonModel();
            break;
        case 'homcudon':
            hisModel = new HisHomcUbonModel();
            break;
        case 'softcon':
            hisModel = new HisSoftConModel();
            break;
        case 'softconudon':
            hisModel = new HisSoftConUdonModel();
            break;
        case 'ssb':
            hisModel = new HisSsbModel();
            break;
        case 'panaceaplus':
            hisModel = new HisPanaceaplusModel();
            break;
        case 'hosxppcuv4':
            hisModel = new HisHosxpv4PCUModel();
            break;
        case 'ephis':
            hisModel = new HisEphisHiModel();
            break;
        case 'hosxppcuxe':
            hisModel = new HisHosxpPcuXEModel();
            break;
        case 'hosxppcu':
            hisModel = new HisHosxpPCUModel();
            break;
        case 'ubonrakthonburi':
            hisModel = new HisUbonrakThonburiModel();
            break;
        case 'hosxploei':
            hisModel = new HisHosxpv3loeiModel();
            break;
        default:
        // hisModel = new HisModel();
    }
    //------------------BEGIN END------------------//

    // department
    fastify.get('/department', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        try {
            let res_depart = await hisModel.getDepartment(db);
            reply.send(res_depart);

        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // appoint/00000/2021-10-01
    fastify.get('/appoint/:hn/:app_date', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const params: any = request.params
        let hn = params.hn;
        let app_date = params.app_date;

        try {
            let res_app = await hisModel.getAppoint(db, hn, app_date);
            reply.send(res_app);

        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // referout//2021-10-01/2021-10-01
    fastify.get('/referout/:start_date/:end_date', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let start_date = req.params.start_date;
        let end_date = req.params.end_date;
        let _info_referno: any = [];
        let _referno_: any = [];
        try {
            let res_ = await hisModel.getReferOut(db, start_date, end_date);
            // console.log(res_);
            _info_referno = await rerferOutModel.rfout_select(token, hoscode, start_date, end_date, '5000');
            let info: any = [];
            let _refer_no_: any;
            for (const v of res_) {
                let _refer_no = `${hoscode}-1-${v.referno}`;
                if (_info_referno) {
                    _referno_ = _info_referno.filter((x: any) => {
                        return x.refer_no == _refer_no;
                    })
                    if (!_referno_[0]) {
                        _refer_no_ = '';
                        const info_data = {
                            "seq": v.seq,
                            "hn": v.hn,
                            "an": v.an,
                            "referno": v.referno,
                            "pttype_id": v.pttype_id,
                            "pttype_name": v.pttype_name,
                            "strength_id": v.strength_id,
                            "location_name": v.location_name,
                            "strength_name": v.strength_name,
                            "title_name": v.title_name,
                            "first_name": v.first_name,
                            "last_name": v.last_name,
                            "ReferDate": moment(v.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "to_hcode": v.to_hcode,
                            "to_hcode_name": v.to_hcode_name,
                            "refer_cause": v.refer_cause,
                            "ReferTime": v.refertime,
                            "doctor": v.doctor,
                            "doctor_name": v.doctor_name,
                            "refer_no_ket": _refer_no_
                        }
                        info.push(info_data);
                        // console.log(info);
                    }
                }
            }
            reply.send(info);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // referback/2021-10-01/2021-10-01
    fastify.get('/referback/:start_date/:end_date', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let start_date = req.params.start_date;
        let end_date = req.params.end_date;
        let _info_referno: any = [];
        let _referno_: any = [];
        try {
            let res_ = await hisModel.getReferBack(db, start_date, end_date);
            _info_referno = await rerferBackModel.rfback_select(token, hoscode, start_date, end_date, '5000');
            let info: any = [];
            let _refer_no_: any = null;
            for (const v of res_) {
                let _refer_no = `${hoscode}-3-${v.referno}`;
                if (_info_referno) {
                    _referno_ = _info_referno.filter((x: any) => {
                        return x.refer_no == _refer_no;
                    })
                    if (!_referno_[0]) {
                        _refer_no_ = '';
                        const info_data = {
                            "seq": v.seq,
                            "hn": v.hn,
                            "an": v.an,
                            "referno": v.referno,
                            "title_name": v.title_name,
                            "first_name": v.first_name,
                            "last_name": v.last_name,
                            "ReferDate": moment(v.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "to_hcode": v.to_hcode,
                            "location_name": v.location_name,
                            "to_hcode_name": v.to_hcode_name,
                            "refer_cause": v.refer_cause,
                            "ReferTime": v.refertime,
                            "doctor": v.doctor,
                            "doctor_name": v.doctor_name,
                            "refer_no_ket": _refer_no_
                        }
                        info.push(info_data);
                        // console.log(info);
                    }
                }
            }
            reply.send(info);
        } catch (error) {
            reply.send({ ok: false, error: error });
        }
    })

    // view/00000/00000000/64000000
    fastify.get('/view/:hn/:seq/:referno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hn = req.params.hn;
        let seq = req.params.seq;
        let referno = req.params.referno;
        let objService: any = {};
        let providerCode;
        let providerName;
        let profile = [];
        let rs_hospital = [];
        let rs_profile = [];
        let rs_vaccine = [];
        let rs_chronic = [];
        let rs_allergy = [];
        let rs_services = [];
        let rs_diagnosis = [];
        let rs_drugs = [];
        let rs_procedure = [];
        let rs_lab = [];
        let rs_apps = [];
        let rs_xray = [];
        let rs_refers = [];
        let rs_nurtures = [];
        let rs_physicals = [];
        let rs_pillness = [];
        let rs_medrecconcile = [];
        // let providerCodeToken = req.decoded.provider_code;
        if (hn && seq) {
            try {
                // console.log("provider:", provider);
                // console.log("process.env.HIS_PROVIDER:", process.env.HIS_PROVIDER);
                rs_hospital.push(await hisModel.getHospital(db, hn));
                // console.log("rs_hospital:", rs_hospital[0]);
                if (rs_hospital[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        let rs_hosp = [];
                        rs_hosp = rs_hospital[0];
                        // console.log(rs_hosp[0].provider_code);

                        providerCode = rs_hosp[0].provider_code;
                        providerName = rs_hosp[0].provider_name;
                    } else {
                        providerCode = rs_hospital[0][0].provider_code;
                        providerName = rs_hospital[0][0].provider_name;
                    }
                }
                rs_profile.push(await hisModel.getProfile(db, hn, seq,referno));
                // console.log("rs_profile:", rs_profile);
                if (rs_profile) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_profile = rs_profile
                    } else {
                        rs_profile = rs_profile[0]
                    }
                    for (const rpro of rs_profile) {
                        const objPro = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "age": rpro.age,
                            "sex": rpro.sex,
                            "occupation": rpro.occupation,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                    //เพิ่ม moopat,addrpart,tmbpart,amppart,chwpart,age(format 000-00-00)ปีเดือนวัน,locatioin_id
                    //location_name,pttype_id,pttype_name,pttype_no,hospmain,hospmain_name,hospsub,hospsub_name,registdate,visitdate
                }

                rs_allergy.push(await hisModel.getAllergyDetail(db, hn,referno));
                // console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_allergy = rs_allergy[0]
                    } else {
                        rs_allergy = rs_allergy[0]
                    }
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        const objAllergy = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                rs_medrecconcile.push(await hisModel.getMedrecconcile(db, hn,referno));
                // console.log("rs_medrecconcile:", rs_medrecconcile[0]);
                if (rs_medrecconcile[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_medrecconcile = rs_medrecconcile[0]
                    } else {
                        rs_medrecconcile = rs_medrecconcile[0]
                    }
                    let medrecconcile: any = [];
                    for (const ra of rs_medrecconcile) {
                        const objMedrecconcile = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_hospcode": ra.drug_hospcode,
                            "drug_hospname": ra.drug_hospname,
                            "drug_name": ra.drug_name,
                            "drug_use": ra.drug_use,
                            "drug_receive_date": moment(ra.drug_receive_date).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        medrecconcile.push(objMedrecconcile);
                    }
                    objService.medrecconcile = medrecconcile;
                }

                rs_services.push(await hisModel.getServices(db, hn, seq,referno));
                // console.log("rs_services:", rs_services[0]);
                if (rs_services[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_services = rs_services
                    } else {
                        rs_services = rs_services[0]
                    }
                    const pillness = [];
                    const physicals = [];
                    const nurtures = [];
                    const diagnosis = [];
                    const drugs = [];
                    const lab = [];
                    const procedure = [];
                    const appointment = [];
                    const refer = [];
                    const xray = [];
                    for (const v of rs_services) {

                        rs_pillness.push(await hisModel.getPillness(db, hn, v.date_serv, v.seq,referno));
                        // console.log("rs_pillness:", rs_pillness[0]);
                        if (rs_pillness[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_pillness = rs_pillness[0]
                            } else {
                                rs_pillness = rs_pillness[0]
                            }
                            for (const ri of rs_pillness) {
                                const objPillness = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": ri.seq,
                                    "hpi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                            objService.hpi = pillness;
                        } else {
                            objService.hpi = [];
                        }

                        rs_physicals.push(await hisModel.getPhysical(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_physicals = rs_physicals[0]
                            } else {
                                rs_physicals = rs_physicals[0]
                            }
                            for (const rp of rs_physicals) {
                                const objPhysicals = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                            objService.pe = physicals;
                        } else {
                            objService.pe = [];
                        }

                        rs_nurtures.push(await hisModel.getNurture(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_nurtures = rs_nurtures[0]
                            } else {
                                rs_nurtures = rs_nurtures[0]
                            }
                            for (const rn of rs_nurtures) {
                                const objNurtures = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight || 0,
                                    "height": rn.height || 0,
                                    "bmi": rn.bmi || 0,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "cc": rn.symptom,
                                    "pmh": rn.pmh,
                                    "depcode": rn.depcode,
                                    "department": rn.department,
                                    "movement_score": rn.movement_score,
                                    "vocal_score": rn.vocal_score,
                                    "eye_score": rn.eye_score,
                                    "oxygen_sat": rn.oxygen_sat,
                                    "gak_coma_sco": rn.gak_coma_sco,
                                    "pupil_right": rn.pupil_right,
                                    "pupil_left": rn.pupil_left,
                                    "diag_text": rn.diag_text || ''
                                }
                                nurtures.push(objNurtures);
                            }
                            objService.nurtures = nurtures;
                        } else {
                            objService.nurtures = [];
                        }

                        rs_diagnosis.push(await hisModel.getDiagnosis(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_diagnosis = rs_diagnosis[0]
                            } else {
                                rs_diagnosis = rs_diagnosis[0]
                            }
                            for (const rg of rs_diagnosis) {
                                const objDiagnosis = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "DiagNote": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                            objService.diagnosis = diagnosis;
                        } else {
                            objService.diagnosis = [];
                        }

                        rs_procedure.push(await hisModel.getProcedure(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_procedure = rs_procedure[0]
                            } else {
                                rs_procedure = rs_procedure[0]
                            }
                            for (const rp of rs_procedure) {
                                const objProcedure = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                            objService.procedure = procedure;
                        } else {
                            objService.procedure = [];
                        }

                        rs_drugs.push(await hisModel.getDrugs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_drugs = rs_drugs[0]
                            } else {
                                rs_drugs = rs_drugs[0]
                            }
                            for (const rd of rs_drugs) {
                                const objDrug = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2,
                                    "usage_line3": rd.usage_line3
                                }
                                drugs.push(objDrug);
                            }
                            objService.drugs = drugs;
                        } else {
                            objService.drugs = [];
                        }

                        rs_lab.push(await hisModel.getLabs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_lab:", rs_lab[0]);
                        if (rs_lab[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_lab = rs_lab[0]
                            } else {
                                rs_lab = rs_lab[0]
                            }
                            //console.log(rs_lab);
                            for (const rl of rs_lab) {
                                let standard_result: any;
                                if (rl.standard_result) {
                                    standard_result = rl.standard_result.toString();
                                } else {
                                    standard_result = rl.standard_result;
                                }
                                const objLab = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "date_serv": moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit,
                                    "standard_result": standard_result
                                }
                                lab.push(objLab);
                            }
                            objService.lab = lab;
                        } else {
                            objService.lab = [];
                        }

                        rs_xray.push(await hisModel.getXray(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_xray = rs_xray[0]
                            } else {
                                rs_xray = rs_xray[0]
                            }
                            for (const v of rs_xray) {
                                const objXray = {
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                            objService.xray = xray;
                        } else {
                            objService.xray = [];
                        }

                        rs_refers.push(await hisModel.getRefer(db, hn, v.date_serv, v.seq, referno));
                        // console.log("rs_refers:", rs_refers[0]);
                        if (rs_refers[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_refers = rs_refers[0]
                            } else {
                                rs_refers = rs_refers[0]
                            }
                            for (const rs_refer of rs_refers) {
                                const objRefer = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rs_refer.seq,
                                    "an": rs_refer.an,
                                    "pid": rs_refer.pid,
                                    "hn": rs_refer.hn,
                                    "referno": rs_refer.referno,
                                    "location_name": rs_refer.location_name,
                                    "station_name": rs_refer.station_name,
                                    "pttype_id": rs_refer.pttype_id,
                                    "pttype_name": rs_refer.pttype_name,
                                    "strength_id": rs_refer.strength_id,
                                    "strength_name": rs_refer.strength_name,
                                    "load_id": rs_refer.loads_id,
                                    "loads_name": rs_refer.loads_name,
                                    "ReferDate": moment(rs_refer.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "ReferTime": rs_refer.refertime,
                                    "to_hcode": rs_refer.to_hcode,
                                    "to_hcode_name": rs_refer.to_hcode_name,
                                    "doctor": rs_refer.doctor,
                                    "doctor_name": rs_refer.doctor_name,
                                    "refer_cause": rs_refer.refer_cause,
                                    "refer_remark": rs_refer.refer_remark,
                                }
                                refer.push(objRefer);
                            }
                            objService.refer = refer;
                        } else {
                            objService.refer = [];
                        }
                    }
                }

                if (objService) {
                    reply.send(objService);
                } else {
                    reply.send({ ok: false });
                }
            } catch (error: any) {
                console.log(error);
                reply.send({ ok: false, error: error.message });
            }
        } else {
            reply.send({ ok: false, error: 'Incorrect data!' });
        }

    })

    // view-insert-referout/000000/000000/00000
    fastify.get('/view-insert-referout/:hn/:seq/:referno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];

        let hn = req.params.hn;
        let seq = req.params.seq;
        let referno = req.params.referno;

        let objService: any = {};

        let providerCode;
        let providerName;
        let profile = [];
        let rs_hospital = [];
        let rs_profile = [];
        let rs_vaccine = [];
        let rs_chronic = [];
        let rs_allergy = [];
        let rs_services = [];
        let rs_diagnosis = [];
        let rs_drugs = [];
        let rs_procedure = [];
        let rs_lab = [];
        let rs_apps = [];
        let rs_xray = [];
        let rs_refers = [];
        let rs_nurtures = [];
        let rs_physicals = [];
        let rs_pillness = [];
        // let providerCodeToken = req.decoded.provider_code;
        if (hn && seq) {
            try {

                rs_hospital.push(await hisModel.getHospital(db, hn));
                // console.log("rs_hospital:", rs_hospital[0]);
                if (rs_hospital[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        let rs_hosp = [];
                        rs_hosp = rs_hospital[0];
                        // console.log(rs_hosp[0].provider_code);

                        providerCode = rs_hosp[0].provider_code;
                        providerName = rs_hosp[0].provider_name;
                    } else {
                        providerCode = rs_hospital[0][0].provider_code;
                        providerName = rs_hospital[0][0].provider_name;
                    }
                }
                rs_profile.push(await hisModel.getProfile(db, hn ,referno));
                // console.log("rs_profile:", rs_profile);
                if (rs_profile) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_profile = rs_profile
                    } else {
                        rs_profile = rs_profile[0]
                    }
                    for (const rpro of rs_profile) {
                        const objPro = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "age": rpro.age,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                    //เพิ่ม moopat,addrpart,tmbpart,amppart,chwpart,age(format 000-00-00)ปีเดือนวัน,locatioin_id
                    //location_name,pttype_id,pttype_name,pttype_no,hospmain,hospmain_name,hospsub,hospsub_name,registdate,visitdate
                }

                // rs_vaccine.push(await hisModel.getVaccine(db, hn));
                // // console.log("rs_vaccine:", rs_vaccine[0]);
                // if (rs_vaccine[0]) {
                //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                //         rs_vaccine = rs_vaccine
                //     } else {
                //         rs_vaccine = rs_vaccine[0]
                //     }
                //     let vaccines: any = [];
                //     for (const rv of rs_vaccine) {
                //         const objVcc = {
                //             "provider_code": providerCode,
                //             "provider_name": providerName,
                //             "date_serv": moment(rv.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                //             "time_serv": rv.time_serv,
                //             "vaccine_code": rv.vaccine_code,
                //             "vaccine_name": rv.vaccine_name
                //         }
                //         vaccines.push(objVcc);
                //     }
                //     objService.vaccines = vaccines;
                // }

                // rs_chronic.push(await hisModel.getChronic(db, hn));
                // // console.log("rs_chronic:", rs_chronic);
                // if (rs_chronic[0]) {
                //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                //         rs_chronic = rs_chronic
                //     } else {
                //         rs_chronic = rs_chronic[0]
                //     }
                //     let chronic: any = [];
                //     for (const rc of rs_chronic) {
                //         const objCho = {
                //             "provider_code": providerCode,
                //             "provider_name": providerName,
                //             "time_serv": rc.time_serv,
                //             "icd_code": rc.icd_code,
                //             "icd_name": rc.icd_name,
                //             "start_date": moment(rc.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD')
                //         }
                //         chronic.push(objCho);
                //     }
                //     objService.chronic = chronic;
                // }

                rs_allergy.push(await hisModel.getAllergyDetail(db, hn ,referno));
                // console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_allergy = rs_allergy
                    } else {
                        rs_allergy = rs_allergy[0]
                    }
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        const objAllergy = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                rs_services.push(await hisModel.getServices(db, hn, seq ,referno));
                // console.log("rs_services:", rs_services[0]);
                if (rs_services[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_services = rs_services
                    } else {
                        rs_services = rs_services[0]
                    }
                    const pillness = [];
                    const physicals = [];
                    const nurtures = [];
                    const diagnosis = [];
                    const drugs = [];
                    const lab = [];
                    const procedure = [];
                    const appointment = [];
                    const refer = [];
                    const xray = [];
                    for (const v of rs_services) {

                        rs_pillness.push(await hisModel.getPillness(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_pillness:", rs_pillness[0]);
                        if (rs_pillness[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_pillness = rs_pillness
                            } else {
                                rs_pillness = rs_pillness[0]
                            }
                            for (const ri of rs_pillness) {
                                const objPillness = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": ri.seq,
                                    "hpi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                            objService.hpi = pillness;
                        }

                        rs_physicals.push(await hisModel.getPhysical(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_physicals = rs_physicals
                            } else {
                                rs_physicals = rs_physicals[0]
                            }
                            for (const rp of rs_physicals) {
                                const objPhysicals = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                            objService.pe = physicals;
                        }

                        rs_nurtures.push(await hisModel.getNurture(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_nurtures = rs_nurtures
                            } else {
                                rs_nurtures = rs_nurtures[0]
                            }
                            for (const rn of rs_nurtures) {
                                const objNurtures = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight,
                                    "height": rn.height,
                                    "bmi": rn.bmi,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "cc": rn.symptom,
                                    "depcode": rn.depcode,
                                    "department": rn.department
                                }
                                nurtures.push(objNurtures);
                            }
                            objService.nurtures = nurtures;
                        }

                        rs_diagnosis.push(await hisModel.getDiagnosis(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_diagnosis = rs_diagnosis
                            } else {
                                rs_diagnosis = rs_diagnosis[0]
                            }
                            for (const rg of rs_diagnosis) {
                                const objDiagnosis = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "DiagNote": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                            objService.diagnosis = diagnosis;
                        }

                        rs_procedure.push(await hisModel.getProcedure(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_procedure = rs_procedure
                            } else {
                                rs_procedure = rs_procedure[0]
                            }
                            for (const rp of rs_procedure) {
                                const objProcedure = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                            objService.procedure = procedure;
                        }

                        rs_drugs.push(await hisModel.getDrugs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_drugs = rs_drugs
                            } else {
                                rs_drugs = rs_drugs[0]
                            }
                            for (const rd of rs_drugs) {
                                const objDrug = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2,
                                    "usage_line3": rd.usage_line3
                                }
                                drugs.push(objDrug);
                            }
                            objService.drugs = drugs;
                        }

                        rs_lab.push(await hisModel.getLabs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_lab:", rs_lab[0]);
                        if (rs_lab[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_lab = rs_lab
                            } else {
                                rs_lab = rs_lab[0]
                            }
                            for (const rl of rs_lab) {
                                const objLab = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "date_serv": moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit,
                                    "standard_result": rl.standard_result
                                }
                                lab.push(objLab);
                            }
                            objService.lab = lab;
                        }

                        // rs_apps.push(await hisModel.getAppointment(db, hn, v.date_serv, v.seq));
                        // // console.log("rs_apps:", rs_apps[0]);
                        // if (rs_apps[0]) {
                        //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                        //         rs_apps = rs_apps
                        //     } else {
                        //         rs_apps = rs_apps[0]
                        //     }
                        //     for (const rs_app of rs_apps) {
                        //         const objAppointment = {
                        //             "provider_code": providerCode,
                        //             "provider_name": providerName,
                        //             "seq": rs_app.seq,
                        //             "date_serv": moment(rs_app.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        //             "time_serv": rs_app.time_serv,
                        //             "clinic": rs_app.department,
                        //             "appoint_date": moment(rs_app.date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        //             "appoint_time": rs_app.time,
                        //             "detail": rs_app.detail
                        //         }
                        //         appointment.push(objAppointment);
                        //     }
                        //     objService.appointment = appointment;
                        // }

                        rs_xray.push(await hisModel.getXray(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_xray = rs_xray
                            } else {
                                rs_xray = rs_xray[0]
                            }
                            for (const v of rs_xray) {
                                const objXray = {
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                            objService.xray = xray;
                        }

                        rs_refers.push(await hisModel.getRefer(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_refers:", rs_refers[0]);
                        if (rs_refers[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_refers = rs_refers
                            } else {
                                rs_refers = rs_refers[0]
                            }
                            for (const rs_refer of rs_refers) {
                                const objRefer = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rs_refer.seq,
                                    "an": rs_refer.an,
                                    "pid": rs_refer.pid,
                                    "hn": rs_refer.hn,
                                    "referno": rs_refer.referno,
                                    "location_name": rs_refer.location_name,
                                    "station_name": rs_refer.station_name,
                                    "pttype_id": rs_refer.pttype_id,
                                    "pttype_name": rs_refer.pttype_name,
                                    "strength_id": rs_refer.strength_id,
                                    "strength_name": rs_refer.strength_name,
                                    "load_id": rs_refer.loads_id,
                                    "loads_name": rs_refer.loads_name,
                                    "ReferDate": moment(rs_refer.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "ReferTime": rs_refer.refertime,
                                    "to_hcode": rs_refer.to_hcode,
                                    "to_hcode_name": rs_refer.to_hcode_name,
                                    "doctor": rs_refer.doctor,
                                    "doctor_name": rs_refer.doctor_name,
                                    "refer_cause": rs_refer.refer_cause,
                                }
                                refer.push(objRefer);
                            }
                            objService.refer = refer;
                        }
                    }
                }

                if (objService) {
                    let info: any = {};
                    let rfdiag: any = [];
                    let rfdrug: any = [];
                    let rfallergy: any = [];
                    let rflab: any = [];
                    let rfprocedure: any = [];
                    let rfxray: any = [];
                    let rfsigntext: any = [];

                    let referout = {
                        "hcode": objService.refer[0].provider_code,
                        "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                        "refer_date": moment(objService.refer[0].referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        "refer_time": objService.refer[0].refertime,
                        "station_id": "",
                        "station_name": objService.refer[0].station_name,
                        "location_id": "",
                        "location_name": objService.refer[0].location_name,
                        "cid": objService.refer[0].cid,
                        "an": objService.refer[0].an,
                        "hn": objService.refer[0].hn,
                        "vn": objService.refer[0].vn,
                        "pid": objService.refer[0].pid,
                        "pname": objService.profile[0].title_name,
                        "fname": objService.profile[0].first_name,
                        "lname": objService.profile[0].last_name,
                        "age": objService.profile[0].age,
                        "addrpart": objService.profile[0].addrpart,
                        "moopart": objService.profile[0].moopart,
                        "tmbpart": objService.profile[0].tmbpart,
                        "amppart": objService.profile[0].amppart,
                        "chwpart": objService.profile[0].chwpart,
                        "pttype_id": objService.profile[0].pttype_id,
                        "pttype_name": objService.profile[0].pttype_name,
                        "pttypeno": objService.profile[0].pttype_no,
                        "hospmain": objService.profile[0].hospmain,
                        "hospsub": objService.profile[0].hospsub,
                        "typept_id": objService.refer[0].typept_id,
                        "typept_name": objService.refer[0].typept_name,
                        "strength_id": objService.refer[0].strength_id,
                        "strength_name": objService.refer[0].strength_name,
                        "doctor_id": objService.refer[0].doctor_id,
                        "doctor_name": objService.refer[0].doctor_name,
                        "refer_hospcode": objService.refer[0].to_hcode,
                        "cause_referout_id": "",
                        "cause_referout_name": objService.refer[0].refer_cause,
                        "expire_date": "",
                        "loads_id": objService.refer[0].loads_id,
                        "loads_name": objService.refer[0].loads_name,
                        "refer_remark": "",
                        "refer_type": "",
                        "refer_type_name": "",
                        "refer_his_no": objService.refer[0].referno,
                        "refer_station_id": "",
                        "refer_station_name": "",
                        "serviceplan_id": "",
                        "serviceplan_name": "",
                        "father_name": objService.profile[0].father_name,
                        "mother_name": objService.profile[0].mother_name,
                        "couple_name": objService.profile[0].couple_name,
                        "contact_name": objService.profile[0].contact_name,
                        "contact_relation": objService.profile[0].contact_relation,
                        "contact_mobile": objService.profile[0].contact_mobile,
                        "refer_triage_id": "",
                        "refer_triage_name": "",
                        "equipwithpatient": "",
                        "location_refer_id": "",
                        "location_refer_name": "",
                        "reject_refer_reason": ""
                    }
                    info.referout = referout;
                    info.hospital = { "providerCode": objService.refer[0].provider_code };
                    info.codestatus = { "code_status": objService.refer[0].provider_code };


                    if (objService.diagnosis) {
                        objService.diagnosis.forEach(async (v: any) => {
                            let rs_diagnosis = {
                                "hcode": v.provider_code,
                                "rec_no": "",
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "icd_code": v.icd_code,
                                "icd_name": v.icd_name,
                                "diag_type": v.diag_type,
                                "diagnote": v.DiagNote,
                                "diagtype_id": v.diagtype_id
                            }
                            rfdiag.push(rs_diagnosis)
                        });
                        info.rfdiag = rfdiag;
                    }
                    if (objService.drugs) {
                        objService.drugs.forEach(async (v: any) => {
                            let rs_drug = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "seq": v.seq,
                                "date_serv": moment(v.date_serv).format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "drug_name": v.drug_name,
                                "qty": v.qty,
                                "unit": v.unit,
                                "usage_line1": v.usage_line1,
                                "usage_line2": v.usage_line2,
                                "usage_line3": v.usage_line3
                            }
                            rfdrug.push(rs_drug);
                        });
                        info.rfdrug = rfdrug;
                    }
                    if (objService.allergy) {
                        objService.allergy.forEach(async (v: any) => {
                            let rs_allergy = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "hname": v.providerName,
                                "drug_name": v.drug_name,
                                "symptom": v.symptom,
                                "begin_date": v.begin_date
                            }
                            rfallergy.push(rs_allergy)
                        });
                        info.rfallergy = rfallergy;
                    }
                    if (objService.lab) {
                        objService.lab.forEach(async (v: any) => {
                            let rs_lab = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "date_serv": moment(v.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "labgroup": v.labgroup,
                                "lab_name": v.lab_name,
                                "lab_result": v.lab_result,
                                "unit": v.unit,
                                "standard_result": v.standard_result
                            };
                            rflab.push(rs_lab);
                        });
                        info.rflab = rflab;
                    }
                    if (objService.procedure) {
                        objService.procedure.forEach(async (v: any) => {
                            let rs_procedure = {
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "seq": v.seq,
                                "date_serv": moment(v.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "procedure_code": v.procedure_code,
                                "procedure_name": v.procedure_name,
                                "start_date": v.start_date,
                                "end_date": v.end_date
                            }
                            rfprocedure.push(rs_procedure)
                        });
                        info.rfprocedure = rfprocedure;
                    }
                    if (objService.xray) {
                        objService.xray.forEach(async (v: any) => {
                            let rs_xray = {
                                "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                                "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "xray_name": v.xray_name
                            }
                            rfxray.push(rs_xray)
                        });
                        info.xray = rfxray;
                    }
                    if (objService.nurtures) {
                        let rs_signtext = {
                            "hcode": objService.nurtures[0].provider_code,
                            "refer_no": `${objService.refer[0].provider_code}-1-${objService.refer[0].referno}`,
                            "body_weight_kg": objService.nurtures[0].weight,
                            "height_cm": objService.nurtures[0].height,
                            "pds": objService.nurtures[0].sbp,
                            "bps": objService.nurtures[0].dbp,
                            "temperature": objService.nurtures[0].temperature,
                            "hr": objService.nurtures[0].pr,
                            "pulse": objService.nurtures[0].pulse,
                            "rr": objService.nurtures[0].rr,
                            "bloodgroup": objService.nurtures[0].bloodgroup,
                            "cc": objService.nurtures[0].cc,
                            "pe": objService.pe[0].pe,
                            "hpi": objService.hpi[0].hpi,
                            "pmh": objService.nurtures[0].pmh,
                            "treatment": objService.nurtures[0].treatment
                        }
                        info.rfsigntext = rs_signtext;
                    }
                    // console.log(info);
                    if (info) {
                        let res_: any = await rerferOutModel.rfInsert(token, info);
                        reply.send({ info });
                    }

                } else {
                    reply.send({ ok: false });
                }
            } catch (error: any) {
                console.log(error);
                reply.send({ ok: false, error: error.message });
            }
        } else {
            reply.send({ ok: false, error: 'Incorrect data!' });
        }
    })

    // view-insert-referback/000000/000000
    fastify.get('/view-insert-referback/:hn/:seq/:referno', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let hn = req.params.hn;
        // let dateServ = req.params.dateServ;
        let seq = req.params.seq;
        let referno = req.params.referno;
        // let uid = req.params.uid;
        // let requestId = req.params.request_id;
        let objService: any = {};

        let providerCode;
        let providerName;
        let profile = [];
        let rs_hospital = [];
        let rs_profile = [];
        let rs_vaccine = [];
        let rs_chronic = [];
        let rs_allergy = [];
        let rs_services = [];
        let rs_diagnosis = [];
        let rs_drugs = [];
        let rs_procedure = [];
        let rs_lab = [];
        let rs_apps = [];
        let rs_xray = [];
        let rs_refers = [];
        let rs_nurtures = [];
        let rs_physicals = [];
        let rs_pillness = [];
        // let providerCodeToken = req.decoded.provider_code;
        if (hn && seq) {
            try {

                rs_hospital.push(await hisModel.getHospital(db, hn));
                // console.log("rs_hospital:", rs_hospital[0]);
                if (rs_hospital[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        let rs_hosp = [];
                        rs_hosp = rs_hospital[0];
                        // console.log(rs_hosp[0].provider_code);

                        providerCode = rs_hosp[0].provider_code;
                        providerName = rs_hosp[0].provider_name;
                    } else {
                        providerCode = rs_hospital[0][0].provider_code;
                        providerName = rs_hospital[0][0].provider_name;
                    }
                }
                rs_profile.push(await hisModel.getProfile(db, hn ,referno));
                // console.log("rs_profile:", rs_profile);
                if (rs_profile) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_profile = rs_profile
                    } else {
                        rs_profile = rs_profile[0]
                    }
                    for (const rpro of rs_profile) {
                        const objPro = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "hn": rpro.hn,
                            "cid": rpro.cid,
                            "title_name": rpro.title_name,
                            "first_name": rpro.first_name,
                            "last_name": rpro.last_name,
                            "moopart": rpro.moopart,
                            "addrpart": rpro.addrpart,
                            "tmbpart": rpro.tmbpart,
                            "amppart": rpro.amppart,
                            "chwpart": rpro.chwpart,
                            "brthdate": moment(rpro.brthdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "age": rpro.age,
                            "pttype_id": rpro.pttype_id,
                            "pttype_name": rpro.pttype_name,
                            "pttype_no": rpro.pttype_no,
                            "hospmain": rpro.hospmain,
                            "hospmain_name": rpro.hospmain_name,
                            "hospsub": rpro.hospsub,
                            "hospsub_name": rpro.hospsub_name,
                            "father_name": rpro.father_name,
                            "mother_name": rpro.mother_name,
                            "couple_name": rpro.couple_name,
                            "contact_name": rpro.contact_name,
                            "contact_relation": rpro.contact_relation,
                            "contact_mobile": rpro.contact_mobile,
                            "registdate": moment(rpro.registdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                            "visitdate": moment(rpro.visitdate).tz('Asia/Bangkok').format('YYYY-MM-DD')
                        }
                        profile.push(objPro);
                    }
                    objService.profile = profile;
                    //เพิ่ม moopat,addrpart,tmbpart,amppart,chwpart,age(format 000-00-00)ปีเดือนวัน,locatioin_id
                    //location_name,pttype_id,pttype_name,pttype_no,hospmain,hospmain_name,hospsub,hospsub_name,registdate,visitdate
                }

                // rs_vaccine.push(await hisModel.getVaccine(db, hn));
                // // console.log("rs_vaccine:", rs_vaccine[0]);
                // if (rs_vaccine[0]) {
                //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                //         rs_vaccine = rs_vaccine
                //     } else {
                //         rs_vaccine = rs_vaccine[0]
                //     }
                //     let vaccines: any = [];
                //     for (const rv of rs_vaccine) {
                //         const objVcc = {
                //             "provider_code": providerCode,
                //             "provider_name": providerName,
                //             "date_serv": moment(rv.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                //             "time_serv": rv.time_serv,
                //             "vaccine_code": rv.vaccine_code,
                //             "vaccine_name": rv.vaccine_name
                //         }
                //         vaccines.push(objVcc);
                //     }
                //     objService.vaccines = vaccines;
                // }

                // rs_chronic.push(await hisModel.getChronic(db, hn));
                // // console.log("rs_chronic:", rs_chronic);
                // if (rs_chronic[0]) {
                //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                //         rs_chronic = rs_chronic
                //     } else {
                //         rs_chronic = rs_chronic[0]
                //     }
                //     let chronic: any = [];
                //     for (const rc of rs_chronic) {
                //         const objCho = {
                //             "provider_code": providerCode,
                //             "provider_name": providerName,
                //             "time_serv": rc.time_serv,
                //             "icd_code": rc.icd_code,
                //             "icd_name": rc.icd_name,
                //             "start_date": moment(rc.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD')
                //         }
                //         chronic.push(objCho);
                //     }
                //     objService.chronic = chronic;
                // }

                rs_allergy.push(await hisModel.getAllergyDetail(db, hn ,referno));
                // console.log("rs_allergy:", rs_allergy[0]);
                if (rs_allergy[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_allergy = rs_allergy
                    } else {
                        rs_allergy = rs_allergy[0]
                    }
                    let allergy: any = [];
                    for (const ra of rs_allergy) {
                        const objAllergy = {
                            "provider_code": providerCode,
                            "provider_name": providerName,
                            "drug_name": ra.drug_name,
                            "symptom": ra.symptom,
                            "begin_date": moment(ra.begin_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),    //วันที่แพ้ยา
                            "daterecord": ra.daterecord     //วันที่บันทึกประวัติแพ้ยา
                        }
                        allergy.push(objAllergy);
                    }
                    objService.allergy = allergy;
                }

                rs_services.push(await hisModel.getServices(db, hn, seq ,referno));
                // console.log("rs_services:", rs_services[0]);
                if (rs_services[0]) {
                    if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                        rs_services = rs_services
                    } else {
                        rs_services = rs_services[0]
                    }
                    const pillness = [];
                    const physicals = [];
                    const nurtures = [];
                    const diagnosis = [];
                    const drugs = [];
                    const lab = [];
                    const procedure = [];
                    const appointment = [];
                    const refer = [];
                    const xray = [];
                    for (const v of rs_services) {

                        rs_pillness.push(await hisModel.getPillness(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_pillness:", rs_pillness[0]);
                        if (rs_pillness[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_pillness = rs_pillness
                            } else {
                                rs_pillness = rs_pillness[0]
                            }
                            for (const ri of rs_pillness) {
                                const objPillness = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": ri.seq,
                                    "hpi": ri.hpi,
                                }
                                pillness.push(objPillness);
                            }
                            objService.hpi = pillness;
                        }

                        rs_physicals.push(await hisModel.getPhysical(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_physicals[0]);
                        if (rs_physicals[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_physicals = rs_physicals
                            } else {
                                rs_physicals = rs_physicals[0]
                            }
                            for (const rp of rs_physicals) {
                                const objPhysicals = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "pe": rp.pe,
                                }
                                physicals.push(objPhysicals);
                            }
                            objService.pe = physicals;
                        }

                        rs_nurtures.push(await hisModel.getNurture(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_nurtures:", rs_nurtures[0]);
                        if (rs_nurtures[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_nurtures = rs_nurtures
                            } else {
                                rs_nurtures = rs_nurtures[0]
                            }
                            for (const rn of rs_nurtures) {
                                const objNurtures = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rn.seq,
                                    "date_serv": moment(rn.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rn.time_serv,
                                    "bloodgrp": rn.bloodgrp,
                                    "weight": rn.weight,
                                    "height": rn.height,
                                    "bmi": rn.bmi,
                                    "temperature": rn.temperature,
                                    "pr": rn.pr,
                                    "rr": rn.rr,
                                    "sbp": rn.sbp,
                                    "dbp": rn.dbp,
                                    "cc": rn.symptom,
                                    "depcode": rn.depcode,
                                    "department": rn.department
                                }
                                nurtures.push(objNurtures);
                            }
                            objService.nurtures = nurtures;
                        }

                        rs_diagnosis.push(await hisModel.getDiagnosis(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_diagnosis:", rs_diagnosis[0]);
                        if (rs_diagnosis[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_diagnosis = rs_diagnosis
                            } else {
                                rs_diagnosis = rs_diagnosis[0]
                            }
                            for (const rg of rs_diagnosis) {
                                const objDiagnosis = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rg.seq,
                                    "date_serv": moment(rg.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rg.time_serv,
                                    "icd_code": rg.icd_code,
                                    "icd_name": rg.icd_name,
                                    "diag_type": rg.diag_type,
                                    "DiagNote": rg.DiagNote,        //DiagNote
                                    "diagtype_id": rg.diagtype_id   //diagtype_id
                                }
                                diagnosis.push(objDiagnosis);
                            }
                            objService.diagnosis = diagnosis;
                        }

                        rs_procedure.push(await hisModel.getProcedure(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_procedure:", rs_procedure[0]);
                        if (rs_procedure[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_procedure = rs_procedure
                            } else {
                                rs_procedure = rs_procedure[0]
                            }
                            for (const rp of rs_procedure) {
                                const objProcedure = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rp.seq,
                                    "date_serv": moment(rp.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rp.time_serv,
                                    "procedure_code": rp.procedure_code,
                                    "procedure_name": rp.procedure_name,
                                    "start_date": moment(rp.start_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "start_time": rp.start_time,
                                    "end_date": rp.end_date ? moment(rp.end_date).tz('Asia/Bangkok').format('YYYY-MM-DD') : rp.end_date,
                                    "end_time": rp.end_time
                                }
                                procedure.push(objProcedure);
                            }
                            objService.procedure = procedure;
                        }

                        rs_drugs.push(await hisModel.getDrugs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_drugs:", rs_drugs[0]);
                        if (rs_drugs[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_drugs = rs_drugs
                            } else {
                                rs_drugs = rs_drugs[0]
                            }
                            for (const rd of rs_drugs) {
                                const objDrug = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rd.seq,
                                    "date_serv": moment(rd.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rd.time_serv,
                                    "drug_name": rd.drug_name,
                                    "qty": rd.qty,
                                    "unit": rd.unit,
                                    "usage_line1": rd.usage_line1,
                                    "usage_line2": rd.usage_line2,
                                    "usage_line3": rd.usage_line3
                                }
                                drugs.push(objDrug);
                            }
                            objService.drugs = drugs;
                        }

                        rs_lab.push(await hisModel.getLabs(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_lab:", rs_lab[0]);
                        if (rs_lab[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_lab = rs_lab
                            } else {
                                rs_lab = rs_lab[0]
                            }
                            for (const rl of rs_lab) {
                                const objLab = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "date_serv": moment(rl.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "time_serv": rl.time_serv,
                                    "labgroup": rl.labgroup,
                                    "lab_name": rl.lab_name,
                                    "lab_result": rl.lab_result,
                                    "unit": rl.unit,
                                    "standard_result": rl.standard_result
                                }
                                lab.push(objLab);
                            }
                            objService.lab = lab;
                        }

                        // rs_apps.push(await hisModel.getAppointment(db, hn, v.date_serv, v.seq ,referno));
                        // // console.log("rs_apps:", rs_apps[0]);
                        // if (rs_apps[0]) {
                        //     if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon") {
                        //         rs_apps = rs_apps
                        //     } else {
                        //         rs_apps = rs_apps[0]
                        //     }
                        //     for (const rs_app of rs_apps) {
                        //         const objAppointment = {
                        //             "provider_code": providerCode,
                        //             "provider_name": providerName,
                        //             "seq": rs_app.seq,
                        //             "date_serv": moment(rs_app.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        //             "time_serv": rs_app.time_serv,
                        //             "clinic": rs_app.department,
                        //             "appoint_date": moment(rs_app.date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        //             "appoint_time": rs_app.time,
                        //             "detail": rs_app.detail
                        //         }
                        //         appointment.push(objAppointment);
                        //     }
                        //     objService.appointment = appointment;
                        // }

                        rs_xray.push(await hisModel.getXray(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_xray:", rs_xray[0]);
                        if (rs_xray[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_xray = rs_xray
                            } else {
                                rs_xray = rs_xray[0]
                            }
                            for (const v of rs_xray) {
                                const objXray = {
                                    "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "xray_name": v.xray_name,
                                }
                                xray.push(objXray);
                            }
                            objService.xray = xray;
                        }

                        rs_refers.push(await hisModel.getRefer(db, hn, v.date_serv, v.seq ,referno));
                        // console.log("rs_refers:", rs_refers[0]);
                        if (rs_refers[0]) {
                            if (process.env.HIS_PROVIDER == "homc" || process.env.HIS_PROVIDER == "homcudon" || process.env.HIS_PROVIDER == "softcon" || process.env.HIS_PROVIDER == "softconudon" || process.env.HIS_PROVIDER == "panaceaplus") {
                                rs_refers = rs_refers
                            } else {
                                rs_refers = rs_refers[0]
                            }
                            for (const rs_refer of rs_refers) {
                                const objRefer = {
                                    "provider_code": providerCode,
                                    "provider_name": providerName,
                                    "seq": rs_refer.seq,
                                    "an": rs_refer.an,
                                    "pid": rs_refer.pid,
                                    "hn": rs_refer.hn,
                                    "referno": rs_refer.referno,
                                    "location_name": rs_refer.location_name,
                                    "station_name": rs_refer.station_name,
                                    "pttype_id": rs_refer.pttype_id,
                                    "pttype_name": rs_refer.pttype_name,
                                    "strength_id": rs_refer.strength_id,
                                    "strength_name": rs_refer.strength_name,
                                    "load_id": rs_refer.loads_id,
                                    "loads_name": rs_refer.loads_name,
                                    "ReferDate": moment(rs_refer.referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                    "ReferTime": rs_refer.refertime,
                                    "to_hcode": rs_refer.to_hcode,
                                    "to_hcode_name": rs_refer.to_hcode_name,
                                    "doctor": rs_refer.doctor,
                                    "doctor_name": rs_refer.doctor_name,
                                    "refer_cause": rs_refer.refer_cause,
                                }
                                refer.push(objRefer);
                            }
                            objService.refer = refer;
                        }
                    }
                }

                if (objService) {
                    let info: any = {};
                    let rfdiag: any = [];
                    let rfdrug: any = [];
                    let rfallergy: any = [];
                    let rflab: any = [];
                    let rfprocedure: any = [];
                    let rfxray: any = [];
                    let rfsigntext: any = [];
                    let rfrepatriate: any = [];

                    let referback = {
                        "hcode": objService.refer[0].provider_code,
                        "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                        "referout_no": "",
                        "refer_date": moment(objService.refer[0].referdate).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                        "refer_time": objService.refer[0].referrime,
                        "hn": objService.refer[0].hn,
                        "cid": objService.refer[0].cid,
                        "pname": objService.profile[0].title_name,
                        "fname": objService.profile[0].first_name,
                        "lname": objService.profile[0].last_name,
                        "age": objService.profile[0].age,
                        "addrpart": objService.profile[0].addrpart,
                        "moopart": objService.profile[0].moopart,
                        "tmbpart": objService.profile[0].tmbpart,
                        "amppart": objService.profile[0].amppart,
                        "chwpart": objService.profile[0].chwpart,
                        "doctor_id": objService.refer[0].doctor_id,
                        "doctor_name": objService.refer[0].doctor_name,
                        "cause_referback_id": "",
                        "cause_referback_name": "",
                        "refer_hospcode": objService.refer[0].to_hcode,
                        "referback_his_no": objService.refer[0].referno,
                        "refer_type_back_id": "",
                        "refer_type_back_name": "",
                        "Station_ID": objService.refer[0].station_id,
                        "Station_name": objService.refer[0].station_name,
                        "Location_ID": "",
                        "Location_name": objService.refer[0].location_name,
                        "pttype_name": objService.profile[0].pttype_name,
                        "pttypeno": objService.profile[0].pttype_no,
                        "loads_id": objService.refer[0].loads_id,
                        "loads_name": objService.refer[0].loads_name,
                        "refer_remark": "",


                    }
                    info.referback = referback;
                    info.hospital = { "providerCode": objService.refer[0].provider_code };
                    info.codestatus = { "code_status": objService.refer[0].provider_code };

                    let rs_repatriate = {
                        "hcode": objService.refer[0].provider_code,
                        "referback_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                        "hcode_referback": objService.refer[0].to_hcode,
                        "hcode_receciveback": "",
                        "care_send_cousin": "",
                        "care_send_nurse": "",
                        "reply_by": "",
                        "reply_by_position": "",
                        "rececive_document": "",
                        "rececive_document_date": "",
                        "rececive_document_time": "",
                        "rececive_document_man": "",
                        "rececive_document_man_position": "",
                        "loads_repatriate_id": "",
                        "getback_load_id": "",
                        "getback_load_name": "",
                        "getback_date": "",
                        "getback_time": "",
                        "getback_man": "",
                        "getback_man_position": "",
                        "rececive_patient_date": "",
                        "rececive_patient_time": "",
                        "rececive_patient_by": "",
                        "rececive_patient_position": "",
                    }
                    info.repatriate = rs_repatriate;

                    if (objService.diagnosis) {
                        objService.diagnosis.forEach(async (v: any) => {
                            let rs_diagnosis = {
                                "hcode": v.provider_code,
                                "rec_no": "",
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "icd_code": v.icd_code,
                                "icd_name": v.icd_name,
                                "diag_type": v.diag_type,
                                "diagnote": v.DiagNote,
                                "diagtype_id": v.diagtype_id
                            }
                            rfdiag.push(rs_diagnosis)
                        });
                        info.rfdiag = rfdiag;
                    }
                    if (objService.drugs) {
                        objService.drugs.forEach(async (v: any) => {
                            let rs_drug = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "seq": v.seq,
                                "date_serv": moment(v.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "drug_name": v.drug_name,
                                "qty": v.qty,
                                "unit": v.unit,
                                "usage_line1": v.usage_line1,
                                "usage_line2": v.usage_line2,
                                "usage_line3": v.usage_line3
                            }
                            rfdrug.push(rs_drug);
                        });
                        info.rfdrug = rfdrug;
                    }
                    if (objService.allergy) {
                        objService.allergy.forEach(async (v: any) => {
                            let rs_allergy = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "hname": v.providerName,
                                "drug_name": v.drug_name,
                                "symptom": v.symptom,
                                "begin_date": v.begin_date
                            }
                            rfallergy.push(rs_allergy)
                        });
                        info.rfallergy = rfallergy;
                    }
                    if (objService.lab) {
                        objService.lab.forEach(async (v: any) => {
                            let rs_lab = {
                                "hcode": v.provider_code,
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "date_serv": moment(v.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "labgroup": v.labgroup,
                                "lab_name": v.lab_name,
                                "lab_result": v.lab_result,
                                "unit": v.unit,
                                "standard_result": v.standard_result
                            }
                            rflab.push(rs_lab)
                        });
                        info.rflab = rflab;
                    }
                    if (objService.procedure) {
                        objService.procedure.forEach(async (v: any) => {
                            let rs_procedure = {
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "seq": v.seq,
                                "date_serv": moment(v.date_serv).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "time_serv": v.time_serv,
                                "procedure_code": v.procedure_code,
                                "procedure_name": v.procedure_name,
                                "start_date": v.start_date,
                                "end_date": v.end_date
                            }
                            rfprocedure.push(rs_procedure)
                        });
                        info.rfprocedure = rfprocedure;
                    }
                    if (objService.xray) {
                        objService.xray.forEach(async (v: any) => {
                            let rs_xray = {
                                "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                                "xray_date": moment(v.xray_date).tz('Asia/Bangkok').format('YYYY-MM-DD'),
                                "xray_name": v.xray_name
                            }
                            rfxray.push(rs_xray)
                        });
                        info.xray = rfxray;
                    }
                    if (objService.nurtures) {
                        let rs_signtext = {
                            "hcode": objService.nurtures[0].provider_code,
                            "refer_no": `${objService.refer[0].provider_code}-3-${objService.refer[0].referno}`,
                            "body_weight_kg": objService.nurtures[0].weight,
                            "height_cm": objService.nurtures[0].height,
                            "pds": objService.nurtures[0].sbp,
                            "bps": objService.nurtures[0].dbp,
                            "temperature": objService.nurtures[0].temperature,
                            "hr": objService.nurtures[0].pr,
                            "pulse": objService.nurtures[0].pulse,
                            "rr": objService.nurtures[0].rr,
                            "bloodgroup": objService.nurtures[0].bloodgroup,
                            "cc": objService.nurtures[0].cc,
                            "pe": objService.pe[0].pe,
                            "hpi": objService.hpi[0].hpi,
                            "pmh": objService.nurtures[0].pmh,
                            "treatment": objService.nurtures[0].treatment
                        }
                        info.rfsigntext = rs_signtext;
                    }
                    // console.log(info);
                    if (info) {
                        let res_: any = await rerferBackModel.rfbackInsert(token, info);
                        reply.send({ info });
                    }

                } else {
                    reply.send({ ok: false });
                }
            } catch (error: any) {
                console.log(error);
                reply.send({ ok: false, error: error.message });
            }
        } else {
            reply.send({ ok: false, error: 'Incorrect data!' });
        }
    })

    // patient/0000000000000
    fastify.get('/patient/:cid', { preValidation: [fastify.authenticate] }, async (request: FastifyRequest, reply: FastifyReply) => {
        const req: any = request
        const token = req.headers.authorization.split(' ')[1];
        let cid = req.params.cid;

        try {
            let res_app = await hisModel.getPtHN(db, cid);
            reply.send(res_app);

        } catch (error) {
            reply.send({ ok: false, error: error });
        }

    })
}
