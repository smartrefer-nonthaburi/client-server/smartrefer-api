import * as fastify from 'fastify'
import { join } from 'path'

const multer = require('fastify-multer')

require('dotenv').config({ path: join(__dirname, '../config') })

const app: fastify.FastifyInstance = fastify.fastify({
  // logger: false
  logger: { level: 'info' }
})

app.register(multer.contentParser)

app.register(require('fastify-formbody'))
app.register(require('fastify-cors'), {})

//  Connection
if (process.env.DB_CLIENT == 'mssql') {
  app.register(require('./plugins/db'), {

    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
        trustServerCertificate: true,
        encrypt: false,
      },
      pool: {
        min: 0,
        max: 100,
      },
      debug: false,

    }
  })
} else if (process.env.DB_CLIENT == 'pg') {
  app.register(require('./plugins/db'), {
    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
      },
      pool: {
        min: 0,
        max: 100,
      },
      debug: false,
    }
  })
} else if (process.env.DB_CLIENT == 'oracledb') {
  // console.log('DB_CLIENT :',process.env.DB_CLIENT);
  app.register(require('./plugins/db'), {
    connectionName: process.env.DB_CLIENT,
    options: {
      client: 'oracledb',
      connection: {
        user: process.env.DB_USER,
        password: process.env.DB_PASSWORD,
        connectString: `(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = ${process.env.DB_HOST})(PORT = ${Number(process.env.DB_PORT)}))(CONNECT_DATA = (SERVER = DEDICATED) (SERVICE_NAME = ${process.env.DB_INSTANCE_NAME})))`
      },
      fetchAsString: ['number', 'clob']
    }
  })
} else {
  app.register(require('./plugins/db'), {

    connectionName: process.env.DB_CLIENT,
    options: {
      client: process.env.DB_CLIENT,
      connection: {
        host: process.env.DB_HOST,
        user: process.env.DB_USER,
        port: Number(process.env.DB_PORT),
        password: process.env.DB_PASSWORD,
        database: process.env.DB_NAME,
      },
      pool: {
        min: 0,
        max: 100,
        afterCreate: (conn: any, done: any) => {
          conn.query('SET NAMES ' + process.env.DB_CHARSET, (err: any) => {
            done(err, conn);
          });
        },
      },
      debug: false,
    }
  })
}



app.register(require('./plugins/jwt'), {
  secret: process.env.SECRET_KEY
})

app.register(require('./plugins/ws'), {
  path: '/ws',
  maxPayload: 1048576,
  verifyClient: function (info: any, next: any) {
    if (info.req.headers['x-fastify-header'] !== 'fastify') {
      return next(false)
    }
    next(true)
  }
})

// Axios
app.register(require('fastify-axios'), {
  clients: {
    v1: {
      baseURL: 'https://apingweb.com/api/rest',
    },
    v2: {
      baseURL: 'https://randomuser.me/api'
    }
  }
})

// QR Code
app.register(require('@chonla/fastify-qrcode'))

// Plugins
app.register(require('./routes/upload'), { prefix: '/upload' });
app.register(require('./routes/schema'), { prefix: '/schema' });

// Service
app.register(require('./routes/index'), { prefix: '/' });
app.register(require('./routes/test'), { prefix: '/test' });

app.register(require('./routes/login'), { prefix: '/login' });
app.register(require('./routes/bedsharing'), { prefix: '/bedsharing' });
app.register(require('./routes/ket/version'), { prefix: '/version' });
app.register(require('./routes/ket/refer_server'), { prefix: '/referserver' });

app.register(require('./routes/services'), { prefix: '/services' });

app.register(require('./routes/ket/attachment_type'), { prefix: '/attachment_type' });
app.register(require('./routes/ket/attachment'), { prefix: '/attachment' });
app.register(require('./routes/ket/barthel_rate'), { prefix: '/barthel_rate' });
app.register(require('./routes/ket/cause_referback'), { prefix: '/cause_referback' });
app.register(require('./routes/ket/cause_referout'), { prefix: '/cause_referout' });
app.register(require('./routes/ket/co_office'), { prefix: '/office' });
app.register(require('./routes/ket/evaluate'), { prefix: '/evaluate' });
app.register(require('./routes/ket/hospcode'), { prefix: '/hospcode' });

app.register(require('./routes/ket/loads'), { prefix: '/loads' });
app.register(require('./routes/ket/person_imc'), { prefix: '/person_imc' });
app.register(require('./routes/ket/refer_log'), { prefix: '/referog' });
app.register(require('./routes/ket/refer_result'), { prefix: '/result' });
app.register(require('./routes/ket/referback'), { prefix: '/referback' });
app.register(require('./routes/ket/referout'), { prefix: '/referout' });
app.register(require('./routes/ket/serviceplan'), { prefix: '/serviceplan' });
app.register(require('./routes/ket/station'), { prefix: '/station' });
app.register(require('./routes/ket/strength'), { prefix: '/strength' });
app.register(require('./routes/ket/thaiaddress'), { prefix: '/address' });
app.register(require('./routes/ket/token'), { prefix: '/token' });
app.register(require('./routes/ket/typept'), { prefix: '/typept' });
app.register(require('./routes/ket/refer_triage'), { prefix: '/refertriage' });
app.register(require('./routes/ket/refer_type'), { prefix: '/refertype' });
app.register(require('./routes/ket/helpdesks'), { prefix: '/helpdesks' });
app.register(require('./routes/ket/appoint'), { prefix: '/appoint' });
app.register(require('./routes/ket/appoint_slot'), { prefix: '/appoint_slot' });
app.register(require('./routes/ket/holiday'), { prefix: '/holiday' });

app.register(require('./routes/anywhere/anywhere_gateway'), { prefix: '/anywhere' });


app.register(require('./routes/coc/coc_allergy'), { prefix: '/coc_allergy' });
app.register(require('./routes/coc/coc_answer'), { prefix: '/coc_answer' });
app.register(require('./routes/coc/coc_catagory'), { prefix: '/coc_catagory' });
app.register(require('./routes/coc/coc_evaluate'), { prefix: '/coc_evaluate' });
app.register(require('./routes/coc/coc_genogram'), { prefix: '/coc_genogram' });
app.register(require('./routes/coc/coc_home'), { prefix: '/coc_home' });
app.register(require('./routes/coc/coc_lab'), { prefix: '/coc_lab' });
app.register(require('./routes/coc/coc_med'), { prefix: '/coc_med' });
app.register(require('./routes/coc/coc_diag'), { prefix: '/coc_diag' });
app.register(require('./routes/coc/coc_items'), { prefix: '/coc_items' });
app.register(require('./routes/coc/coc_map_catagory'), { prefix: '/coc_map_catagory' });
app.register(require('./routes/coc/coc_patient_status'), { prefix: '/coc_patient_status' });
app.register(require('./routes/coc/coc_question'), { prefix: '/coc_question' });
app.register(require('./routes/coc/coc_question_type'), { prefix: '/coc_question_type' });
app.register(require('./routes/coc/coc_register'), { prefix: '/coc_register' });
app.register(require('./routes/coc/coc_xray'), { prefix: '/coc_xray' });
app.register(require('./routes/coc/coc_refer_level'), { prefix: '/coc_refer_level' });

app.register(require('./routes/coc/report_question'), { prefix: '/question' });

app.register(require('./routes/services_coc'), { prefix: '/services_coc' });

const port = process.env.PORT || 3000
const address = process.env.HOST || '0.0.0.0'

const start = async () => {
  try {
    await app.listen(port, address)
  } catch (error) {
    console.error(error)
    process.exit(1)
  }
  console.log(`Server listening on ${address}`)
}

start()

export default app;